package ai.mindslab.maienglish.mAITalk;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ai.mindslab.maienglish.Book.Books___Activity;
import ai.mindslab.maienglish.Common.ConstDef;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.ExternLib.RippleLayout;
import ai.mindslab.maienglish.Model.ReactionVO;
import ai.mindslab.maienglish.RestAPI.Param;
import ai.mindslab.maienglish.Model.RestApiVO;
import ai.mindslab.maienglish.Model.mAITalkVO;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.RestAPI.ResCode;
import ai.mindslab.maienglish.RestAPI.mAITalkAPI;
import ai.mindslab.maienglish.STT.SttHandler;
import ai.mindslab.maienglish.SideMenu.SideMenu___Activity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class mAI_Talk___Activity extends AppCompatActivity {

    final int STATE___BEGIN_SCENARIO = 1;
    final int STATE___RUN_SCENARIO = 2;
    final int STATE___REACTION_CORRECT = 3;
    final int STATE___REACTION_INCORRECT = 4;
    final int STATE___END_SCENARIO = 5;

    int mState = STATE___BEGIN_SCENARIO;

    int mRetryCnt = 0; // 질문에 대한 오답 회수

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mai_talk___activity);
        ButterKnife.bind(this);

        initAPI();
        initMic();
        initAudio();
        initButton();

        Intent it = getIntent();
        int talk_id = it.getIntExtra(Param.MAI_TALK_ID, 0);
        loadScenario(talk_id);
    }


    @Override
    public void onPause() {
        super.onPause();

        if(mMediaPlayer.isPlaying()) mMediaPlayer.stop();

        /* 마이크가 활성화 되어 있으면 Off 상태로 설정한다. */
        if(mSttHandler.isIdle() == false) {
            mSttHandler.setReady("");
        }
    }

    /* ###################################################################################################### */
    // MIC UI

    @BindView(R.id.EffectMic) ImageView mEffectMic;
    @BindView(R.id.Button_Mic) ImageView mButton_Mic;
    @BindView(R.id.ScrollView) ScrollView mScrollView;

    SttHandler mSttHandler;
    void initMic() {
        mSttHandler = new SttHandler(this, mHandler, mButton_Mic, mEffectMic);
    }


    /* ###################################################################################################### */
    // HANDLER

    static public final int MSG___BEGIN_SCENARIO = 1;
    static public final int MSG___END_SCENARIO = 2;
    static public final int MSG___NEXT_SCENARIO = 3;

    static public final int MSG___DISABLE_MIC = 10;
    static public final int MSG___OFF_MIC = 11;
    static public final int MSG___ON_MIC = 12;

    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what) {
                /* 마이톡 시나리오가 정상 로딩되면 수신되는 메세지 */
                case MSG___BEGIN_SCENARIO:
                    initUI();
                    beginScenario();
                    break;

                case MSG___END_SCENARIO:
                    endScenario();
                    break;

                case MSG___NEXT_SCENARIO:
                    runNextScenario();
                    break;


                case MSG___DISABLE_MIC:
                    mSttHandler.setIdle();
                    break;

                case MSG___OFF_MIC:
                    mSttHandler.setReady((String) msg.obj);
                    break;

                case MSG___ON_MIC:
                    mSttHandler.setStart((String) msg.obj);
                    break;

                case SttHandler.MSG___STT_RESPONSE:
                    onSttResult((Map<String, Object>) msg.obj);
                    break;
            }
        }
    };



    /* ###################################################################################################### */
    // LOAD DATA

    mAITalkVO   mTalk = null;
    mAITalkAPI  mAPI_mAITalk;

    void initAPI() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConstDef.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mAPI_mAITalk = retrofit.create(mAITalkAPI.class);
    }

    void loadScenario(final int talk_id) {
        mAPI_mAITalk.getScenario(talk_id).enqueue(new Callback<RestApiVO>() {
            @Override
            public void onResponse(@NonNull Call<RestApiVO> call, @NonNull Response<RestApiVO> response) {
                if (response.isSuccessful()) {
                    RestApiVO body = response.body();
                    if (body != null) {
                        Log.e("AAA", "body ========>>> " + body.toString());
                        mTalk = body.getMai_talk();
                        if(body.getRes_code() == ResCode.SUCC) mHandler.sendEmptyMessage(MSG___BEGIN_SCENARIO);

                    } else {
                        Toast.makeText(mAI_Talk___Activity.this, "(1) 서버와 연결이 원활하지 않습니다.", Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestApiVO> call, @NonNull Throwable t) {
                Log.e("AAA", "getScenario()................................... fail >> " + t.toString());
                Toast.makeText(mAI_Talk___Activity.this, "(2) 서버와 연결이 원활하지 않습니다.", Toast.LENGTH_LONG).show();
            }
        });
    }


    /* ###################################################################################################### */
    // UI

    @BindView(R.id.Image) ImageView mImage;
    @BindView(R.id.Layer_Talk) LinearLayout mLayer_Talk;

    View mCurrentTalkView;

    void initUI() {
        setImage();

    }

    void setImage() {
        if(mTalk.getImg_path() == null || mTalk.getImg_path().length() == 0) return;

        Picasso.with(this).load(ConstDef.BASE_IMAGE_URL + mTalk.getImg_path()).into(mImage);
        mImage.setVisibility(View.VISIBLE);
    }

    void addMaiTalk(String sentence, String translation) {
        LayoutInflater inflater = (LayoutInflater) getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        mCurrentTalkView = inflater.inflate( R.layout.talk___row, null );

        /* 사용자 아이콘 감추기 */
        ImageView userImage = (ImageView) mCurrentTalkView.findViewById(R.id.Image_User);
        userImage.setVisibility(View.INVISIBLE);

        /* 마이 아이콘 보이기 */
        ImageView maiImage = (ImageView) mCurrentTalkView.findViewById(R.id.Image_mAI);
        maiImage.setVisibility(View.VISIBLE);

        /* 메세지 출력 */
        TextView textMessage = (TextView) mCurrentTalkView.findViewById(R.id.Text_Message);
        TextView textTranslation = (TextView) mCurrentTalkView.findViewById(R.id.Text_Translation);
        textMessage.setText(sentence);
        textTranslation.setText(translation);

        /* 메세지 Layout 조정 */
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mCurrentTalkView.findViewById(R.id.Layer_Message).getLayoutParams();
        lp.addRule(RelativeLayout.RIGHT_OF, R.id.Image_mAI);
        lp.rightMargin = Utils.dp2px(mAI_Talk___Activity.this, 32);

        mLayer_Talk.addView(mCurrentTalkView);
    }

    void addUserTalk() {
        LayoutInflater inflater = (LayoutInflater) getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        mCurrentTalkView = inflater.inflate( R.layout.talk___row, null );

        /* 사용자 아이콘 감추기 */
        ImageView userImage = (ImageView) mCurrentTalkView.findViewById(R.id.Image_User);
        userImage.setVisibility(View.VISIBLE);

        /* 마이 아이콘 보이기 */
        ImageView maiImage = (ImageView) mCurrentTalkView.findViewById(R.id.Image_mAI);
        maiImage.setVisibility(View.INVISIBLE);

        /* 메세지 출력 */
        TextView textMessage = (TextView) mCurrentTalkView.findViewById(R.id.Text_Message);
        TextView textTranslation = (TextView) mCurrentTalkView.findViewById(R.id.Text_Translation);
//        textMessage.setText(sentence);
//        textTranslation.setText(translation);
        textMessage.setVisibility(View.GONE);
        textTranslation.setVisibility(View.GONE);

        /* 메세지 Layout 조정 */
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mCurrentTalkView.findViewById(R.id.Layer_Message).getLayoutParams();
        lp.addRule(RelativeLayout.LEFT_OF, R.id.Image_User);
        lp.leftMargin = Utils.dp2px(mAI_Talk___Activity.this, 32);

        mLayer_Talk.addView(mCurrentTalkView);
    }

    void setUserText(String text) {
        TextView textView = mCurrentTalkView.findViewById(R.id.Text_Message);
        textView.setText(text);
        textView.setVisibility(View.VISIBLE);
    }

    void stopProgress() {
        mCurrentTalkView.findViewById(R.id.Progress).setVisibility(View.GONE);
    }

    /* ###################################################################################################### */
    // AUDIO

    MediaPlayer mMediaPlayer;

    void initAudio() {
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mMediaPlayer.setOnCompletionListener(mOnCompletion_Audio);
        mMediaPlayer.setOnErrorListener(mOnError_Audio);
        mMediaPlayer.setOnPreparedListener(mOnPrepared_Audio);
    }

    MediaPlayer.OnCompletionListener mOnCompletion_Audio = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            switch(mState) {
                case STATE___BEGIN_SCENARIO:
                    stopProgress();
                    runNextScenario();
                    break;

                case STATE___END_SCENARIO:
                    stopProgress();
                    resultScenario();
                    break;

                case STATE___RUN_SCENARIO:
                    stopProgress();
                    runUserScenario();
                    break;

                case STATE___REACTION_INCORRECT:
                    mState = STATE___RUN_SCENARIO;
                    stopProgress();
                    runMaiScenario();
                    break;

                case STATE___REACTION_CORRECT:
                    stopProgress();
                    runNextScenario();
                    break;
            }
        }
    };

    MediaPlayer.OnErrorListener mOnError_Audio = new MediaPlayer.OnErrorListener() {
        @Override
        public boolean onError(MediaPlayer mp, int what, int extra) {
            Log.e("AAA", "Audio error................");

//            mOnCompletion_Audio.onCompletion(mp);
            return false;
        }
    };

    MediaPlayer.OnPreparedListener mOnPrepared_Audio = new MediaPlayer.OnPreparedListener() {
        @Override
        public void onPrepared(MediaPlayer mp) {
            mp.start();
        }
    };

    int setAudio(String audio_path) {
        try {
            mMediaPlayer.reset();
            Uri uri = Uri.parse(ConstDef.BASE_MAI_AUDIO_URL + audio_path);
            mMediaPlayer.setDataSource(mAI_Talk___Activity.this, uri);
            mMediaPlayer.prepareAsync();
        } catch (Exception e) {
            Log.e("AAA", "setAudio()........................................ media fail >> " + e.toString());
            return -1;
        }

        mHandler.sendEmptyMessage(MSG___DISABLE_MIC);
        return 0;
    }

    /* ###################################################################################################### */
    // SCENARIO

    List<String> mList_UserText = new ArrayList<String>();
    int mScenarioNo = -1;

    /* 모든 톡 정보를 삭제하고 대화를 다시 시작한다. */
    void retryScenario() {
        mLayer_Talk.removeAllViews();
        mScenarioNo = -1;
        mList_UserText.clear();
        mHandler.sendEmptyMessage(MSG___BEGIN_SCENARIO);

        /* 재시도 버턴 비활성화 */
        mButton_Retry.setEnabled(false);
    }

    void beginScenario() {
        mState = STATE___BEGIN_SCENARIO;
        addMaiTalk(mTalk.getBegin_sentence(), mTalk.getBeginTranslation(mAI_Talk___Activity.this));
        setAudio(mTalk.getBegin_voice_path());

        mHandler.post(new Runnable() {
            @Override
            public void run() { mScrollView.fullScroll(ScrollView.FOCUS_DOWN); }
        });
    }

    void endScenario() {
        mSttHandler.setIdle();
        mState = STATE___END_SCENARIO;
        addMaiTalk(mTalk.getEnd_sentence(), mTalk.getEndTranslation(mAI_Talk___Activity.this));
        setAudio(mTalk.getEnd_voice_path());

        mHandler.post(new Runnable() {
            @Override
            public void run() { mScrollView.fullScroll(ScrollView.FOCUS_DOWN); }
        });
    }

    void runNextScenario() {
        mState = STATE___RUN_SCENARIO;
        mScenarioNo++;
        if(mScenarioNo >= mTalk.getList_content().size()) {
            endScenario();
            return;
        }

        runMaiScenario();
    }

    void runMaiScenario() {
        addMaiTalk(mTalk.getList_content().get(mScenarioNo).getQuestion(), mTalk.getList_content().get(mScenarioNo).getTranslation(mAI_Talk___Activity.this));
        if(setAudio(mTalk.getList_content().get(mScenarioNo).getVoice_path()) < 0) {
//            stopProgress();
//            runUserScenario();
        }

        mHandler.post(new Runnable() {
            @Override
            public void run() { mScrollView.fullScroll(ScrollView.FOCUS_DOWN); }
        });
    }

    void runUserScenario() {
        addUserTalk();
        setHint(mTalk.getList_content().get(mScenarioNo).getHint());
        mHandler.sendEmptyMessage(MSG___OFF_MIC);

        mHandler.post(new Runnable() {
            @Override
            public void run() { mScrollView.fullScroll(ScrollView.FOCUS_DOWN); }
        });
    }

    void onSttResult(Map<String, Object> resultMap) {
        Map<String, Object> result = (Map<String, Object>) resultMap.get(Param.STT_RESULT);

        String answerText = (String) result.get(Param.STT_ANSWER_TEXT);
        String userText = (String) result.get(Param.STT_USER_TEXT);
        String userRecordPath = (String) result.get(Param.STT_RECORD_URL);
        int ascore = Integer.parseInt((String) result.get(Param.STT_GRAMMAR_SCORE));
        int pscore = Integer.parseInt((String) result.get(Param.STT_PRONOUNCE_SCORE));

        Log.e("AAA", String.format("onResult................................... %s / %d / %d", userText, ascore, pscore));
        setUserText(userText);
        stopProgress();
        if(mScenarioNo == mList_UserText.size()) mList_UserText.add(userText);
        else mList_UserText.set(mScenarioNo, userText);

        unsetHint();

        mAPI_mAITalk.checkAnswer(mTalk.getList_content().get(mScenarioNo).getContent_id(), userText, mRetryCnt).enqueue(new Callback<RestApiVO>() {
            @Override
            public void onResponse(@NonNull Call<RestApiVO> call, @NonNull Response<RestApiVO> response) {
                if (response.isSuccessful()) {
                    RestApiVO body = response.body();
                    if (body != null) {
                        if(body.getRes_code() == ResCode.SUCC) {
                            reaction(true, body.getReaction());
                            Log.e("AAA", "오~~~ 정답 입니다. ......");
                        } else {
                            reaction(false, body.getReaction());
                            Log.e("AAA", "에이~ 틀렸어요~~");
                        }
                    } else {
                        Toast.makeText(mAI_Talk___Activity.this, "(1) 서버와 연결이 원활하지 않습니다.", Toast.LENGTH_LONG).show();
                        runNextScenario();
                    }
                }
                else {
                    Toast.makeText(mAI_Talk___Activity.this, "(2) 서버와 연결이 원활하지 않습니다.", Toast.LENGTH_LONG).show();
                    runNextScenario();
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestApiVO> call, @NonNull Throwable t) {
                Log.e("AAA", "checkAnswer()................................... fail >> " + t.toString());
                Toast.makeText(mAI_Talk___Activity.this, "(3) 서버와 연결이 원활하지 않습니다.", Toast.LENGTH_LONG).show();
                runNextScenario();
            }
        });
    }

    void reaction(boolean correct_flag, ReactionVO reactionVO) {
        if(correct_flag == true) {
            mRetryCnt = 0;
            mState = STATE___REACTION_CORRECT;
        }
        else {
            mRetryCnt++;
            mState = STATE___REACTION_INCORRECT;
        }

        addMaiTalk(reactionVO.getSentence(), reactionVO.getTranslation(mAI_Talk___Activity.this));
        if(setAudio(reactionVO.getVoice_path()) < 0) {
//            stopProgress();
//            runUserScenario();
        }

        mHandler.post(new Runnable() {
            @Override
            public void run() { mScrollView.fullScroll(ScrollView.FOCUS_DOWN); }
        });
    }

    void resultScenario() {
        mSttHandler.setIdle();

        String message = "You said.\n\n";
        for(int xx = 0; xx < mList_UserText.size(); xx++) message += "" + (xx+1) + ". " + mList_UserText.get(xx) + "\n";
        message += "\nMore patterns are waiting for you!";
        addMaiTalk(message, "");
        stopProgress();
        mCurrentTalkView.findViewById(R.id.Text_Translation).setVisibility(View.GONE);

        mHandler.post(new Runnable() {
            @Override
            public void run() { mScrollView.fullScroll(ScrollView.FOCUS_DOWN); }
        });

        /* 재시도 버턴 활성화 */
        mButton_Retry.setEnabled(true);
    }

    /* ###################################################################################################### */
    // HINT

    View mDetailedHintView = null;
    TextView        mText_Hint;
    ImageView       mButton_ShowHint;
    ImageView       mButton_HideHint;

    void setHint(String hint) {
        addDetailedHint(hint);

        mScrollView.setSmoothScrollingEnabled(true);

        mHandler.post(new Runnable() {
            @Override
            public void run() { mScrollView.fullScroll(ScrollView.FOCUS_DOWN); }
        });
    }

    void unsetHint() {
        mLayer_Talk.removeView(mDetailedHintView);
        mDetailedHintView = null;

        mHandler.post(new Runnable() {
            @Override
            public void run() { mScrollView.fullScroll(ScrollView.FOCUS_DOWN); }
        });
    }

    void addDetailedHint(String hint) {
        LayoutInflater inflater = (LayoutInflater) getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        mDetailedHintView = inflater.inflate( R.layout.hint___sentence, null );
        mText_Hint = mDetailedHintView.findViewById(R.id.Text_Hint);
        mText_Hint.setText(hint);
        mText_Hint.setOnClickListener(mOnClick_HideHint);
        mText_Hint.setVisibility(View.INVISIBLE);

        mButton_ShowHint = mDetailedHintView.findViewById(R.id.Button_Hint);
        mButton_ShowHint.setVisibility(View.VISIBLE);
        mButton_ShowHint.setOnClickListener(mOnClick_ShowHint);

        mButton_HideHint = mDetailedHintView.findViewById(R.id.Button_Hint2);
        mButton_HideHint.setVisibility(View.INVISIBLE);
        mButton_HideHint.setOnClickListener(mOnClick_HideHint);

        mLayer_Talk.addView(mDetailedHintView);
    }

    View.OnClickListener mOnClick_ShowHint = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mButton_ShowHint.setVisibility(View.INVISIBLE);
            mButton_HideHint.setVisibility(View.VISIBLE);
            mText_Hint.setVisibility(View.VISIBLE);
        }
    };

    View.OnClickListener mOnClick_HideHint = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mButton_ShowHint.setVisibility(View.VISIBLE);
            mButton_HideHint.setVisibility(View.INVISIBLE);
            mText_Hint.setVisibility(View.INVISIBLE);
        }
    };

    /* ###################################################################################################### */
    // BUTTON

    @BindView(R.id.Button_Retry) RippleLayout mButton_Retry;
    @BindView(R.id.Button_Finish) RippleLayout mButton_Finish;

    void initButton() {
        mButton_Retry.setEnabled(false);
        mButton_Retry.setOnRippleCompleteListener(mOnClick_Retry);
        mButton_Finish.setOnRippleCompleteListener(mOnClick_Finish);
    }

    RippleLayout.OnRippleCompleteListener mOnClick_Retry = new RippleLayout.OnRippleCompleteListener() {
        @Override
        public void onComplete(RippleLayout rippleView) {
            retryScenario();
        }
    };

    RippleLayout.OnRippleCompleteListener mOnClick_Finish = new RippleLayout.OnRippleCompleteListener() {
        @Override
        public void onComplete(RippleLayout rippleView) {
            finish();
        }
    };




    /* ############################################################################################################ */
    /* 상단 공통 버턴 */

    @OnClick(R.id.Button_TopLogo)
    void onCLick_TopLog(View view) {
        Intent it = new Intent(this, Books___Activity.class);
        startActivity(it);
        finish();
    }

    @OnClick(R.id.Button_Hambuger)
    void onCLick_SideMenu(View view) {
        Intent it = new Intent(this, SideMenu___Activity.class);
        startActivity(it);
        finish();
    }
}
