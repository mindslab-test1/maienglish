package ai.mindslab.maienglish.Notice;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ai.mindslab.maienglish.Book.Books___Activity;
import ai.mindslab.maienglish.Common.ConstDef;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.ExternLib.RippleLayout;
import ai.mindslab.maienglish.Model.RestApiVO;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.RestAPI.BookAPI;
import ai.mindslab.maienglish.RestAPI.NoticeAPI;
import ai.mindslab.maienglish.RestAPI.ResCode;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Notice___Activity extends AppCompatActivity {
    final int PAGE_COUNT = 50;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notice___activity);
        ButterKnife.bind(this);
        Utils.setColor_StatusBarIcon(this, true, this.getResources().getColor(R.color.sys_bg___subscribe));

        initAPI();
        initTopLayer();
        initRecyclerView();

        getNoticeList();
    }

    /* ============================================================================================================================ */
    // TOP Layer
    /* ============================================================================================================================ */

    @BindView(R.id.Text_TopTitle) TextView mText_Title;
    @BindView(R.id.Button_Back) RippleLayout mButton_Back;

    void initTopLayer() {
        mText_Title.setText("공지 사항");

        mButton_Back.setOnRippleCompleteListener(mOnClick_Back);
    }

    RippleLayout.OnRippleCompleteListener mOnClick_Back = new RippleLayout.OnRippleCompleteListener() {
        @Override
        public void onComplete(RippleLayout rippleView) {
            finish();
        }
    };


    /* ============================================================================================================================ */
    // RecyclerView
    /* ============================================================================================================================ */

    @BindView(R.id.RecyclerView) RecyclerView mRecyclerView;

    List<NoticeItem> mList = new ArrayList<NoticeItem>();
    Notice_Adapter mAdapter;
    int mLastId = Integer.MAX_VALUE;

    void initRecyclerView() {
        mAdapter = new Notice_Adapter(mList);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setAdapter(mAdapter);
    }


    /* ============================================================================================================================ */
    // API
    /* ============================================================================================================================ */

    NoticeAPI mAPI_Notice;

    void initAPI() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConstDef.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mAPI_Notice = retrofit.create(NoticeAPI.class);
    }


    void getNoticeList() {
        mAPI_Notice.getNoticeList(SharedInfo.User.mAuthKey, mLastId, PAGE_COUNT).enqueue(new Callback<RestApiVO>() {
            @Override
            public void onResponse(@NonNull Call<RestApiVO> call, @NonNull Response<RestApiVO> response) {
                if(response.code() == 401) {
                    Toast.makeText(Notice___Activity.this, "인증이 되지 않았습니다. ", Toast.LENGTH_SHORT).show();
                    return;
                }
                RestApiVO body = response.body();
                if (body != null && body.getRes_code() == ResCode.SUCC) {
                    for(int xx = 0; xx < body.getNotice_list().size(); xx++) {
                        NoticeItem item = new NoticeItem();
                        item.setTitle(body.getNotice_list().get(xx).getTitle());
                        item.setCreate_datetime(body.getNotice_list().get(xx).getCreate_datetime().substring(0, 10));
                        item.setContents(body.getNotice_list().get(xx).getContents());
                        mList.add(item);
                    }
                    mAdapter.notifyDataSetChanged();

                    /* 공지사항 관련 정보 초기화 */
                    SharedInfo.Notice.mNewNoticeCount = 0;
                    String last_notice_date = Utils.getCurrentDateTime();
                    SharedPreferences sp = getSharedPreferences(ConstDef.SPREF_NAME, Activity.MODE_PRIVATE);
                    SharedPreferences.Editor ed = sp.edit();
                    ed.putString(ConstDef.SPREF_PARAM___LAST_NOTICE_DATE, last_notice_date);
                    ed.commit();

                    Log.e("AAA", "Saved Last date = " + last_notice_date);

                } else {
                    Toast.makeText(Notice___Activity.this, "네트워크가 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestApiVO> call, @NonNull Throwable t) {
                Toast.makeText(Notice___Activity.this, "네트워크가 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
