package ai.mindslab.maienglish.Notice;

import android.content.ClipData;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ai.mindslab.maienglish.R;

public class Notice_Adapter extends RecyclerView.Adapter<Notice_Adapter.NoticeViewHolder> {
    private List<NoticeItem> mList;

    public Notice_Adapter(List<NoticeItem> list) {
        mList = list;
    }

    @Override
    public NoticeViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View view = null;
        LayoutInflater inflater = null;

        inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.notice___item, parent, false);
        return new NoticeViewHolder(view);
    }

    public void onBindViewHolder(NoticeViewHolder holder, int position) {
        final NoticeItem item = mList.get(position);

        holder.mText_Title.setText(item.getTitle());
        holder.mText_Date.setText(item.getCreate_datetime());
        holder.mText_Message.setText(item.getContents());

        holder.mButton_Expand.setTag(R.string.tag___index, position);
        if(item.expand_flag == true) {
            holder.mText_Message.setVisibility(View.VISIBLE);
            holder.mButton_Expand.setImageResource(R.mipmap.arrow___to_up);
        }
        else {
            holder.mText_Message.setVisibility(View.GONE);
            holder.mButton_Expand.setImageResource(R.mipmap.arrow___to_down);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class NoticeViewHolder extends RecyclerView.ViewHolder {
        public TextView mText_Title;
        public TextView mText_Date;
        public ImageView mButton_Expand;
        public TextView mText_Message;

        public NoticeViewHolder(View itemView) {
            super(itemView);
            mText_Title = (TextView) itemView.findViewById(R.id.Text_Title);
            mText_Date = (TextView) itemView.findViewById(R.id.Text_Date);
            mButton_Expand = (ImageView) itemView.findViewById(R.id.Button_Expand);
            mText_Message = (TextView) itemView.findViewById(R.id.Text_Message);

            mButton_Expand.setOnClickListener(mOnClick_Expand);
        }
    }

    View.OnClickListener mOnClick_Expand = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (Integer) v.getTag(R.string.tag___index);
            mList.get(position).expand_flag = !mList.get(position).expand_flag;

            Notice_Adapter.this.notifyItemChanged(position);
        }
    };
}
