package ai.mindslab.maienglish.Notice;

import java.util.List;

import ai.mindslab.maienglish.Model.NoticeVO;
import lombok.Data;

public class NoticeItem extends NoticeVO {
    boolean expand_flag = false;
}
