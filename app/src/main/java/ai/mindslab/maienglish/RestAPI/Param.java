package ai.mindslab.maienglish.RestAPI;

public class Param {

    public static final String AUTH_KEY                     = "auth_key";

    public static final String BOOK_NO                      = "book_no";

    public static final String CONTENT_ID                   = "content_id";
    public static final String COUNT                        = "count";
    public static final String COUPON                       = "coupon";
    public static final String COUPON_LIST                  = "coupon_list";

    public static final String DATETIME                     = "datetime";

    public static final String EMAIL                        = "email";

    public static final String FREE_BOOK_COUNT              = "free_book_count";
    public static final String FREE_EXP_SURVEY              = "free_exp_survey";
    public static final String FREE_PATTERN_COUNT           = "free_pattern_count";

    public static final String LAST_DATETIME                = "last_datetime";
    public static final String LAST_ID                      = "last_id";

    public static final String MAI_TALK                     = "mai_talk";
    public static final String MAI_TALK_ID                  = "mai_talk_id";


    public static final String RES_CODE                     = "res_code";
    public static final String RETRY_COUNT                  = "retry_count";

    public static final String STT_ANSWER_TEXT              = "answerText";
    public static final String STT_RECORD_URL               = "recordUrl";
    public static final String STT_RES_CODE                 = "resCode";
    public static final String STT_RESULT                   = "result";
    public static final String STT_USER_TEXT                = "userText";
    public static final String STT_GRAMMAR_SCORE            = "grammarScore";
    public static final String STT_PRONOUNCE_SCORE          = "pronounceScore";
    public static final String STUDY_HISTORY_STAT           = "study_history_stat";

    public static final String STUDY_HISTORY                = "study_history";
    public static final String SUBS_FLAG                    = "subs_flag";
    public static final String SURVEY_URL                   = "survey_url";

    public static final String TITLE                        = "title";

    public static final String UNIT_NO                      = "unit_no";
    public static final String USER_TEXT                    = "user_text";
    public static final String URL                          = "url";
}
