package ai.mindslab.maienglish.RestAPI;

import ai.mindslab.maienglish.Model.RestApiVO;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CommonAPI {

    /* 최신 앱 버전 정보 조회 */
    @GET("/maieng/app/checkVersion")
    Call<RestApiVO> checkVersion(@Query(Param.LAST_DATETIME) String last_datetime);

    /* 최신 앱 버전 정보 조회 */
    @GET("/maieng/app/notification")
    Call<RestApiVO> getNotiAlarm(@Header(Param.AUTH_KEY) String auth_key, @Query("subs_flag") int subs_flag);

}
