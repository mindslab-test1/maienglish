package ai.mindslab.maienglish.RestAPI;

import java.util.ArrayList;
import java.util.List;

import ai.mindslab.maienglish.Model.BookVO;
import ai.mindslab.maienglish.Model.RestApiVO;
import ai.mindslab.maienglish.Model.UnitVO;
import ai.mindslab.maienglish.Model.mAITalkVO;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface UserAPI {
    @FormUrlEncoded
    @POST("/maieng/user")
    Call<RestApiVO> registerUser(@Field(Param.EMAIL) String email);

    @PUT("/maieng/user/login")
    Call<RestApiVO> login(@Header(Param.AUTH_KEY) String auth_key);

    @PUT("/maieng/user/logout")
    Call<RestApiVO> logout(@Header(Param.AUTH_KEY) String auth_key);

    @DELETE("/maieng/user")
    Call<RestApiVO> removeUser(@Header(Param.AUTH_KEY) String auth_key);
}
