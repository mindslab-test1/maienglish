package ai.mindslab.maienglish.RestAPI;

import java.util.List;

import ai.mindslab.maienglish.Model.RestApiVO;
import ai.mindslab.maienglish.Model.StudyResultVO;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface NoticeAPI {

    @GET("/maieng/notice/count")
    Call<RestApiVO> getNoticeCount(@Header(Param.AUTH_KEY) String auth_key, @Query(Param.LAST_DATETIME) String last_datetime);

    @GET("/maieng/notice")
    Call<RestApiVO> getNoticeList(@Header(Param.AUTH_KEY) String auth_key, @Query(Param.LAST_ID) int last_id, @Query(Param.COUNT) int page_count);
}
