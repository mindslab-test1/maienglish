package ai.mindslab.maienglish.RestAPI;

import java.util.ArrayList;
import java.util.List;

import ai.mindslab.maienglish.Model.BookVO;
import ai.mindslab.maienglish.Model.PatternContentVO;
import ai.mindslab.maienglish.Model.PatternVO;
import ai.mindslab.maienglish.Model.RestApiVO;
import ai.mindslab.maienglish.Model.UnitVO;
import ai.mindslab.maienglish.Model.mAITalkVO;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PatternAPI {

    @GET("/maieng/units/{unit_id}/patterns")
    Call<RestApiVO> getPatternList(@Header(Param.AUTH_KEY) String auth_key,
                                   @Path("unit_id") int unit_id,
                                   @Query(Param.BOOK_NO) int book_no,
                                   @Query(Param.UNIT_NO) int unit_no ,
                                   @Query(Param.SUBS_FLAG) int subs_flag);

}
