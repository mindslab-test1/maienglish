package ai.mindslab.maienglish.RestAPI;

import ai.mindslab.maienglish.Model.RestApiVO;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CouponAPI {

    /* 미사용된 쿠폰 개수 조회 */
    @GET("/maieng/coupons/count")
    Call<RestApiVO> getCouponCount(@Header(Param.AUTH_KEY) String auth_key);

    /* 쿠폰 목록 조회 */
    @GET("/maieng/coupons")
    Call<RestApiVO> getCouponList(@Header(Param.AUTH_KEY) String auth_key);

    /* 쿠폰 등록 */
    @PUT("/maieng/coupons/{publishing_id}/register")
    Call<RestApiVO> registerCoupon(@Header(Param.AUTH_KEY) String auth_key, @Path("publishing_id") String publishing_id);

    /* 쿠폰 사용 */
    @PUT("/maieng/coupons/{publishing_id}/use")
    Call<RestApiVO> useCoupon(@Header(Param.AUTH_KEY) String auth_key, @Path("publishing_id") String publishing_id);

    /* 쿠폰 발행 */
    @POST("/maieng/coupons/{coupon_id}")
    Call<RestApiVO> publishCoupon(@Header(Param.AUTH_KEY) String auth_key, @Path("coupon_id") int coupon_id);
}
