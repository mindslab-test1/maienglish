package ai.mindslab.maienglish.RestAPI;

import java.util.List;

import ai.mindslab.maienglish.Model.PatternContentVO;
import ai.mindslab.maienglish.Model.PatternVO;
import ai.mindslab.maienglish.Model.RestApiVO;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ContentAPI {

    @GET("/maieng/patterns/{pattern_id}/contents")
    Call<RestApiVO> getContentList(@Header(Param.AUTH_KEY) String auth_key, @Path("pattern_id") int pattern_id);

















//
//
//
//
//    /* ############################################################################################################ */
//    /* SAMPLE - PATTERN */
//
//    static List<PatternVO> samplePatternList() {
//        List<PatternVO> list = new ArrayList<PatternVO>();
//
//        list.add(samplePattern(1, 1, "I __________.", 1));
//        list.add(samplePattern(2, 2, "She __________.", 1));
//        list.add(samplePattern(3, 3, "I do not __________.", 0));
//        list.add(samplePattern(4, 4, "Do you __________?", 0));
//        list.add(samplePattern(5, 5, "Does he __________?.", 0));
//        list.add(samplePattern(6, 6, "I will __________.", 0));
//        list.add(samplePattern(7, 7, "She will __________.", 0));
//        list.add(samplePattern(8, 8, "I will not __________.", 0));
//        list.add(samplePattern(9, 9, "He will not __________.", 0));
//        list.add(samplePattern(10, 10, "Will you __________?", 0));
//        list.add(samplePattern(11, 11, "Will she __________?", 0));
//        list.add(samplePattern(12, 12, "I __________-ed.", 0));
//        list.add(samplePattern(13, 13, "She drove.", 0));
//        list.add(samplePattern(14, 14, "I did not __________.", 0));
//        list.add(samplePattern(15, 15, "Suji did not __________.", 0));
//        list.add(samplePattern(16, 16, "Did you __________?", 0));
//        list.add(samplePattern(17, 17, "Did the dog __________?", 0));
//        list.add(samplePattern(18, 18, "I work __________.", 0));
//        list.add(samplePattern(19, 19, "She works __________.", 0));
//        list.add(samplePattern(20, 20, "I worked __________.", 0));
//        list.add(samplePattern(21, 21, "She worked __________.", 0));
//        list.add(samplePattern(22, 22, "I will work __________.", 0));
//        list.add(samplePattern(23, 23, "She will work __________.", 0));
//        list.add(samplePattern(24, 24, "I work __________.", 0));
//        list.add(samplePattern(25, 25, "She acts __________.", 0));
//        list.add(samplePattern(26, 26, "I don't walk __________.", 0));
//        list.add(samplePattern(27, 27, "She doesn't act __________.", 0));
//        list.add(samplePattern(28, 28, "Do you study __________?", 0));
//        list.add(samplePattern(29, 29, "Does she act __________?", 0));
//        list.add(samplePattern(30, 30, "I worked __________.", 0));
//        list.add(samplePattern(31, 31, "She worked __________.", 0));
//        list.add(samplePattern(32, 32, "I didn't study __________.", 0));
//        list.add(samplePattern(33, 33, "She didn't study __________.", 0));
//        list.add(samplePattern(34, 34, "Did you work _____ _____?", 0));
//        list.add(samplePattern(35, 35, "Did Paul work _____ _____?", 0));
//        list.add(samplePattern(36, 36, "I am __________.", 0));
//        list.add(samplePattern(37, 37, "She is __________.", 0));
//        list.add(samplePattern(38, 38, "I am not __________.", 0));
//        list.add(samplePattern(39, 39, "She is not __________.", 0));
//        list.add(samplePattern(40, 40, "Are you __________?", 0));
//        list.add(samplePattern(41, 41, "Is she __________?", 0));
//
//        return list;
//    }
//
//    static PatternVO samplePattern(long patternId, int patternNo, String title, int enableFlag) {
//        PatternVO pattern = new PatternVO();
//        pattern.set_patternId(patternId);
//        pattern.set_patternNo(patternNo);
//        pattern.set_title(title);
//        pattern.set_enableFlag(enableFlag);
//
//        return pattern;
//    }
//
//
//    /* ############################################################################################################ */
//    /* SAMPLE - CONTENT LIST */
//
//    static List<PatternContentVO> samplePatternContentList() {
//        List<PatternContentVO> list = new ArrayList<PatternContentVO>();
//
//        list.add(samplePatternContent(1, "", "", "I work.", "나는 일합니다.", 1, "I ________.", "work / run / exercise", "work / I", "", ""));
//        list.add(samplePatternContent(2, "", "", "I run.", "나는 뜁니다.", 1, "I ________.", "work / run / exercise", "run / I", "", ""));
//        list.add(samplePatternContent(3, "", "", "I exercise.", "나는 운동합니다.", 1, "I ________.", "work / run / exercise", "exercise / I", "", ""));
//        return list;
//    }
//
//    static PatternContentVO samplePatternContent(long contentId, String question, String questionTrans, String answer, String answerTrans, int type, String practiceSentence, String hintPrac, String hintChall, String maiVoiceUrl, String userVoiceUrl) {
//        PatternContentVO content = new PatternContentVO();
//        content.set_contentId(contentId);
//        content.get_sentenceList()[0] = question;
//        content.get_translationList()[0] = questionTrans;
//        content.get_sentenceList()[1] = answer;
//        content.get_translationList()[1] = answerTrans;
//        content.set_type(type);
//        content.set_practiceSentence(practiceSentence);
//        content.set_hintPractice(hintPrac);
//        content.set_hintChallenge(hintChall);
//        content.set_maiVoiceUrl(maiVoiceUrl);
//        content.set_userVoiceUrl(userVoiceUrl);
//
//        return content;
//    }
}
