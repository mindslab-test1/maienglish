package ai.mindslab.maienglish.RestAPI;

public class ResCode {
    public static final int SUCC = 0;

    public static final int FAIL = -1;                      // 일반적인 에러는 모두 이걸로 반환

    public static final int UNAUTH = -401;                  // 인증 실패
    public static final int NOT_FOUND = -404;               // 데이타 검색 실패

    public static final int ALREADY = -601;                 // 이미.... 존재? 삭제? 등록?....
    public static final int EXPIRED = -602;                 // 이미.... 만료
    public static final int NOT_YET = -603;                 // 아직 준비되지 않았음.
    public static final int DUPLICATED = -604;              // 중복. (무료 쿠폰 이중 등록 방지 등)

    public static final int TOO_MANY_RESULT = -1001;        // 결과가 많음.
    public static final int TOO_MANY_PARAM = -1002;         // 파라메타가 많음 (복수 등)
}
