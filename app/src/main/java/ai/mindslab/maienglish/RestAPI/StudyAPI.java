package ai.mindslab.maienglish.RestAPI;

import java.util.List;

import ai.mindslab.maienglish.Model.RestApiVO;
import ai.mindslab.maienglish.Model.StudyHistoryVO;
import ai.mindslab.maienglish.Model.StudyResultVO;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface StudyAPI {

    @Headers("Content-Type: application/json")
    @POST("/maieng/study")
    Call<RestApiVO> addStudyHistory(@Header(Param.AUTH_KEY) String auth_key, @Body List<StudyResultVO> study_history);

    @GET("/maieng/study/history/stat")
    Call<RestApiVO> getStudyHistoryStat(@Header(Param.AUTH_KEY) String auth_key);

    @GET("/maieng/study/history/daily/{date}")
    Call<RestApiVO> getStudyHistoryContent(@Header(Param.AUTH_KEY) String auth_key, @Path("date") String date);
}
