package ai.mindslab.maienglish.RestAPI;

import java.util.ArrayList;
import java.util.List;

import ai.mindslab.maienglish.Model.BookVO;
import ai.mindslab.maienglish.Model.RestApiVO;
import ai.mindslab.maienglish.Model.UnitVO;
import ai.mindslab.maienglish.Model.mAITalkVO;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface BookAPI {

    @GET("/maieng/books")
    Call<RestApiVO> getBookList(@Header(Param.AUTH_KEY) String auth_key, @Query(Param.SUBS_FLAG) int subs_flag);



















//    /* ############################################################################################################ */
//    /* SAMPLE - BOOK */
//
//    static List<BookVO> sampleBookList() {
//        List<BookVO> list = new ArrayList<BookVO>();
//
//        list.add(sampleBook(1, "진짜진짜 쉬운\n문장 구조 만들기", 0xFF13c0d7, sampleUnitList(), sampleTalkList()));
//        list.add(sampleBook(2, "조금 더\n많은 정보 담기", 0xFF40a8f5, sampleUnitList(), sampleTalkList()));
//        list.add(sampleBook(3, "복잡한 구조\n쉽게 만들기", 0xFF6b78d7, sampleUnitList(), sampleTalkList()));
//        list.add(sampleBook(4, "다양한 표현으로\n문장 만들기", 0xFF0a253e, sampleUnitList(), sampleTalkList()));
//
//        return list;
//    }
//
//    static BookVO sampleBook(long bookId, String title, int color, List<UnitVO> listUnit, List<mAITalkVO> listTalk) {
//        BookVO book = new BookVO();
//        book.set_bookId(bookId);
//        book.set_title(title);
//        book.set_color(color);
//        book.set_listUnit(listUnit);
//        book.set_listTalk(listTalk);
//
//        return book;
//    }
//
//    /* ############################################################################################################ */
//    /* SAMPLE - UNIT */
//
//    static List<UnitVO> sampleUnitList() {
//        List<UnitVO> list = new ArrayList<UnitVO>();
//
//        list.add(sampleUnit(1, 1, "아주 간단한 문장", 0xFF13c0d7, 1, 1));
//        list.add(sampleUnit(1, 2, "be동사와 친구들", 0xFF40a8f5, 0, 0));
//        list.add(sampleUnit(1, 3, "'무엇을' 붙여볼까?", 0xFF6b78d7, 0, 0));
//        list.add(sampleUnit(1, 4, "'~에게' 붙여볼까?", 0xFF0a253e, 0, 0));
//
//        return list;
//    }
//
//    static UnitVO sampleUnit(long unitId, int unitNo, String title, int color, int enableFlag, int freeFlag) {
//        UnitVO unit = new UnitVO();
//        unit.set_unitId(1);
//        unit.set_unitNo(unitNo);
//        unit.set_title(title);
//        unit.set_color(color);
//        unit.set_enableFlag(freeFlag);
//        unit.set_freeFlag(freeFlag);
//
//        return unit;
//    }
//
//    /* ############################################################################################################ */
//    /* SAMPLE - mAI Talk */
//
//    static List<mAITalkVO> sampleTalkList() {
//        List<mAITalkVO> list = new ArrayList<mAITalkVO>();
//
//        list.add(sampleTalk(1, 1));
//        list.add(sampleTalk(1, 0));
//        list.add(sampleTalk(1, 0));
//        list.add(sampleTalk(1, 0));
//
//        return list;
//    }
//
//    static mAITalkVO sampleTalk(int talkId, int enableFlag) {
//        mAITalkVO talk = new mAITalkVO();
//        talk.setMai_talk_id(talkId);
//        talk.setEnable_flag(enableFlag);
//
//        return talk;
//    }
}
