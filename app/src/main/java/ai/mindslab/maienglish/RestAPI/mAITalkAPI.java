package ai.mindslab.maienglish.RestAPI;

import ai.mindslab.maienglish.Model.RestApiVO;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface mAITalkAPI {

    @GET("/maieng/maitalk/{mai_talk_id}/scenario")
    Call<RestApiVO> getScenario(@Path(Param.MAI_TALK_ID) int mai_talk_id);

    @GET("/maieng/maitalk/{content_id}/checkAnswer")
    Call<RestApiVO> checkAnswer(@Path(Param.CONTENT_ID) int mai_talk_id,
                                @Query(Param.USER_TEXT) String user_text,
                                @Query(Param.RETRY_COUNT) int retry_count);




















    /* ############################################################################################################ */
    /* SAMPLE - PATTERN */

//    static mAITalkVO sampleTalk() {
//        mAITalkVO  talk = new mAITalkVO();
//
//        talk.set_imageUrl("http://10.122.66.51/resources/images/mAITalk/talk_1.png");
//        talk.set_startSentence("Hi, I'm mAI. This is my English class.");
//        talk.set_startTrans("안녕하세요. 저는 마이입니다. 여기는 저의 영어 교실입니다.");
//        talk.set_startSoundUrl("");
//        talk.set_endSentence("See you again soon!");
//        talk.set_endTrans("다음에 또 만나요.");
//        talk.set_endSoundUrl("");
//        talk.get_listContent().add(sampleTalkContent("Do they study hard?", "그들은 공부를 열심히 하나요?", "Yes, they study hard.", new String[] {"%they study%"}));
//        talk.get_listContent().add(sampleTalkContent("Do they speak politely?", "그들은 예의바르게 말을 하나요?", "Yes, they speak politely.", new String[] {"%they speak%"}));
//        talk.get_listContent().add(sampleTalkContent("Will you join us?", "당신도 우리와 함께 할래요?", "Yes, I will join you.", new String[] {"%i will join%"}));
//
//        return talk;
//    }
//
//    static mAITalkContentVO sampleTalkContent(String question, String questionTrans, String hint, String[] arrayAnswer) {
//        mAITalkContentVO content = new mAITalkContentVO();
//        content.set_question(question);
//        content.set_questionTrans(questionTrans);
//        content.set_hint(hint);
//
//        for(String answer : arrayAnswer) content.get_listAnswerPattern().add(answer);
//
//        return content;
//    }
}
