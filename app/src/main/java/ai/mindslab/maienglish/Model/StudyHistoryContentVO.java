package ai.mindslab.maienglish.Model;

import lombok.Data;

import java.util.List;

/*
** 문장 단위의 학습 이력
*/
@Data
public class StudyHistoryContentVO {
    int pattern_id;
    int pattern_no;
    String pattern_title;
    int content_id;
    int type;
    String study_datetime;
    String sentence;
    String user_text;
    int grade;
    int ascore;
    int pscore;
}
