package ai.mindslab.maienglish.Model;

import lombok.Data;

import java.util.List;

@Data
public class StudyHistoryDayStatVO {
    String date_str;
    int num_pattern;            // 학습 패턴 수
    int ascore;                 // 하루 평균
    int pscore;                 // 하루 평균 발음 점수
}
