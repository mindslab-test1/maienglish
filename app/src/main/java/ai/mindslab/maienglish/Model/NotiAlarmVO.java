package ai.mindslab.maienglish.Model;

import lombok.Data;

/*
** 안드로이드 상단 알림 메세지
*/
@Data
public class NotiAlarmVO {
    private String title;
    private String message;
}
