package ai.mindslab.maienglish.Model;

import android.content.Context;

import ai.mindslab.maienglish.Common.Utils;
import lombok.Data;

@Data
public class PatternContentVO {
    private int                     content_id;

    private String                  sentence;
    private String                  sentence_eval;
    private String                  translation;
    private String                  translation_zh;
    private String                  voice_path;

    private String                  sentence_practice;

    private String                  hint_practice;
    private String                  hint_challenge;

    private int                     pattern_id;

    public String getTranslation(Context ctx) {
        if(Utils.getSystemLanguage(ctx).equals("zh")) return translation_zh;
        else return translation;
    }
}


