package ai.mindslab.maienglish.Model;

import lombok.Data;

@Data
public class AdvertisementVO {
    private String              img_path;
    private String              color;
}
