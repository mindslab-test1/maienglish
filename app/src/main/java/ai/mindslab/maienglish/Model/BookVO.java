package ai.mindslab.maienglish.Model;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.Model.UnitVO;
import lombok.Data;

@Data
public class BookVO {
    private int                     book_id;
    private String                  title;
    private String                  title_zh;
    private String                  color;

    private List<UnitVO>            list_unit = new ArrayList<UnitVO>();
    private List<mAITalkVO>         list_mai_talk = new ArrayList<mAITalkVO>();

    public String getTitle(Context ctx) {
        if(Utils.getSystemLanguage(ctx).equals("zh")) return title_zh;
        else return title;
    }
}
