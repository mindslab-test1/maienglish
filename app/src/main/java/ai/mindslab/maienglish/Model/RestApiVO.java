package ai.mindslab.maienglish.Model;

import java.util.List;

import lombok.Data;

@Data
public class RestApiVO {
    Integer                 res_code;

    mAITalkVO               mai_talk;

    AppVersionVO            app_version;
    List<AdvertisementVO>   ad_list;
    Integer                 subs_price;
    String                  support_email;
    String                  support_phone;
    Integer                 notice_count;
    Integer                 free_book_count;
    Integer                 free_pattern_count;
    String                  survey_url;

    String                  datetime;   // 사용자 등록 날짜, 시간
    String                  auth_key;

    Integer                 free_exp_survey;       // 무료 체험 설문 조사 대상 여부

    List<BookVO>            book_list;

    List<PatternVO>         pattern_list;

    List<PatternContentVO>  content_list;

    ReactionVO              reaction;

    List<NoticeVO>          notice_list;

    List<CouponVO>          coupon_list;

    CouponVO                coupon;

    Integer                 coupon_count;

    StudyHistoryStatVO      study_history_stat;

    List<StudyHistoryContentVO> study_history_list;

    NotiAlarmVO             noti_alarm;
}
