package ai.mindslab.maienglish.Model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class
StudyHistoryVO {
    List<StudyResultVO>         repeat_list;
    List<StudyResultVO>         practice_list;
    List<StudyResultVO>         challenge_list;
}
