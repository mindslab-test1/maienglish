package ai.mindslab.maienglish.Model;

import java.io.Serializable;

import lombok.Data;

@Data
public class CouponVO implements Serializable {
    private int                     coupon_id;
    private String                  publishing_id;
    private String                  title;

    private int                     coupon_type;        // 1: 무료쿠폰

    private String                  begin_date;         // 유효 날짜
    private String                  end_date;

    private String                  publish_datetime;    // 발행일

    private int                     free_days;
    private String                  use_datetime;       // 쿠폰 사용 시작일
    private String                  expire_datetime;    // 쿠폰 사용 종료일

    private int                     state;              // 0:미등록, 1:등록, 2:사용, 3:기한만료

    private String                  color;              // 쿠폰 색상


    public void set(CouponVO coupon) {
        this.coupon_id = coupon.coupon_id;
        this.publishing_id = coupon.publishing_id;
        this.title = coupon.title;
        this.coupon_type = coupon.coupon_type;
        this.begin_date = coupon.begin_date;
        this.end_date = coupon.end_date;
        this.publish_datetime = coupon.publish_datetime;
        this.free_days = coupon.free_days;
        this.use_datetime = coupon.use_datetime;
        this.expire_datetime = coupon.expire_datetime;
        this.state = coupon.state;
        this.color = coupon.color;
    }
}
