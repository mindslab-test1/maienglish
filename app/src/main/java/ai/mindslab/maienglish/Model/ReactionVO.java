package ai.mindslab.maienglish.Model;

import android.content.Context;

import ai.mindslab.maienglish.Common.Utils;
import lombok.Data;

@Data
public class ReactionVO {
    private String sentence;
    private String translation;
    private String translation_zh;
    private String voice_path;

    public String getTranslation(Context ctx) {
        if(Utils.getSystemLanguage(ctx).equals("zh")) return translation_zh;
        else return translation;
    }
}
