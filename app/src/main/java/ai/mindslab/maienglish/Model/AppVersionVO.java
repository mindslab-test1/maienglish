package ai.mindslab.maienglish.Model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class AppVersionVO {
    private int                 major_ver;
    private int                 minor_ver;
    private int                 patch_ver;

    private String              description;
    private String              store_url;
}
