package ai.mindslab.maienglish.Model;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import ai.mindslab.maienglish.Common.Utils;
import lombok.Data;

@Data
public class mAITalkVO {
    private int                     mai_talk_id;

    private int                     enable_flag;

    private String                  img_path;

    private String                  begin_sentence;
    private String                  begin_translation;
    private String                  begin_translation_zh;
    private String                  begin_voice_path;

    private String                  end_sentence;
    private String                  end_translation;
    private String                  end_translation_zh;
    private String                  end_voice_path;

    private List<mAITalkContentVO>  list_content = new ArrayList<mAITalkContentVO>();

    public String getBeginTranslation(Context ctx) {
        if(Utils.getSystemLanguage(ctx).equals("zh")) return begin_translation_zh;
        else return begin_translation;
    }

    public String getEndTranslation(Context ctx) {
        if(Utils.getSystemLanguage(ctx).equals("zh")) return end_translation_zh;
        else return end_translation;
    }
}
