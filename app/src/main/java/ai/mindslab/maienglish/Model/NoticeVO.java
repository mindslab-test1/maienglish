package ai.mindslab.maienglish.Model;

import lombok.Data;

@Data
public class NoticeVO {
    private String          title;
    private String          contents;
    private String          create_datetime;
}
