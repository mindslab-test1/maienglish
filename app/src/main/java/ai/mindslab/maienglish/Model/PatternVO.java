package ai.mindslab.maienglish.Model;

import android.content.Context;

import java.util.ArrayList;

import ai.mindslab.maienglish.Common.Utils;
import lombok.Data;

@Data
public class PatternVO {
    private int                     pattern_id;
    private int                     order_no;

    private String                  sentence;
    private String                  translation;
    private String                  translation_zh;

    private int                     enable_flag;
    private int                     studied_flag;

    public String getTranslation(Context ctx) {
        if(Utils.getSystemLanguage(ctx).equals("zh")) return translation_zh;
        else return translation;
    }
}
