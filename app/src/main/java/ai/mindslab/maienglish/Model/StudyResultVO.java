package ai.mindslab.maienglish.Model;

import lombok.Data;

@Data
public class StudyResultVO {
    private int                     content_id;
    private int                     pattern_id;
    private int                     type;
    private int                     grade;          // 등급: 0, 1, 2, 3, 4, 5
    private int                     ascore;         // 문장 점수
    private int                     pscore;         // 발음 점수
    private String                  user_text;
    private String                  sentence;

    public static final int TYPE___REPEAT = 1;
    public static final int TYPE___PRACTICE = 2;
    public static final int TYPE___CHALLENGE = 3;

    static final int GRADE___FANTASTIC = 8;
    static final int GRADE___AMAZING = 7;
    static final int GRADE___EXCELLENT = 6;
    static final int GRADE___GREAT = 5;
    static final int GRADE___GOOD = 4;
    static final int GRADE___KEEP_IT_UP = 3;
    static final int GRADE___NOT_BAD = 2;
    static final int GRADE___GOOD_TRY = 1;

    public static int calcGrade(int ascore, int pscore) {
        if(ascore == 100) {
            if(pscore == 100) return GRADE___FANTASTIC;
            else if(pscore >= 90 && pscore <= 99) return GRADE___AMAZING;
            else if(pscore >= 80 && pscore <= 89) return GRADE___EXCELLENT;
            else if(pscore >= 70 && pscore <= 79) return GRADE___EXCELLENT;
            else if(pscore <= 69) return GRADE___EXCELLENT;
        }
        else if(ascore >= 90 && ascore <= 99) {
            if(pscore == 100) return GRADE___FANTASTIC;
            else if(pscore >= 90 && pscore <= 99) return GRADE___AMAZING;
            else if(pscore >= 80 && pscore <= 89) return GRADE___EXCELLENT;
            else if(pscore >= 70 && pscore <= 79) return GRADE___EXCELLENT;
            else if(pscore <= 69) return GRADE___GREAT;
        }
        else if(ascore >= 90 && ascore <= 99) {
            if(pscore == 100) return GRADE___EXCELLENT;
            else if(pscore >= 90 && pscore <= 99) return GRADE___GREAT;
            else if(pscore >= 80 && pscore <= 89) return GRADE___GOOD;
            else if(pscore >= 70 && pscore <= 79) return GRADE___GOOD;
            else if(pscore <= 69) return GRADE___KEEP_IT_UP;
        }
        else if(ascore >= 90 && ascore <= 99) {
            if(pscore == 100) return GRADE___GOOD;
            else if(pscore >= 90 && pscore <= 99) return GRADE___GOOD;
            else if(pscore >= 80 && pscore <= 89) return GRADE___KEEP_IT_UP;
            else if(pscore >= 70 && pscore <= 79) return GRADE___KEEP_IT_UP;
            else if(pscore <= 69) return GRADE___NOT_BAD;
        }
        else if(ascore <= 69) {
            if(pscore == 100) return GRADE___KEEP_IT_UP;
            else if(pscore >= 90 && pscore <= 99) return GRADE___KEEP_IT_UP;
            else if(pscore >= 80 && pscore <= 89) return GRADE___NOT_BAD;
            else if(pscore >= 70 && pscore <= 79) return GRADE___GOOD_TRY;
            else if(pscore <= 69) return GRADE___GOOD_TRY;
        }
        return GRADE___GOOD_TRY;
    }

    public static String convGrade(int grade) {
        switch(grade) {
            case 1: return "Good try!";
            case 2: return "Not bad";
            case 3: return "Keep it up!";
            case 4: return "Good!";
            case 5: return "Great!";
            case 6: return "Excellent!";
            case 7: return "Amazing!";
            case 8: return "Fantastic!";
            default:return "Good try!";
        }
    }
}
