package ai.mindslab.maienglish.Model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class StudyHistoryStatVO {
    float studied_patterns_rate = 0;
    float ascore = 0;
    float pscore = 0;

    List<StudyHistoryDayStatVO> daily_list = new ArrayList<>();         // 매일 학습 통계
}
