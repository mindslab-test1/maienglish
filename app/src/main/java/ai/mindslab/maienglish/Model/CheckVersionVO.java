package ai.mindslab.maienglish.Model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class CheckVersionVO {
    AppVersionVO            app_version;
    List<String>            list_ad;
    int                     subs_price;
}
