package ai.mindslab.maienglish.Model;

import android.content.Context;

import ai.mindslab.maienglish.Common.Utils;
import lombok.Data;

@Data
public class mAITalkContentVO {
    private int         content_id;

    private int         mai_talk_id;

    private int         order_no;

    private String      question;
    private String      translation;
    private String      translation_zh;
    private String      voice_path;

    private String      hint;

    public String getTranslation(Context ctx) {
        if(Utils.getSystemLanguage(ctx).equals("zh")) return translation_zh;
        else return translation;
    }
}
