package ai.mindslab.maienglish.Model;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import ai.mindslab.maienglish.Common.Utils;
import lombok.Data;

@Data
public class UnitVO {
    private int             unit_id;
    private int             unit_no;
    private String          title;
    private String          title_zh;
    private String          color;
    private int             enable_flag;

    private List<PatternVO> list_pattern = new ArrayList<PatternVO>();

    public String getTitle(Context ctx) {
        if(Utils.getSystemLanguage(ctx).equals("zh")) return title_zh;
        else return title;
    }
}
