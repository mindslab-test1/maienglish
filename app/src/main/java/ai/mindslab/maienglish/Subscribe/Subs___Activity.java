package ai.mindslab.maienglish.Subscribe;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import java.util.concurrent.TimeUnit;

import ai.mindslab.maienglish.Book.Books___Activity;
import ai.mindslab.maienglish.Common.ConstDef;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.ExternLib.RippleLayout;
import ai.mindslab.maienglish.Model.RestApiVO;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.Realm.UserFactory;
import ai.mindslab.maienglish.RestAPI.ResCode;
import ai.mindslab.maienglish.RestAPI.UserAPI;
import ai.mindslab.maienglish.SchedulerTask.Worker_FreeCoupon;
import ai.mindslab.maienglish.SideMenu.SideMenu___Activity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Subs___Activity extends AppCompatActivity implements BillingProcessor.IBillingHandler {
    final int RC___GOOGLE_LOGIN = 100;

    private Realm realm;
    private BillingProcessor mBilling;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subs___activity);
        ButterKnife.bind(this);
        Utils.setColor_StatusBarIcon(this, true, this.getResources().getColor(R.color.sys_bg___subscribe));

        Realm.init(this);
        realm = Realm.getDefaultInstance();

        initAPI();
        initTopButtons();
        initGoogleLogin();

        mBilling = new BillingProcessor(this, ConstDef.BILLING_LICENSE_KEY, this);
        mBilling.initialize();
    }

    @Override
    protected void onStart() {
        super.onStart();

        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);

    }

    @Override
    public void onBackPressed() {
        Intent it = new Intent();
        setResult(RESULT_CANCELED, it);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // 구글 로그인 결과
        if (requestCode == RC___GOOGLE_LOGIN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }

        if (!mBilling.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onDestroy() {
        if (mBilling != null) {
            mBilling.release();
        }
        super.onDestroy();
    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
        Log.e("AAA", "[ Subs ] ....................................... onProductPurchased() >> " + details.toString());
        SharedInfo.Subscription.mSubsFlag = 1;
        Intent it = new Intent();
        setResult(RESULT_OK, it);
        finish();
    }

    @Override
    public void onPurchaseHistoryRestored() {
        Log.e("AAA", "[ Subs ] ....................................... onPurchaseHistoryRestored()");
    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {
        Log.e("AAA", "[ Subs ] ....................................... onBillingError() >> " + errorCode);
        SharedInfo.Subscription.mSubsFlag = 0;
        Intent it = new Intent();
        setResult(RESULT_CANCELED, it);
        finish();
    }

    @Override
    public void onBillingInitialized() {
        Log.e("AAA", "[ Subs ] ....................................... onBillingInitialized()");
    }

    @OnClick(R.id.Button_Subs)
    public void onClick_Subs(View v) {
        Log.e("AAA", "[ Subs ] ....................................... onClick_Subs()");

        if(SharedInfo.User.isLogined() == false) goGoogleLogin();
        else mBilling.subscribe(this, ConstDef.SUBS_CODE);
    }



    /* =================================================================================================================================== */
    // 로그인
    /* =================================================================================================================================== */


    GoogleSignInClient mGoogleSignInClient;

    /* 구글 로그인 초기화 */
    void initGoogleLogin() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    void goGoogleLogin() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC___GOOGLE_LOGIN);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            /* 구글에서 수신한 정보 등록 */
            SharedInfo.User.mName = account.getDisplayName();
            SharedInfo.User.mEmail = account.getEmail();
            if(account.getPhotoUrl() != null) SharedInfo.User.mPhotoUrl = account.getPhotoUrl().toString();

            /* 서버에 정보 저장 */
            mAPI_User.registerUser(SharedInfo.User.mEmail).enqueue(new Callback<RestApiVO>() {
                @Override
                public void onResponse(@NonNull Call<RestApiVO> call, @NonNull Response<RestApiVO> response) {
                    RestApiVO body = response.body();
                    if (body != null && body.getRes_code() == ResCode.SUCC) {
                        /* 인증 정보 등록 */
                        UserFactory.login(realm, SharedInfo.User.mEmail, SharedInfo.User.mName, SharedInfo.User.mPhotoUrl, body.getAuth_key());
                        SharedInfo.User.mAuthKey = body.getAuth_key();
                        SharedInfo.User.mCreateDate = body.getDatetime();

                        SharedInfo.Coupon.mFreeCoupon = body.getCoupon();
                        if(SharedInfo.Coupon.getFreeCoupon() != null) Worker_FreeCoupon.build();

                        Toast.makeText(Subs___Activity.this, "로그인 되셨습니다.", Toast.LENGTH_SHORT).show();

                        if(mBilling.subscribe(Subs___Activity.this, ConstDef.SUBS_CODE) == false) {
                            Toast.makeText(Subs___Activity.this, "구독 서비스를 위한 초기화가 되지 않았습니다.", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        SharedInfo.User.reset();
                        Toast.makeText(Subs___Activity.this, "(1) 서버와 연결이 원활하지 않아서\n로그인이 되지 않았습니다.", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<RestApiVO> call, @NonNull Throwable t) {
                    SharedInfo.User.reset();
                    Toast.makeText(Subs___Activity.this, "(2) 서버와 연결이 원활하지 않아서\n로그인이 되지 않았습니다.", Toast.LENGTH_SHORT).show();

                    Log.e("AAA", "API.login() >> onFailure() >> " + t.toString());
                }
            });

        } catch (ApiException e) {
            SharedInfo.User.reset();
            Toast.makeText(Subs___Activity.this, "구글 로그인이 되지 않습니다.", Toast.LENGTH_SHORT).show();
        }
    }




    /* ############################################################################################# */
    // API

    UserAPI mAPI_User;

    void initAPI() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConstDef.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mAPI_User = retrofit.create(UserAPI.class);
    }



    /* ############################################################################################################ */
    /* 상단 공통 버턴 */

    @BindView(R.id.Button_Back) RippleLayout mButton_Back;

    void initTopButtons() {
        mButton_Back.setOnRippleCompleteListener(new RippleLayout.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleLayout rippleView) {
                onBackPressed();
            }
        });
    }
}
