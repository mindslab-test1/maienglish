package ai.mindslab.maienglish.Unit;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ai.mindslab.maienglish.Book.Books___Activity;
import ai.mindslab.maienglish.Common.ConstDef;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.ExternLib.RippleLayout;
import ai.mindslab.maienglish.Model.RestApiVO;
import ai.mindslab.maienglish.Model.UnitVO;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.RestAPI.BookAPI;
import ai.mindslab.maienglish.RestAPI.Param;
import ai.mindslab.maienglish.RestAPI.PatternAPI;
import ai.mindslab.maienglish.RestAPI.ResCode;
import ai.mindslab.maienglish.SideMenu.SideMenu___Activity;
import ai.mindslab.maienglish.Subscribe.Subs___Activity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Unit___Activity extends AppCompatActivity {
    public static final int RC___STUDY = 1900;


    @BindView(R.id.House) View mHouse;
    static boolean mFirstFlag = true;

    public static int mBookNo = 0;
    public static int mSelectedUnitIndex = 0;
    String mColor = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.unit___activity);
        ButterKnife.bind(this);
        Utils.setColor_StatusBarIcon(this, true, this.getResources().getColor(R.color.sys_bg___ad));

        Intent it = this.getIntent();
        mBookNo = it.getIntExtra(Param.BOOK_NO, 0);
        mSelectedUnitIndex = it.getIntExtra("unitIndex", 0);
        mColor = it.getStringExtra("color");

        loadingData();
    }

    @Override
    protected void onResume() {
        super.onResume();

        initSubsButton();

        /* 학습 결과 반영 */
        if(mAdapter != null) {
            for(int xx = 0; xx < mPager.getChildCount(); xx++) {
                RecyclerView rview = (RecyclerView) mAdapter.mList_View.get(xx).findViewById(R.id.RecyclerView);
                rview.getAdapter().notifyDataSetChanged();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("AAA", "학습이 끝났어요.");
        if(resultCode == RESULT_OK){
            switch (requestCode){
                case RC___STUDY:

                    break;
            }
        }
    }


    /* ############################################################################################################ */
    // PAGER

    @BindView(R.id.Pager)               com.example.lib.Deck    mPager;

    List<UnitVO> mList_Unit = new ArrayList<UnitVO>();
    List<View> mList_View = new ArrayList<View>();

    Units___Apdapter mAdapter = null;

    void initPager() {
        mAdapter = new Units___Apdapter(this, mBookNo, mList_Unit, mList_View);
        mPager.setOffscreenPageLimit(1);
        mPager.addOnPageChangeListener(mPagerListener);
        mPager.useDefaultPadding(this);
        mPager.setAdapter(mAdapter);
        if(mList_Unit.size() > 0) mHouse.setBackgroundColor(Color.parseColor(mColor));

        mPager.setCurrentItem(mSelectedUnitIndex);
    }

    ViewPager.OnPageChangeListener mPagerListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {
        }

        @Override
        public void onPageSelected(int position) {
            mHouse.setBackgroundColor(Color.parseColor(mColor));
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };



    /* ############################################################################################################ */
    // 데이타 로딩


    void loadingData() {
        new taskLoadingData().execute();
    }

    class taskLoadingData extends AsyncTask<Void /* doInBackground IN */, Integer /* onProgressUpdate IN */, Integer /* doInBackground OUT, onPostExecute IN */> {

        protected Integer doInBackground(Void ... value) {
            mList_Unit = SharedInfo.Study.mBook.getList_unit();

            for(int xx = 0; xx < mList_Unit.size(); xx++) {
                mList_Unit.get(xx).getList_pattern().clear();
            }

            for(int xx = 0; xx < mList_Unit.size(); xx++) {
                mList_View.add(LayoutInflater.from(Unit___Activity.this).inflate(R.layout.unit___card, null));
            }

            return 0;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(Integer result) {
            initPager();
        }
    }

    /* ############################################################################################################ */
    /* 상단 공통 버턴 */

    @OnClick(R.id.Button_TopLogo)
    void onCLick_TopLog(View view) {
        Intent it = new Intent(this, Books___Activity.class);
        startActivity(it);
        finish();
    }

    @OnClick(R.id.Button_Hambuger)
    void onCLick_SideMenu(View view) {
        Intent it = new Intent(this, SideMenu___Activity.class);
        startActivity(it);
    }


    /* ############################################################################################################ */
    /* SUBS 버턴 */

    @BindView(R.id.Button_Subs)
    RippleLayout mButton_Subs;

    void initSubsButton() {
        if(Utils.isAllowedAllContent()) mButton_Subs.setVisibility(View.GONE);
        else mButton_Subs.setVisibility(View.VISIBLE);

        mButton_Subs.setOnRippleCompleteListener(new RippleLayout.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleLayout rippleView) {
                Intent it = new Intent(Unit___Activity.this, Subs___Activity.class);
                startActivity(it);
            }
        });
    }
}
