package ai.mindslab.maienglish.Unit;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ai.mindslab.maienglish.Book.Books___Activity;
import ai.mindslab.maienglish.Common.ConstDef;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Model.BookVO;
import ai.mindslab.maienglish.Model.RestApiVO;
import ai.mindslab.maienglish.Model.UnitVO;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.RestAPI.BookAPI;
import ai.mindslab.maienglish.RestAPI.PatternAPI;
import ai.mindslab.maienglish.RestAPI.ResCode;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Units___Apdapter extends PagerAdapter {
    Context mContext;
    public List<UnitVO> mList;
    public List<View> mList_View;
    int mBookNo;

    Units___Apdapter(Context ctx, int book_no, List<UnitVO> list, List<View> listView) {
        mContext = ctx;
        mList = list;
        mList_View = listView;
        mBookNo = book_no;

        initAPI();
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object obj) {
        return view == obj;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Log.e("AAA", "Unit 신규 화면 구성.................... position = " + position);

        View view = mList_View.get(position);
        container.addView(view);

        /* 타이틀 출력 */
        TextView textView = (TextView) view.findViewById(R.id.Text_UpTitle);
        textView.setText("Unit " + mList.get(position).getUnit_no());

        textView = (TextView) view.findViewById(R.id.Text_DownTitle);
        textView.setText(mList.get(position).getTitle(mContext));


        /* 목록 설정 */
        RecyclerView rview = (RecyclerView) view.findViewById(R.id.RecyclerView);
        rview.setLayoutManager(new LinearLayoutManager(mContext));
        rview.setHasFixedSize(true);
        apiGetPatternList(position, rview);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }


    /* ############################################################################################# */
    //

    final int MAX_RECYCLERVIEW_DISPLAY_ITEM = 20;       // 출력 가능한 RecyclerView의 RowView를 성능 향상을 위해 사전에 만든다.
    class Task_BuildUnit extends AsyncTask<Object /* doInBackground IN */, Object /* onProgressUpdate IN */, Integer /* doInBackground OUT, onPostExecute IN */> {

        protected Integer doInBackground(Object ... values) {
            Integer position = (Integer) values[0];
            RecyclerView rview = (RecyclerView) values[1];

            List<View> listView = new ArrayList<View>();
            for(int xx = 0; xx < MAX_RECYCLERVIEW_DISPLAY_ITEM; xx++) {
                listView.add(LayoutInflater.from(mContext).inflate(R.layout.unit___card___item, null));
            }

            Unit___RecyclerView_Adapter adapter = new Unit___RecyclerView_Adapter(mContext, mList.get(position).getList_pattern(), listView, position);
            publishProgress(position, rview, adapter);
            return position;
        }

        protected void onProgressUpdate(Object... values) {
            Integer position = (Integer) values[0];
            RecyclerView rview = (RecyclerView) values[1];
            Unit___RecyclerView_Adapter adapter = (Unit___RecyclerView_Adapter) values[2];

            rview.setAdapter(adapter);
        }

        protected void onPostExecute(Integer position) {
        }
    }



    /* ============================================================================================================================================== */
    /* API */
    /* ============================================================================================================================================== */

    PatternAPI mAPI_Pattern;

    void initAPI() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConstDef.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mAPI_Pattern = retrofit.create(PatternAPI.class);
    }

    void apiGetPatternList(final int position, final View rview) {
        Log.e("AAA", String.format("apiGetPatternList() ...................... unit_id=%d, bookd_no=%d, unit_no=%d", mList.get(position).getUnit_id(), mBookNo, mList.get(position).getUnit_no()));

        mAPI_Pattern.getPatternList(SharedInfo.User.mAuthKey, mList.get(position).getUnit_id(), mBookNo, mList.get(position).getUnit_no(), SharedInfo.Subscription.mSubsFlag).enqueue(new Callback<RestApiVO>() {
            @Override
            public void onResponse(@NonNull Call<RestApiVO> call, @NonNull Response<RestApiVO> response) {
                if(response.code() == 401) {
                    Toast.makeText(mContext, "인증이 되지 않았습니다. ", Toast.LENGTH_SHORT).show();
//                    if(mProgress != null) mProgress.hide();
                    return;
                }
                RestApiVO body = response.body();
                if (body != null && body.getRes_code() == ResCode.SUCC) {
                    mList.get(position).getList_pattern().clear();
                    mList.get(position).getList_pattern().addAll(body.getPattern_list());
                    new Task_BuildUnit().execute((Integer) position, rview);
                } else {
                    Toast.makeText(mContext, "네트워크가 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
//                    if(mProgress != null) mProgress.hide();
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestApiVO> call, @NonNull Throwable t) {
                Log.e("AAA", "API.login() >> onFailure() >> " + t.toString());
                Toast.makeText(mContext, "네트워크가 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
//                if(mProgress != null) mProgress.hide();
            }
        });
    }
}
