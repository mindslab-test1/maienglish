package ai.mindslab.maienglish.Unit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;

import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.ExternLib.MaterialRippleLayout;
import ai.mindslab.maienglish.Model.PatternVO;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.Study.Study___Activity;

public class Unit___RecyclerView_Adapter extends RecyclerView.Adapter<Unit___RecyclerView_Adapter.UnitViewHolder> {

    List<PatternVO> mList = null;
    Context mContext;
    List<View> mList_View = null;         // 화면 출력 속도를 위해 미리 만들어 놓음.
    int mUnitIdx = 0;

    public Unit___RecyclerView_Adapter(Context ctx, List<PatternVO> list, List<View> listView, int unit_idx) {
        this.mContext = ctx;
        this.mList = list;
        this.mList_View = listView;
        this.mUnitIdx = unit_idx;
    }

    @Override
    public UnitViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = null;

        /* 출력 지연을 방지하게 위해 초기 일정량을 미리 만들어 놓는다.  */
        if(mList_View.size() == 0)
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.unit___card___item, viewGroup, false);
        else view = mList_View.remove(mList_View.size()-1);

        UnitViewHolder viewHolder = new UnitViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull UnitViewHolder holder, int position) {

        holder.mText_PatternNo.setText("P" + mList.get(position).getOrder_no());
        holder.mText_Title.setText(mList.get(position).getSentence());

        /* 학습 가능: 구매중 || (유닛이 무료대상의 유닛 && 패턴 < free_pattern_count) */
        if(Utils.isAllowedAllContent()
                || (Unit___Activity.mBookNo < SharedInfo.Free.mFreeBookCnt && mUnitIdx == 0 && position < SharedInfo.Free.mFreePatternCnt )) {
            holder.mMask.setVisibility(View.GONE);
            holder.mButton_Row.setEnabled(true);
        }
        else  {
            holder.mMask.setVisibility(View.VISIBLE);
            holder.mButton_Row.setEnabled(false);
        }

        holder.mCheck.setChecked(mList.get(position).getStudied_flag() == 1 ? true : false);

        holder.mButton_Row.getChildView().setTag(R.string.tag___index, position);
        holder.mButton_Row.setOnClickListener(mOnClick_Row);
    }

    @Override
    public int getItemCount() {
        return (null != mList ? mList.size() : 0);
    }

    /* ################################################################################################################# */

    public class UnitViewHolder extends RecyclerView.ViewHolder {
        MaterialRippleLayout mButton_Row;
        RadioButton mCheck;
        TextView mText_PatternNo;
        TextView mText_Title;
        View mMask;

        public UnitViewHolder(View view) {
            super(view);

            mButton_Row = (MaterialRippleLayout) view.findViewById(R.id.Button_Row);
            mCheck = (RadioButton) view.findViewById(R.id.CheckBox);
            mText_PatternNo = (TextView) view.findViewById(R.id.Text_No);
            mText_Title = (TextView) view.findViewById(R.id.Text_Title);
            mMask = view.findViewById(R.id.Mask);
        }
    }

    /* ################################################################################################################# */

    View.OnClickListener mOnClick_Row = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (Integer) v.getTag(R.string.tag___index);
            SharedInfo.Study.mPattern = mList.get(position);

            Intent it = new Intent(mContext, Study___Activity.class);
            ((Activity) mContext).startActivityForResult(it, Unit___Activity.RC___STUDY);

        }
    };

}
