package ai.mindslab.maienglish.My;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ai.mindslab.maienglish.Common.ConstDef;
import ai.mindslab.maienglish.Common.MaiDialog;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.ExternLib.RippleLayout;
import ai.mindslab.maienglish.Model.RestApiVO;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.Realm.UserFactory;
import ai.mindslab.maienglish.RestAPI.ResCode;
import ai.mindslab.maienglish.RestAPI.UserAPI;
import ai.mindslab.maienglish.mAITalk.mAI_Talk___Activity;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class My___Activity extends AppCompatActivity {

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my___activity);
        ButterKnife.bind(this);
        Utils.setColor_StatusBarIcon(this, true, this.getResources().getColor(R.color.sys_bg___books));

        Realm.init(this);
        realm = Realm.getDefaultInstance();

        initAPI();
        initTopLayer();
        initInfo();
        initTermButton();
    }


    /* ============================================================================================================================== */
    // TOP LAYER
    /* ============================================================================================================================== */

    @BindView(R.id.Text_TopTitle) TextView mText_Title;
    @BindView(R.id.Button_Back) RippleLayout mButton_Back;

    void initTopLayer() {
        mText_Title.setText("내 정보");

        mButton_Back.setOnRippleCompleteListener(new RippleLayout.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleLayout rippleView) {
                finish();
            }
        });
    }



    /* ============================================================================================================================== */
    // MY INFO
    /* ============================================================================================================================== */

    @BindView(R.id.Text_Name) TextView mText_Name;
    @BindView(R.id.Text_Email) TextView mText_Email;
    @BindView(R.id.Image_User) ImageView mImage_User;
    @BindView(R.id.Text_CreateDate) TextView mText_CreateDate;

    void initInfo() {
        Picasso.with(this).load(SharedInfo.User.mPhotoUrl).placeholder(R.mipmap.male).into(mImage_User);
        mText_Name.setText(SharedInfo.User.mName);
        mText_Email.setText(SharedInfo.User.mEmail);

        /* 가입 날짜 출력 */
        try {
            SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date toDate = transFormat.parse(SharedInfo.User.mCreateDate);

            SimpleDateFormat transFormat2 = new SimpleDateFormat("yyyy년 MM월 dd일");
            mText_CreateDate.setText(transFormat2.format(toDate) + " 가입");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /* ============================================================================================================================== */
    // 회원 탈퇴 버턴
    /* ============================================================================================================================== */

    @BindView(R.id.Button_Term) RippleLayout mButton_Term;

    void initTermButton() {
        mButton_Term.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = "";

                message = "회원 탈퇴를 하시면, 모든 학습 이력도 함께 삭제됩니다.\n";
                if(SharedInfo.Subscription.isSubscribed()) {
                    message += "탈퇴를 하셔도, 구독은 자동해지가 되지 않습니다.\n스토어 계정에서 구독해지 하세요.\n";
                }
                message += "\n탈퇴하시겠습니까?";

                MaiDialog.build(My___Activity.this)
                        .setImage(R.mipmap.info)
                        .setMessage(message)
                        .setCancel("취소", null)
                        .setOk("확인", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                apiRemoveAccount();
                            }
                        })
                        .show();

            }
        });
    }

    void apiRemoveAccount() {
        mAPI_User.removeUser(SharedInfo.User.mAuthKey).enqueue(new Callback<RestApiVO>() {
            @Override
            public void onResponse(@NonNull Call<RestApiVO> call, @NonNull Response<RestApiVO> response) {
                if (response.isSuccessful()) {
                    RestApiVO body = response.body();
                    if (body != null) {
                        if(body.getRes_code() == ResCode.SUCC) {
                            Log.e("AAA", "안타깝네요. 이제 영원히 작별이에요.");
                            SharedInfo.User.reset();
                            UserFactory.logout(realm);
                            finish();
                        } else {
                            Log.e("AAA", "좋은 일인지는 모르겠지만, 탈퇴가 실패하셨어요.");
                            Toast.makeText(My___Activity.this, "(1) 서버와 연결이 원활하지 않습니다.", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(My___Activity.this, "(2) 서버와 연결이 원활하지 않습니다.", Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestApiVO> call, @NonNull Throwable t) {
                Log.e("AAA", "checkAnswer()................................... fail >> " + t.toString());
                Toast.makeText(My___Activity.this, "(3) 서버와 연결이 원활하지 않습니다.", Toast.LENGTH_LONG).show();
            }
        });
    }


    /* ############################################################################################# */
    // API

    UserAPI mAPI_User;

    void initAPI() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConstDef.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mAPI_User = retrofit.create(UserAPI.class);
    }
}
