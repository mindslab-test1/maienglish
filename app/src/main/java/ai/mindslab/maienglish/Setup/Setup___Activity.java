package ai.mindslab.maienglish.Setup;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ai.mindslab.maienglish.Book.Books___Activity;
import ai.mindslab.maienglish.Common.ConstDef;
import ai.mindslab.maienglish.Common.MaiDialog;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.ExternLib.RippleLayout;
import ai.mindslab.maienglish.Model.RestApiVO;
import ai.mindslab.maienglish.Model.StudyResultVO;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.RestAPI.ContentAPI;
import ai.mindslab.maienglish.RestAPI.ResCode;
import ai.mindslab.maienglish.RestAPI.StudyAPI;
import ai.mindslab.maienglish.STT.SttHandler;
import ai.mindslab.maienglish.SharedPref.SharedPref_Notification;
import ai.mindslab.maienglish.SideMenu.SideMenu___Activity;
import ai.mindslab.maienglish.Study.StudyChallenge___Fragment;
import ai.mindslab.maienglish.Study.StudyPractice___Fragment;
import ai.mindslab.maienglish.Study.StudyRepeat___Fragment;
import ai.mindslab.maienglish.Study.Study___Fragment;
import ai.mindslab.maienglish.Study.ViewPager_NoSwiping;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Setup___Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setup___activity);
        ButterKnife.bind(this);
        Utils.setColor_StatusBarIcon(this, true, this.getResources().getColor(R.color.sys_bg___ad));

        initTopLayer();
    }

    @Override
    public void onResume() {
        super.onResume();

        initAlarm();
    }

    /* ================================================================================================================== */
    // 상단 영역 설정
    /* ================================================================================================================= */

    @BindView(R.id.Text_TopTitle) TextView mText_Title;
    @BindView(R.id.Button_Back)
    RippleLayout mButton_Back;

    void initTopLayer() {
        mText_Title.setText("설정");
        mButton_Back.setOnRippleCompleteListener(mOnClick_Back);
    }

    RippleLayout.OnRippleCompleteListener mOnClick_Back = new RippleLayout.OnRippleCompleteListener() {
        @Override
        public void onComplete(RippleLayout rippleView) {
            finish();
        }
    };



    /* ================================================================================================================== */
    // 알림 설정
    /* ================================================================================================================= */

    @BindView(R.id.Text_AlarmTime) TextView mText_AlarmTime;
    @BindView(R.id.Button_AlarmTime) RippleLayout mButton_AlarmTime;
    @BindView(R.id.Switch_Alarm) SwitchCompat mSwitch_Alarm;
    @BindView(R.id.Mask_AlarmTime) View mMask_AlarmTime;

    void initAlarm() {
        mText_AlarmTime.setText(SharedPref_Notification.getTimeToString(Setup___Activity.this));
        mButton_AlarmTime.setOnRippleCompleteListener(mOnClick_AlarmTime);

        mSwitch_Alarm.setChecked(SharedPref_Notification.getFlag(Setup___Activity.this));
        mSwitch_Alarm.setOnCheckedChangeListener(mOnClick_Alarm);

        if(SharedPref_Notification.getFlag(Setup___Activity.this)) {
            mMask_AlarmTime.setVisibility(View.GONE);
        }
        else mMask_AlarmTime.setVisibility(View.VISIBLE);
    }


    RippleLayout.OnRippleCompleteListener mOnClick_AlarmTime = new RippleLayout.OnRippleCompleteListener() {
        @Override
        public void onComplete(RippleLayout rippleView) {
            Intent it = new Intent(Setup___Activity.this, Setup_AlarmTime___Activity.class);
            startActivity(it);
        }
    };

    CompoundButton.OnCheckedChangeListener mOnClick_Alarm = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(isChecked) {
                mButton_AlarmTime.setEnabled(true);
                mMask_AlarmTime.setVisibility(View.GONE);
            } else {
                mButton_AlarmTime.setEnabled(false);
                mMask_AlarmTime.setVisibility(View.VISIBLE);
            }
            SharedPref_Notification.setFlag(Setup___Activity.this, isChecked);
        }
    };


}
