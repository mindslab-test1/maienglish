package ai.mindslab.maienglish.Setup;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.nex3z.togglebuttongroup.SingleSelectToggleGroup;
import com.nex3z.togglebuttongroup.button.CircularToggle;

import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.ExternLib.RippleLayout;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.SharedPref.SharedPref_Notification;
import ai.mindslab.maienglish.StudyHistory.CircleSeekBar;
import butterknife.BindView;
import butterknife.ButterKnife;

public class Setup_AlarmTime___Activity extends AppCompatActivity {

    String mAnswerText = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setup___alarm_time___activity);
        ButterKnife.bind(this);
        Utils.setColor_StatusBarIcon(this, true, this.getResources().getColor(R.color.sys_bg___ad));

        initTopLayer();
        initClock();
        initSave();
    }


    /* ================================================================================================================== */
    // 상단 영역 설정
    /* ================================================================================================================= */

    @BindView(R.id.Text_TopTitle) TextView mText_Title;
    @BindView(R.id.Button_Back) RippleLayout mButton_Back;

    void initTopLayer() {
        mText_Title.setText("알림 시각 설정");
        mButton_Back.setOnRippleCompleteListener(mOnClick_Back);
    }

    RippleLayout.OnRippleCompleteListener mOnClick_Back = new RippleLayout.OnRippleCompleteListener() {
        @Override
        public void onComplete(RippleLayout rippleView) {
            finish();
        }
    };

    /* ================================================================================================================== */
    // 시계
    /* ================================================================================================================= */

    @BindView(R.id.Progress) CircleSeekBar mClock;
    @BindView(R.id.Text_Time) TextView mText_Time;
    @BindView(R.id.Group_Choice) SingleSelectToggleGroup mChoice;
    @BindView(R.id.Choice_AM) CircularToggle mChoice_AM;
    @BindView(R.id.Choice_PM) CircularToggle mChoice_PM;

    void initClock() {
        int time = SharedPref_Notification.getTime(Setup_AlarmTime___Activity.this) / 100;
        mClock.setProgressDisplay(time > 12 ? time-12 : time);
        mClock.setSeekBarChangeListener(mSeekChanger);

        mText_Time.setText(String.format("%02d:00", time > 12 ? time-12 : time));

        mChoice.check(time > 12 ? R.id.Choice_PM : R.id.Choice_AM);
    }

    CircleSeekBar.OnSeekBarChangedListener mSeekChanger = new CircleSeekBar.OnSeekBarChangedListener() {
        @Override
        public void onPointsChanged(CircleSeekBar circleSeekBar, int points, boolean fromUser) {
            if(fromUser) {
                Log.e("AAA", "points = " + points);
                circleSeekBar.setProgressDisplay(points);
            }

            mText_Time.setText(String.format("%02d:00", points));
        }

        @Override
        public void onStartTrackingTouch(CircleSeekBar circleSeekBar) {

        }

        @Override
        public void onStopTrackingTouch(CircleSeekBar circleSeekBar) {

        }
    };

    /* ================================================================================================================== */
    // 저장
    /* ================================================================================================================= */

    @BindView(R.id.Button_Save) RippleLayout mButton_Save;

    void initSave() {
        mButton_Save.setOnRippleCompleteListener(new RippleLayout.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleLayout rippleView) {
                int time = (mChoice.getCheckedId() == R.id.Choice_AM ? mClock.getProgressDisplay() : mClock.getProgressDisplay() + 12)%24;
                SharedPref_Notification.setTime(Setup_AlarmTime___Activity.this, time*100 + 51);
                finish();
            }
        });
    }
}
