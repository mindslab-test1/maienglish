package ai.mindslab.maienglish.Coupon;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.balysv.materialripple.MaterialRippleLayout;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.zip.Inflater;

import ai.mindslab.maienglish.Common.ConstDef;
import ai.mindslab.maienglish.Common.MaiDialog;
import ai.mindslab.maienglish.Common.MaiProgress;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.Event.FreeCouponEvent___Activity;
import ai.mindslab.maienglish.Event.SurveyEvent___Activity;
import ai.mindslab.maienglish.ExternLib.RippleLayout;
import ai.mindslab.maienglish.Model.CouponVO;
import ai.mindslab.maienglish.Model.RestApiVO;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.Realm.UserFactory;
import ai.mindslab.maienglish.RestAPI.CouponAPI;
import ai.mindslab.maienglish.RestAPI.Param;
import ai.mindslab.maienglish.RestAPI.ResCode;
import ai.mindslab.maienglish.RestAPI.UserAPI;
import ai.mindslab.maienglish.SchedulerTask.Worker_FreeCoupon;
import ai.mindslab.maienglish.SideMenu.SideMenu___Activity;
import ai.mindslab.maienglish.Unit.Unit___RecyclerView_Adapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static java.security.AccessController.getContext;

public class Coupon___Activity extends AppCompatActivity  implements  GoogleApiClient.OnConnectionFailedListener {

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coupon___activity);
        ButterKnife.bind(this);
        Utils.setColor_StatusBarIcon(this, true, this.getResources().getColor(R.color.sys_bg___books));

        Realm.init(this);
        realm = Realm.getDefaultInstance();

        initProgress();
        initAPI();
        initTopLayer();
        initRegister();
        initGoogleLogin();
        initCouponList();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("AAA", String.format("onActivityResult.................... requestCode = %d, result=%d", requestCode, resultCode) );
        // 구글 로그인 결과
        if (requestCode == RC___GOOGLE_LOGIN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleGoogleLogin(task);
        }
    }


    /* ============================================================================================================================== */
    // TOP LAYER
    /* ============================================================================================================================== */

    @BindView(R.id.Text_TopTitle) TextView mText_Title;
    @BindView(R.id.Button_Back) RippleLayout mButton_Back;

    void initTopLayer() {
        mText_Title.setText("쿠폰함");

        mButton_Back.setOnRippleCompleteListener(new RippleLayout.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleLayout rippleView) {
                finish();
            }
        });
    }

    /* ============================================================================================================================== */
    // Progress
    /* ============================================================================================================================== */
    private MaiProgress mProgress;
    void initProgress() {
        mProgress = MaiProgress.build(this);
    }

    /* ============================================================================================================================== */
    // COUPON REGISTER
    /* ============================================================================================================================== */

    @BindView(R.id.Edit_CouponNumber) EditText mEdit_CouponNumber;
    @BindView(R.id.Button_Register) MaterialRippleLayout mButton_Register;
    @BindView(R.id.Text_Register) TextView mText_Register;

    void initRegister() {
        mEdit_CouponNumber.addTextChangedListener(mWatcher);
    }

    TextWatcher mWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if(mEdit_CouponNumber.getText().length() == ConstDef.COUPON_NUM_LENGTH) {
                mText_Register.setEnabled(true);
                mButton_Register.setEnabled(true);
            }
            else {
                mText_Register.setEnabled(false);
                mButton_Register.setEnabled(false);
            }
        }
    };

    @OnClick(R.id.Button_Register)
    void onClick_Register(View v) {
        if(SharedInfo.User.isLogined() == false) {
            MaiDialog.build(this)
                    .setImage(R.mipmap.male)
                    .setMessage("쿠폰 등록은 로그인 후에 처리됩니다.\n로그인 하시겠습니까?")
                    .setCancel("취소", null)
                    .setOk("로그인", mOnClick_Login)
                    .show();
            return;
        } else registerCoupon();
    }

    View.OnClickListener mOnClick_Login = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, RC___GOOGLE_LOGIN);
        }
    };

    void registerCoupon() {
        mProgress.show();
        mAPI_Coupon.registerCoupon(SharedInfo.User.mAuthKey, mEdit_CouponNumber.getText().toString()).enqueue(new Callback<RestApiVO>() {
            @Override
            public void onResponse(@NonNull Call<RestApiVO> call, @NonNull Response<RestApiVO> response) {
                mProgress.hide();

                RestApiVO body = response.body();
                if (body != null && body.getRes_code() == ResCode.SUCC) {
                    mEdit_CouponNumber.setText("");

                    mList_Coupon.add(0, body.getCoupon());
                    mAdapter.notifyItemInserted(0);
                    mLayer_Empty.setVisibility(View.GONE);

                    final CouponVO coupon = body.getCoupon();

                    /* 체험 무료 쿠폰일 경우에 축하 액티비티로 이동 */
                    if(coupon.getCoupon_id() == ConstDef.COUPON_ID___FREE_EXPERIENCE_EVENT) {
                        Intent it = new Intent(Coupon___Activity.this, FreeCouponEvent___Activity.class);
                        it.putExtra(Param.COUPON, coupon);
                        startActivity(it);
                    }

                } else if(body != null && body.getRes_code() == ResCode.NOT_FOUND) {
                    Toast.makeText(Coupon___Activity.this, "등록되지 않은 쿠폰입니다.", Toast.LENGTH_SHORT).show();

                } else if(body != null && body.getRes_code() == ResCode.ALREADY) {

                    Toast.makeText(Coupon___Activity.this, "이미 등록되어 있는 쿠폰입니다.", Toast.LENGTH_SHORT).show();
                } else if(body != null && body.getRes_code() == ResCode.DUPLICATED) {

                    Toast.makeText(Coupon___Activity.this, "무료 체험 쿠폰은 1회만 등록 가능합니다. ", Toast.LENGTH_SHORT).show();
                }else if(body != null && body.getRes_code() == ResCode.EXPIRED) {

                    Toast.makeText(Coupon___Activity.this, "이 쿠폰은 등록이 만료되었습니다.", Toast.LENGTH_SHORT).show();
                } else if(body != null && body.getRes_code() == ResCode.NOT_YET) {
                    Toast.makeText(Coupon___Activity.this, "아직 등록 가능하지 않습니다.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(Coupon___Activity.this, "(1) 서버와 연결이 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestApiVO> call, @NonNull Throwable t) {
                mProgress.hide();
                Toast.makeText(Coupon___Activity.this, "(2) 서버와 연결이 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
            }
        });
    }


    /* ============================================================================================================================== */
    // COUPON LIST
    /* ============================================================================================================================== */

    @BindView(R.id.Layer_Empty) LinearLayout mLayer_Empty;

    List<CouponVO> mList_Coupon = new ArrayList<CouponVO>();
    RecyclerView mRecyclerView;
    Coupon_Adapter mAdapter;

    void initCouponList() {
        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(true);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(this, R.drawable.coupon_divider));
        mRecyclerView.addItemDecoration(itemDecorator);

        mAdapter = new Coupon_Adapter(this, mList_Coupon);
        mRecyclerView.setAdapter(mAdapter);

        if(SharedInfo.User.isLogined() == false) {
            return;
        }

        mAPI_Coupon.getCouponList(SharedInfo.User.mAuthKey).enqueue(new Callback<RestApiVO>() {
            @Override
            public void onResponse(@NonNull Call<RestApiVO> call, @NonNull Response<RestApiVO> response) {
                RestApiVO body = response.body();
                if (body != null && body.getRes_code() == ResCode.SUCC) {

                    mList_Coupon.addAll(body.getCoupon_list());
                    mAdapter.notifyDataSetChanged();

                    if(mList_Coupon.size() != 0) mLayer_Empty.setVisibility(View.GONE);
                }
                else {
                    Toast.makeText(Coupon___Activity.this, "(1) 서버와 연결이 원활하지 않습니다. >> " + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestApiVO> call, @NonNull Throwable t) {
                Toast.makeText(Coupon___Activity.this, "(2) 서버와 연결이 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
            }
        });
    }





    /* ############################################################################################# */
    // API

    CouponAPI mAPI_Coupon;
    UserAPI mAPI_User;

    void initAPI() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConstDef.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mAPI_Coupon = retrofit.create(CouponAPI.class);
        mAPI_User = retrofit.create(UserAPI.class);
    }





    /* ============================================================================================================================== */
    // 구글 로그인
    /* ============================================================================================================================== */
    final int RC___GOOGLE_LOGIN = 100;

    private FirebaseAuth mAuth;
    private GoogleApiClient mGoogleApiClient;
    GoogleSignInClient mGoogleSignInClient;

    /* 구글 API 초기화 */
    void initGoogleLogin() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestIdToken(String.valueOf(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mAuth = FirebaseAuth.getInstance();
    }


    /* 구글로부터 결과 수신 처리*/
    void handleGoogleLogin(Task<GoogleSignInAccount> completedTask) {

        GoogleSignInAccount account = null;
        try {
            account = completedTask.getResult(ApiException.class);

            /* 구글에서 수신한 정보 등록 */
            SharedInfo.User.mName = account.getDisplayName();
            SharedInfo.User.mEmail = account.getEmail();
            if(account.getPhotoUrl() != null) SharedInfo.User.mPhotoUrl = account.getPhotoUrl().toString();

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "구글 로그인이 되지 않습니다.", Toast.LENGTH_SHORT).show();
            return;
        }

        registerUser();
    }


    /* 구글 로그인 후에 서버 등록 */
    private void registerUser() {
        /* 서버에 정보 저장 */
        mAPI_User.registerUser(SharedInfo.User.mEmail).enqueue(new Callback<RestApiVO>() {
            @Override
            public void onResponse(@NonNull Call<RestApiVO> call, @NonNull Response<RestApiVO> response) {
                RestApiVO body = response.body();
                if (body != null && body.getRes_code() == ResCode.SUCC) {
                    /* 인증 정보 등록 */
                    UserFactory.login(realm, SharedInfo.User.mEmail, SharedInfo.User.mName, SharedInfo.User.mPhotoUrl, body.getAuth_key());
                    SharedInfo.User.mAuthKey = body.getAuth_key();
                    SharedInfo.User.mCreateDate = body.getDatetime();

                    SharedInfo.Coupon.mFreeCoupon = body.getCoupon();
                    if(SharedInfo.Coupon.getFreeCoupon() != null) Worker_FreeCoupon.build();

                    Toast.makeText(Coupon___Activity.this, "로그인 되셨습니다.", Toast.LENGTH_SHORT).show();

                    registerCoupon();
                } else {
                    SharedInfo.User.reset();
                    MaiDialog.build(Coupon___Activity.this)
                            .setImage(R.mipmap.info)
                            .setMessage("구글 로그인이 되지 않습니다.(1)\n인터넷 연결 상태 확인하시고 고객센터에 문의하세요.")
                            .setOk("확인", null)
                            .show();
                }

            }

            @Override
            public void onFailure(@NonNull Call<RestApiVO> call, @NonNull Throwable t) {
                SharedInfo.User.reset();
                MaiDialog.build(Coupon___Activity.this)
                        .setImage(R.mipmap.info)
                        .setMessage("구글 로그인이 되지 않습니다.(2)\n인터넷 연결 상태 확인하시고 고객센터에 문의하세요.")
                        .setOk("확인", null)
                        .show();
            }
        });
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        MaiDialog.build(this)
                .setImage(R.mipmap.info)
                .setMessage("구글 로그인이 되지 않습니다.(3)\n인터넷 연결 상태 확인하시고 고객센터에 문의하세요.")
                .setOk("확인", null)
                .show();
    }
}
