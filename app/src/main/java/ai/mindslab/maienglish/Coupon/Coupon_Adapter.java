package ai.mindslab.maienglish.Coupon;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ai.mindslab.maienglish.Book.Books___Activity;
import ai.mindslab.maienglish.Common.ConstDef;
import ai.mindslab.maienglish.Common.MaiDialog;
import ai.mindslab.maienglish.Common.MaiProgress;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Event.FreeCouponEvent___Activity;
import ai.mindslab.maienglish.Model.CouponVO;
import ai.mindslab.maienglish.Model.PatternVO;
import ai.mindslab.maienglish.Model.RestApiVO;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.RestAPI.CouponAPI;
import ai.mindslab.maienglish.RestAPI.ResCode;
import ai.mindslab.maienglish.SchedulerTask.Worker_FreeCoupon;
import ai.mindslab.maienglish.Study.Study___Activity;
import ai.mindslab.maienglish.Unit.Unit___Activity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Coupon_Adapter extends RecyclerView.Adapter<Coupon_Adapter.ViewHolder> {
    List<CouponVO> mList = null;
    Context mContext;

    public Coupon_Adapter(Context ctx, List<CouponVO> list) {
        this.mContext = ctx;
        this.mList = list;

        initAPI();
        initProgress();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = null;
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.coupon___card, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CouponVO coupon = mList.get(position);

        holder.mText_Title.setText(coupon.getTitle());
        holder.mText_Title.setTextColor(Color.parseColor(coupon.getColor()));

        holder.mText_PublishingId.setText(String.format("%s-%s-%s-%s",
                coupon.getPublishing_id().substring(0, 4),
                coupon.getPublishing_id().substring(4, 8),
                coupon.getPublishing_id().substring(8, 12),
                coupon.getPublishing_id().substring(12, 16)
        ));
        holder.mText_PublishingId.setTextColor(Color.parseColor(coupon.getColor()));

        holder.mText_Period.setText(String.format("유효기간: %s ~ %s", coupon.getBegin_date(), coupon.getEnd_date()));
        holder.mText_PublishingDate.setText(String.format("발행일: %s", coupon.getPublish_datetime().substring(0, 10)));

        /* 쿠폰 상태 정보 */
        holder.mLayer_State.setBackgroundColor(Color.parseColor(coupon.getColor()));
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date beginDateTime;
        String endDateTime = null;

        if(coupon.getState() == CouponState.PUBLISHING___USED || coupon.getState() == CouponState.PUBLISHING___USE_COMPLETED) {
            try {
                beginDateTime = dateFormat.parse(coupon.getUse_datetime());
                cal.setTime(beginDateTime);
                cal.add(Calendar.DAY_OF_YEAR, coupon.getFree_days());
                endDateTime = dateFormat.format(cal.getTime());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        switch(coupon.getState()) {
            case CouponState.PUBLISHING___REGISTERED:
                holder.mButton_Coupon.setEnabled(true);
                holder.mMask.setVisibility(View.GONE);
                holder.mText_StatePeriod.setVisibility(View.GONE);
                holder.mText_Period.setVisibility(View.VISIBLE);
                break;

            case CouponState.PUBLISHING___USED:
                holder.mButton_Coupon.setEnabled(false);
                holder.mMask.setVisibility(View.VISIBLE);
                holder.mText_State.setText("사용중");
                holder.mText_StatePeriod.setText(coupon.getUse_datetime().substring(0, 16) + " ~ " + endDateTime.substring(0, 16));
                holder.mText_StatePeriod.setVisibility(View.VISIBLE);
                holder.mText_Period.setVisibility(View.INVISIBLE);
                break;

            case CouponState.PUBLISHING___USE_COMPLETED:
                holder.mButton_Coupon.setEnabled(false);
                holder.mMask.setVisibility(View.VISIBLE);
                holder.mText_State.setText("사용\n만료");
                holder.mText_StatePeriod.setText(coupon.getUse_datetime().substring(0, 16) + " ~ " + endDateTime.substring(0, 16));
                holder.mText_StatePeriod.setVisibility(View.VISIBLE);
                holder.mText_Period.setVisibility(View.INVISIBLE);
                break;

            case CouponState.PUBLISHING___EXPIRED:
                holder.mButton_Coupon.setEnabled(false);
                holder.mMask.setVisibility(View.VISIBLE);
                holder.mText_State.setText("기간\n만료");
                holder.mText_StatePeriod.setText(coupon.getBegin_date() + " ~ " + coupon.getEnd_date());
                holder.mText_StatePeriod.setVisibility(View.VISIBLE);
                holder.mText_Period.setVisibility(View.INVISIBLE);
                break;
        }

        holder.mButton_Coupon.getChildView().setTag(R.string.tag___object, coupon);
        holder.mButton_Coupon.setOnClickListener(mOnClick_Coupon);
    }

    @Override
    public int getItemCount() {
        return (null != mList ? mList.size() : 0);
    }


    View.OnClickListener mOnClick_Coupon = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {

            if(SharedInfo.Subscription.isSubscribed()) {
                MaiDialog.build(mContext)
                        .setImage(R.mipmap.coupon)
                        .setMessage("이미 구독을 하고 계십니다.\n다음에 사용하세요.")
                        .setOk("확인", null).show();
            } else if(SharedInfo.Coupon.mFreeCoupon != null) {
                MaiDialog.build(mContext)
                        .setImage(R.mipmap.coupon)
                        .setMessage("이미 무료 쿠폰을 이용하고 계십니다.\n다음에 사용하세요.")
                        .setOk("확인", null).show();
            } else {
                CouponDialog.build(mContext, (CouponVO) v.getTag(R.string.tag___object))
                        .setCancel("아니오", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Toast.makeText(mContext, "쿠폰 사용이 취소 되었습니다.", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setOk("네", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                requestUseCoupon((CouponVO) v.getTag(R.string.tag___object));
                            }
                        }).show();

            }
        }
    };

    void requestUseCoupon(final CouponVO coupon) {
        mProgress.show();
        mAPI_Coupon.useCoupon(SharedInfo.User.mAuthKey, coupon.getPublishing_id()).enqueue(new Callback<RestApiVO>() {
            @Override
            public void onResponse(@NonNull Call<RestApiVO> call, @NonNull Response<RestApiVO> response) {
                mProgress.hide();

                RestApiVO body = response.body();
                if (body != null && body.getRes_code() == ResCode.SUCC) {
                    coupon.set(body.getCoupon());
//                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                    coupon.setState(CouponState.PUBLISHING___USED);
//                    coupon.setUse_datetime(dateFormat.format(new Date()));
                    if(coupon.getCoupon_id() == ConstDef.COUPON_ID___FREE_EXPERIENCE_EVENT) {
                        MaiDialog.build(mContext)
                                .setImage(R.mipmap.coupon)
                                .setMessage("쿠폰이 사용되었습니다.\n\n3일후에 알림으로 제공되는 설문지를 작성해 주시면 추가 일주일 무료 쿠폰을 드립니다.")
                                .setOk("확인", null).show();
                    } else {
                        Toast.makeText(mContext, "쿠폰이 사용되었습니다.", Toast.LENGTH_SHORT).show();
                    }

                    SharedInfo.Coupon.setFreeCoupon(coupon);
                    Worker_FreeCoupon.build();

                    notifyDataSetChanged();
                } else if(body != null && body.getRes_code() == ResCode.NOT_FOUND) {
                    mList.remove(coupon);
                    notifyDataSetChanged();

                    Toast.makeText(mContext, "이 쿠폰은 사용이 불가능한 쿠폰입니다.", Toast.LENGTH_SHORT).show();
                } else if(body != null && body.getRes_code() == ResCode.ALREADY) {
                    mList.remove(coupon);
                    notifyDataSetChanged();

                    Toast.makeText(mContext, "이미 사용된 쿠폰입니다.", Toast.LENGTH_SHORT).show();
                } else if(body != null && body.getRes_code() == ResCode.EXPIRED) {
                    coupon.setState(CouponState.PUBLISHING___EXPIRED);
                    notifyDataSetChanged();

                    Toast.makeText(mContext, "기간 만료된 쿠폰입니다.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, "(1) 서버와 연결이 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestApiVO> call, @NonNull Throwable t) {
                mProgress.hide();
                Toast.makeText(mContext, "(2) 서버와 연결이 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /* ################################################################################################################# */

    public class ViewHolder extends RecyclerView.ViewHolder {
        MaterialRippleLayout mButton_Coupon;
        TextView mText_Title;
        TextView mText_PublishingId;
        TextView mText_PublishingDate;
        TextView mText_Period;
        TextView mText_StatePeriod;
        TextView mText_State;
        View     mLayer_State;
        View     mMask;

        public ViewHolder(View view) {
            super(view);

            mButton_Coupon = (MaterialRippleLayout) view.findViewById(R.id.Button_Coupon);
            mText_Title = (TextView) view.findViewById(R.id.Text_Title);
            mText_PublishingId = (TextView) view.findViewById(R.id.Text_PublishingId);
            mText_PublishingDate = (TextView) view.findViewById(R.id.Text_PublishingDate);
            mText_Period = (TextView) view.findViewById(R.id.Text_Period);
            mText_StatePeriod = (TextView) view.findViewById(R.id.Text_StatePeriod);
            mText_State = (TextView) view.findViewById(R.id.Text_State);
            mLayer_State = (View) view.findViewById(R.id.Layer_State);
            mMask = view.findViewById(R.id.Mask);
        }
    }


    /* ============================================================================================================================== */
    // Progress
    /* ============================================================================================================================== */
    private MaiProgress mProgress;
    void initProgress() {
        mProgress = MaiProgress.build(mContext);
    }


    /* ============================================================================================================================== */
    // API
    /* ============================================================================================================================== */

    CouponAPI mAPI_Coupon;

    void initAPI() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConstDef.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mAPI_Coupon = retrofit.create(CouponAPI.class);
    }
}
