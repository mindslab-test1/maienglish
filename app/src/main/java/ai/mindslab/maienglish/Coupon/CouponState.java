package ai.mindslab.maienglish.Coupon;

public class CouponState {
    public static final int PUBLISHING___NOT_REGISTERED = 0;
    public static final int PUBLISHING___REGISTERED = 1;
    public static final int PUBLISHING___USED = 2;
    public static final int PUBLISHING___EXPIRED = 3;
    public static final int PUBLISHING___USE_COMPLETED = 4;

    public static final int USE___USING = 1;
    public static final int USE___EXPIRED = 2;
    public static final int USE___STOP = 3;

}
