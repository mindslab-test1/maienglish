package ai.mindslab.maienglish.Coupon;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.squareup.picasso.Picasso;

import ai.mindslab.maienglish.Model.CouponVO;
import ai.mindslab.maienglish.R;
import butterknife.BindView;

public class CouponDialog {
    private Context mContext;
    private Dialog mDialog;
    private boolean mOkFlag = false;
    private boolean mCancelFlag = false;
    private CouponVO mCoupon;

    @BindView(R.id.Image) ImageView mImage;
    @BindView(R.id.Text_Message) TextView mText_Message;
    @BindView(R.id.Button_Ok) MaterialRippleLayout mButton_Ok;
    @BindView(R.id.Text_Ok) TextView mText_Ok;
    @BindView(R.id.Button_Cancel) MaterialRippleLayout mButton_Cancel;
    @BindView(R.id.Text_Cancel) TextView mText_Cancel;
    @BindView(R.id.Button_Common) MaterialRippleLayout mButton_Common;
    @BindView(R.id.Text_Common) TextView mText_Common;

    @BindView(R.id.Text_Title) TextView mText_Title;
    @BindView(R.id.Text_PublishingId) TextView mText_PublishingId;
    @BindView(R.id.Text_PublishingDate) TextView mText_PublishingDate;
    @BindView(R.id.Text_Period) TextView mText_Period;

    public static CouponDialog build(Context context, CouponVO coupon) {
        CouponDialog dialog = new CouponDialog();
        dialog.mContext = context;
        dialog.mCoupon = coupon;

        dialog.mDialog = new Dialog(dialog.mContext);
        dialog.mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.mDialog.setContentView(R.layout.coupon___dialog);
//        dialog.mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(context.getResources().getColor(R.color.dialog___outside___bg)));
        dialog.mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.mImage = (ImageView) dialog.mDialog.findViewById(R.id.Image);
        dialog.mText_Message = (TextView) dialog.mDialog.findViewById(R.id.Text_Message);
        dialog.mButton_Ok = (MaterialRippleLayout) dialog.mDialog.findViewById(R.id.Button_Ok);
        dialog.mText_Ok = (TextView) dialog.mDialog.findViewById(R.id.Text_Ok);
        dialog.mButton_Cancel = (MaterialRippleLayout) dialog.mDialog.findViewById(R.id.Button_Cancel);
        dialog.mText_Cancel = (TextView) dialog.mDialog.findViewById(R.id.Text_Cancel);
        dialog.mButton_Common = (MaterialRippleLayout) dialog.mDialog.findViewById(R.id.Button_Common);
        dialog.mText_Common = (TextView) dialog.mDialog.findViewById(R.id.Text_Common);

        dialog.mButton_Ok.setOnClickListener(dialog.mOnClick___DefaultOk);
        dialog.mButton_Cancel.setOnClickListener(dialog.mOnClick___DefaultCancel);
        dialog.mButton_Common.setOnClickListener(dialog.mOnClick___DefaultCommon);

        dialog.mText_Title = (TextView) dialog.mDialog.findViewById(R.id.Text_Title);
        dialog.mText_PublishingId = (TextView) dialog.mDialog.findViewById(R.id.Text_PublishingId);;
        dialog.mText_PublishingDate = (TextView) dialog.mDialog.findViewById(R.id.Text_PublishingDate);;
        dialog.mText_Period = (TextView) dialog.mDialog.findViewById(R.id.Text_Period);;

        dialog.setCoupon();

        dialog.mDialog.setCancelable(false);
        return dialog;
    }

    public CouponDialog setMessage(String title) {
        mText_Message.setText(title);
        return this;
    }

    public CouponDialog setImage(int rscId) {
        Picasso.with(mContext).load(rscId).into(mImage);
        return this;
    }

    public CouponDialog setOk(String title, View.OnClickListener listener) {
        mText_Ok.setText(title);
        mOnClick_Ok = listener;

        mText_Common.setText(title);
        mOnClick_Common = listener;

        mOkFlag = true;
        return this;
    }

    public CouponDialog setCancel(String title, View.OnClickListener listener) {
        mText_Cancel.setText(title);
        mOnClick_Cancel = listener;

        mText_Common.setText(title);
        mOnClick_Common = listener;

        mCancelFlag = true;
        return this;
    }

    public CouponDialog show() {
        if(mOkFlag == false || mCancelFlag == false) {
            mButton_Ok.setVisibility(View.GONE);
            mButton_Cancel.setVisibility(View.GONE);
            mButton_Common.setVisibility(View.VISIBLE);
        }
        else {
            mButton_Ok.setVisibility(View.VISIBLE);
            mButton_Cancel.setVisibility(View.VISIBLE);
            mButton_Common.setVisibility(View.GONE);
        }

        mDialog.show();
        return this;
    }

    public CouponDialog hide() {
        mDialog.hide();
        return this;
    }

    public CouponDialog dismiss() {
        mDialog.dismiss();
        return this;
    }

    /* ############################################################################################ */
    // CALLBACK

    View.OnClickListener mOnClick___DefaultOk = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(mOnClick_Ok != null) mOnClick_Ok.onClick(v);
            mDialog.dismiss();
        }
    };
    View.OnClickListener mOnClick___DefaultCancel = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(mOnClick_Cancel != null) mOnClick_Cancel.onClick(v);
            mDialog.dismiss();
        }
    };
    View.OnClickListener mOnClick___DefaultCommon = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(mOnClick_Common != null) mOnClick_Common.onClick(v);
            mDialog.dismiss();
        }
    };

    View.OnClickListener mOnClick_Ok = null;
    View.OnClickListener mOnClick_Cancel = null;
    View.OnClickListener mOnClick_Common = null;


    /* ========================================================================================================== */
    // 쿠폰
    /* ========================================================================================================== */

    public void setCoupon() {
        mText_Title.setText(mCoupon.getTitle());
        mText_Title.setTextColor(Color.parseColor(mCoupon.getColor()));

        mText_PublishingId.setText(String.format("%s-%s-%s-%s",
                mCoupon.getPublishing_id().substring(0, 4),
                mCoupon.getPublishing_id().substring(4, 8),
                mCoupon.getPublishing_id().substring(8, 12),
                mCoupon.getPublishing_id().substring(12, 16)
        ));
        mText_PublishingId.setTextColor(Color.parseColor(mCoupon.getColor()));

        mText_Period.setText(String.format("유효기간: %s ~ %s", mCoupon.getBegin_date(), mCoupon.getEnd_date()));
        mText_PublishingDate.setText(String.format("발행일: %s", mCoupon.getPublish_datetime().substring(0, 10)));
    }
}
