package ai.mindslab.maienglish.Realm;

import io.realm.Realm;

public class UserFactory {

    /* 기본 사용자 정보 객체 생성 */
    static public Realm_User create(Realm realm) {
        Realm_User user = new Realm_User();
        user.setEmail(null);
        realm.beginTransaction();
        realm.copyToRealm(user);
        realm.commitTransaction();

        return user;
    }

    /* 사용자 정보 객체 반환 */
    static public Realm_User get(Realm realm) {
        return realm.where(Realm_User.class).findFirst();
    }

    /* 로그인 */
    static public void login(Realm realm, String email, String name, String photoUrl, String auth_key) {
        realm.beginTransaction();
        Realm_User user = get(realm);
        user.setEmail(email);
        user.setName(name);
        user.setPhotoUrl(photoUrl);
        user.setAuthKey(auth_key);
        realm.commitTransaction();
    }

    /* 로그아웃 */
    static public void logout(Realm realm) {
        realm.beginTransaction();
        Realm_User user = get(realm);
        user.setEmail(null);
        user.setAuthKey(null);
        realm.commitTransaction();
    }
}
