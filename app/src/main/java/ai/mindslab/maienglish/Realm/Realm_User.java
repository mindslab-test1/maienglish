package ai.mindslab.maienglish.Realm;

import io.realm.RealmObject;
import io.realm.annotations.Required;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
public class Realm_User extends RealmObject {
    private String Email = null;
    private String Name = null;
    private String PhotoUrl = null;
    private String AuthKey = null;
}
