package ai.mindslab.maienglish;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;

import ai.mindslab.maienglish.Book.Books___Activity;
import ai.mindslab.maienglish.Common.MaiDialog;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.Subscribe.Subs___Activity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Home___Activity extends AppCompatActivity {

    final int RC___CHG_PERMISSION = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home___activity);
        ButterKnife.bind(this);
        Utils.setColor_StatusBarIcon(this, false, this.getResources().getColor(R.color.sys_bg___home));

        if(SharedInfo.Coupon.getFreeCoupon() != null) Log.i("AAA", "[ 쿠폰 ] 무료 쿠폰을 사용중입니다.");
        else Log.i("AAA", "[ 쿠폰 ] 무료 쿠폰을 사용하고 있지 않습니다.");

        if(SharedInfo.Subscription.isSubscribed()) Log.i("AAA", "[ 구독 ] 구독 중입니다.");
        else Log.i("AAA", "[ 구독 ] 미구독 중입니다.");

        initPermission();
        initUI();

        Log.e("AAA", "Home____Activity .................................... onCreate()");
    }

    /* ######################################################################################################################### */
    // 종료 처리

    private long backKeyClickTime = 0;

    public void onBackPressed() {
        if (System.currentTimeMillis() > backKeyClickTime + 2000) {
            backKeyClickTime = System.currentTimeMillis();
            showToast();
            return;
        }
        if (System.currentTimeMillis() <= backKeyClickTime + 2000) {
            finishAffinity();
//            System.runFinalization();
//            System.exit(0);
        }
    }

    public void showToast() {
        Toast.makeText(this, "뒤로 가기 버튼을 한 번 더 누르면 종료됩니다.", Toast.LENGTH_SHORT).show();
    }



    /* ######################################################################################################################### */
    // UI 설정

    final int DESIGNED_WIDTH___BG_IMAGE = 1080;
    final int DESIGNED_HEIGHT___BG_IMAGE = 786;

    void initUI() {
        setSize_BgImage();
    }

    @BindView(R.id.Image_Bg) ImageView mImage_Bg;
    @BindView(R.id.Button_Start1) MaterialRippleLayout mButton_Start1;
    @BindView(R.id.Button_Start2) MaterialRippleLayout mButton_Start2;
    @BindView(R.id.Button_Subscribe) MaterialRippleLayout mButton_Subscribe;
    @BindView(R.id.Text_Price) TextView mText_Price;

    void setSize_BgImage() {
        ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) mImage_Bg.getLayoutParams();
        lp.width = Utils.getScreenWidth(this);
        lp.height = Utils.convSizeByRatio(this, DESIGNED_HEIGHT___BG_IMAGE, lp.width/(float) DESIGNED_WIDTH___BG_IMAGE);

        initStart();
        initSubscribe();
    }

    void initStart() {
        if(Utils.isAllowedAllContent()) {
            mButton_Start1.setVisibility(View.GONE);
            mButton_Start2.setVisibility(View.VISIBLE);
            mButton_Subscribe.setVisibility(View.GONE);
        } else {
            mButton_Start1.setVisibility(View.VISIBLE);
            mButton_Start2.setVisibility(View.GONE);
            mButton_Subscribe.setVisibility(View.VISIBLE);
        }
    }

    void initSubscribe() {
//        mText_Price.setText(String.format("매달 %s원", Utils.addCommaInNumber(SharedInfo.Subscription.mPrice)));
    }

    /* ======================================================================================================================== */
    // 학습 GOGO
    /* ======================================================================================================================== */

    @OnClick({R.id.Button_Start1, R.id.Button_Start2})
    public void clickStart() {
        Intent it = new Intent(Home___Activity.this, Books___Activity.class);
        startActivity(it);
        finish();
    }


    /* ======================================================================================================================== */
    /* 구독 GOGO
    /* ========================================================================================================================= */

    @OnClick(R.id.Button_Subscribe)
    public void clickBottom() {
        Intent it = new Intent(Home___Activity.this, Subs___Activity.class);
        startActivity(it);
    }


    /* ======================================================================================================================== */
    // 권한 체크
    /* ======================================================================================================================== */

    void initPermission() {
        /* 권한이 없으면 권한 획득을 위한 Dialog를 연다 */
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {

            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECORD_AUDIO)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, 0);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, 0);
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for(int xx = 0; xx < grantResults.length; xx++) {
            if(permissions[xx].equals(Manifest.permission.RECORD_AUDIO) == true && grantResults[xx] != 0) {
                MaiDialog.build(this)
                        .setImage(R.mipmap.mic)
                        .setMessage("마이크 권한이 허용되지 않으면\nmAI English의 서비스를 이용할 수 없습니다.")
                        .setOk("권한 설정 가기", mOnClick_GoPermission)
                        .setCancel("취소", null)
                        .show();
            }
        }
    }

    View.OnClickListener mOnClick_GoPermission = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent appDetail = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
            appDetail.addCategory(Intent.CATEGORY_DEFAULT);
            appDetail.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivityForResult(appDetail, RC___CHG_PERMISSION);
        }
    };


}
