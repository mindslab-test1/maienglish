package ai.mindslab.maienglish.Study;

import android.content.Context;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import ai.mindslab.maienglish.Common.SharedInfo;

public class StudyPractice___Adapter extends FragmentPagerAdapter {

    Handler mHandler;
    Context mContext;

    public StudyPractice___Adapter(Context context, FragmentManager fm, Handler handler) {
        super(fm);
        mHandler = handler;
        mContext = context;
    }

    @Override
    public int getCount() {
        return SharedInfo.Study.mList_PatternContent.size();
    }

    @Override
    public Fragment getItem(int position) {
        return StudyPractice_Sub___Fragment.newInstance(mContext, mHandler, position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }
}