package ai.mindslab.maienglish.Study;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;

import java.io.IOException;

import ai.mindslab.maienglish.Common.ConstDef;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StudyChallenge_Sub___Fragment extends Fragment {
    View mLayout;
    Handler mHandler;
    int mPosition;
    Context mContext;

    public static StudyChallenge_Sub___Fragment newInstance(Context context, Handler handler, int position) {
        StudyChallenge_Sub___Fragment fragment = new StudyChallenge_Sub___Fragment();
        fragment.mHandler = handler;
        fragment.mPosition = position;
        fragment.mContext = context;
        fragment.initAudio();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mLayout = inflater.inflate(R.layout.study_challenge_sub___fragment, container, false);
        ButterKnife.bind(this, mLayout);

        initUserPart();
        initHint();
        return mLayout;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("AAA", "StudyRepeat_Sub___Fragment .............................................. onResume()");

    }

    @Override
    public void onPause() {
        super.onPause();

        Log.e("AAA", "StudyRepeat_Sub___Fragment .............................................. onPause()");
        stopAudio();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        Log.e("AAA", "StudyRepeat_Sub___Fragment .............................................. onDestroyView()");
        releaseAudio();
    }

    /* ##################################################################################################### */
    //

    void study() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                playAudio();
            }
        }, 700);
    }


    /* ##################################################################################################### */
    // USER PART

    @BindView(R.id.Text_UserKor) TextView mText_UserKor;

    void initUserPart() {
        mText_UserKor.setText(SharedInfo.Study.mList_PatternContent.get(mPosition).getTranslation(mContext));
    }

    /* ######################################################################################################################## */
    /* HINT */

    @BindView(R.id.Text_Hint) TextView mText_Hint;
    @BindView(R.id.Button_Hint) ImageView mButton_Hint;
    @BindView(R.id.Button_Hint2) ImageView mButton_Hint2;

    void initHint() {
        mText_Hint.setText(SharedInfo.Study.mList_PatternContent.get(mPosition).getHint_challenge());
    }

    @OnClick({R.id.Button_Hint, R.id.Button_Hint2, R.id.Text_Hint})
    void toggleHint(View view) {
        if(mText_Hint.getVisibility() == View.VISIBLE) {
            mText_Hint.setVisibility(View.GONE);
            mButton_Hint.setVisibility(View.VISIBLE);
            mButton_Hint2.setVisibility(View.GONE);
        } else {
            mText_Hint.setVisibility(View.VISIBLE);
            mButton_Hint.setVisibility(View.GONE);
            mButton_Hint2.setVisibility(View.VISIBLE);
        }
    }


    /* ##################################################################################################### */
    // SOUND

    MediaPlayer mMediaPlayer;
    Uri mUri;
    void initAudio() {
        Log.e("AAA", "[ Audio ] ................... init");
        mUri = Uri.parse(ConstDef.BASE_MAI_AUDIO_URL + SharedInfo.Study.mList_PatternContent.get(mPosition).getVoice_path());

        mMediaPlayer = MediaPlayer.create(mContext, mUri);
        mMediaPlayer.setOnCompletionListener(mOnCompletion_Audio);
        mMediaPlayer.setOnErrorListener(mOnError_Audio);
        mMediaPlayer.setOnPreparedListener(mOnPrepared_Audio);
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
    }

    void playAudio() {
        Log.e("AAA", "[ Study ] Audio = " + ConstDef.BASE_MAI_AUDIO_URL + SharedInfo.Study.mList_PatternContent.get(mPosition).getVoice_path());
        stopAudio();
        mMediaPlayer.start();
    }

    void stopAudio() {
        if(mMediaPlayer != null && mMediaPlayer.isPlaying()) mMediaPlayer.stop();
    }

    void releaseAudio() {
        if(mMediaPlayer == null) return;

        stopAudio();
        mMediaPlayer.release();
        mMediaPlayer = null;
    }

    MediaPlayer.OnCompletionListener mOnCompletion_Audio = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            Log.e("AAA", "[ Audio ] ................ onCompletion()");

        }
    };

    MediaPlayer.OnErrorListener mOnError_Audio = new MediaPlayer.OnErrorListener() {
        @Override
        public boolean onError(MediaPlayer mp, int what, int extra) {
            Log.e("AAA", "[ Audio ] ................ onError(), what = " + what + ", extra = " + extra);
            return false;
        }
    };

    MediaPlayer.OnPreparedListener mOnPrepared_Audio = new MediaPlayer.OnPreparedListener() {
        @Override
        public void onPrepared(MediaPlayer mp) {
            Log.e("AAA", "[ Audio ] ................ onPrepared()");
        }
    };

    @OnClick(R.id.Button_Sound)
    void onClick_Sound(View view) {
        playAudio();
    }
}
