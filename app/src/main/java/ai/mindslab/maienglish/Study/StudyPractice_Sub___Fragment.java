package ai.mindslab.maienglish.Study;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.zip.Inflater;

import ai.mindslab.maienglish.Common.ConstDef;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static ai.mindslab.maienglish.Common.ConstDef.HINT_FONT_SIZE;

public class StudyPractice_Sub___Fragment extends Fragment {
    View mLayout;
    Handler mHandler;
    int mPosition;
    Context mContext;

    public static StudyPractice_Sub___Fragment newInstance(Context context, Handler handler, int position) {
        StudyPractice_Sub___Fragment fragment = new StudyPractice_Sub___Fragment();
        fragment.mHandler = handler;
        fragment.mPosition = position;
        fragment.mContext = context;
        fragment.initAudio();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mLayout = inflater.inflate(R.layout.study_practice_sub___fragment, container, false);
        ButterKnife.bind(this, mLayout);

        initUserPart();
        return mLayout;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("AAA", "StudyRepeat_Sub___Fragment .............................................. onResume()");

    }

    @Override
    public void onPause() {
        super.onPause();

        Log.e("AAA", "StudyRepeat_Sub___Fragment .............................................. onPause()");
        stopAudio();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        Log.e("AAA", "StudyRepeat_Sub___Fragment .............................................. onDestroyView()");
        releaseAudio();
    }

    /* ##################################################################################################### */
    //

    void study() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                playAudio();
            }
        }, 700);
    }

    /* ##################################################################################################### */
    // USER PART

    @BindView(R.id.Text_UserEng) TextView mText_UserEng;
    @BindView(R.id.Text_UserKor) TextView mText_UserKor;
    @BindView(R.id.Layer_Hint) FlexboxLayout mLayer_Hint;

    void initUserPart() {
        mText_UserEng.setText(SharedInfo.Study.mList_PatternContent.get(mPosition).getSentence_practice());
        mText_UserKor.setText(SharedInfo.Study.mList_PatternContent.get(mPosition).getTranslation(mContext));

        buildHints();
    }

    void buildHints() {
        String [] splitedHints = SharedInfo.Study.mList_PatternContent.get(mPosition).getHint_practice().split("/");
        List<String> listHint = new ArrayList<String>();
        for(int xx = 0; xx < splitedHints.length; xx++) {
            listHint.add(splitedHints[xx].trim());
        }

        Random random = new Random();
        for(int xx = 0; xx < splitedHints.length; xx++) {
            int selectNum = Math.abs(random.nextInt()%listHint.size());

            String hint = listHint.remove(selectNum);
            addHint(hint);
        }
    }

    void addHint(String hint) {
        TextView textView = new TextView(this.getContext());
        textView.setPadding(
                Utils.dp2px(this.getContext(), 8),
                Utils.dp2px(this.getContext(), 3),
                Utils.dp2px(this.getContext(), 8),
                Utils.dp2px(this.getContext(), 3));
        textView.setText(hint);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, HINT_FONT_SIZE);
        textView.setTextColor(getResources().getColor(R.color.hint_text));
        textView.setBackgroundResource(R.drawable.frame___hint);

        mLayer_Hint.addView(textView);
    }


    /* ##################################################################################################### */
    // SOUND

    MediaPlayer mMediaPlayer;
    Uri mUri;
    void initAudio() {
        Log.e("AAA", "[ Audio ] ................... init");
        mUri = Uri.parse(ConstDef.BASE_MAI_AUDIO_URL + SharedInfo.Study.mList_PatternContent.get(mPosition).getVoice_path());

        mMediaPlayer = MediaPlayer.create(mContext, mUri);
        mMediaPlayer.setOnCompletionListener(mOnCompletion_Audio);
        mMediaPlayer.setOnErrorListener(mOnError_Audio);
        mMediaPlayer.setOnPreparedListener(mOnPrepared_Audio);
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
    }

    void playAudio() {
        Log.e("AAA", "[ Study ] Audio = " + ConstDef.BASE_MAI_AUDIO_URL + SharedInfo.Study.mList_PatternContent.get(mPosition).getVoice_path());
        stopAudio();
        mMediaPlayer.start();
    }

    void stopAudio() {
        if(mMediaPlayer != null && mMediaPlayer.isPlaying()) mMediaPlayer.stop();
    }

    void releaseAudio() {
        if(mMediaPlayer == null) return;

        stopAudio();
        mMediaPlayer.release();
        mMediaPlayer = null;
    }

    MediaPlayer.OnCompletionListener mOnCompletion_Audio = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            Log.e("AAA", "[ Audio ] ................ onCompletion()");

        }
    };

    MediaPlayer.OnErrorListener mOnError_Audio = new MediaPlayer.OnErrorListener() {
        @Override
        public boolean onError(MediaPlayer mp, int what, int extra) {
            Log.e("AAA", "[ Audio ] ................ onError(), what = " + what + ", extra = " + extra);
            return false;
        }
    };

    MediaPlayer.OnPreparedListener mOnPrepared_Audio = new MediaPlayer.OnPreparedListener() {
        @Override
        public void onPrepared(MediaPlayer mp) {
            Log.e("AAA", "[ Audio ] ................ onPrepared()");
        }
    };

    @OnClick(R.id.Button_Sound)
    void onClick_Sound(View view) {
        playAudio();
    }
}
