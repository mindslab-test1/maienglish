package ai.mindslab.maienglish.Study;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.badoualy.stepperindicator.StepperIndicator;

import java.util.Map;

import ai.mindslab.maienglish.Model.StudyHistoryVO;
import ai.mindslab.maienglish.Model.StudyResultVO;
import ai.mindslab.maienglish.RestAPI.Param;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class StudyRepeat___Fragment extends Study___Fragment {
    View mLayout;
    Handler mHandler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mLayout = inflater.inflate(R.layout.study_repeat___fragment, container, false);
        ButterKnife.bind(this, mLayout);

        return mLayout;
    }


    /* ###################################################################################################################### */
    // STUDY CONTROLLER

    public void start(Handler handler) {
        mHandler = handler;
        initPager();
    }

    public void onResult(Map<String, Object> resultMap) {
        Map<String, Object> result = (Map<String, Object>) resultMap.get(Param.STT_RESULT);

        String answerText = (String) result.get(Param.STT_ANSWER_TEXT);
        String userText = (String) result.get(Param.STT_USER_TEXT);
        String userRecordPath = (String) result.get(Param.STT_RECORD_URL);
        int ascore = Integer.parseInt((String) result.get(Param.STT_GRAMMAR_SCORE));
        int pscore = Integer.parseInt((String) result.get(Param.STT_PRONOUNCE_SCORE));

        StudyResultVO resultVO = new StudyResultVO();
        resultVO.setContent_id(SharedInfo.Study.mList_PatternContent.get(mPager.getCurrentItem()).getContent_id());
        resultVO.setPattern_id(SharedInfo.Study.mList_PatternContent.get(mPager.getCurrentItem()).getPattern_id());
        resultVO.setType(StudyResultVO.TYPE___REPEAT);
        resultVO.setSentence(answerText);
        resultVO.setUser_text(userText);
        resultVO.setAscore(ascore);
        resultVO.setPscore(pscore);
        resultVO.setGrade(StudyResultVO.calcGrade(ascore, pscore));

        SttResultDialog.build(mContext)
                .setGrade(StudyResultVO.convGrade(resultVO.getGrade()))
                .setAnswerText(answerText)
                .setUserText(userText, userRecordPath)
                .setAccScore(ascore)
                .setPronScore(pscore)
                .setOk(mOnClick_Next)
                .setRetry(mOnClick_Retry)
                .show();

        Log.e("AAA", "---------------------------> Result show()");

        Message msg = mHandler.obtainMessage(Study___Activity.MSG___REPEAT_RESULT, mPager.getCurrentItem(), 0, resultVO);
        mHandler.sendMessage(msg);
    }

    View.OnClickListener mOnClick_Next = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(mPager.getCurrentItem() == mStepIndicator.getStepCount()-1) {
                mHandler.sendEmptyMessage(Study___Activity.MSG___START_PRACTICE);
            }
            else mPager.setCurrentItem(mPager.getCurrentItem() + 1, true);
        }
    };

    View.OnClickListener mOnClick_Retry = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ((StudyRepeat_Sub___Fragment) mAdapter.getItem(mPager.getCurrentItem())).study();
        }
    };

    /* ###################################################################################################################### */
    /* PAGER */

    @BindView(R.id.StepIndicator) StepperIndicator mStepIndicator;
    @BindView(R.id.ViewPager) ViewPager_NoSwiping mPager;

    StudyRepeat___Adapter   mAdapter;
    void initPager() {
        mAdapter = new StudyRepeat___Adapter(mContext, getChildFragmentManager(), mHandler);
        mPager.addOnPageChangeListener(mOnPageChange);
        mPager.setAdapter(mAdapter);

        mStepIndicator.setViewPager(mPager);
        mStepIndicator.setStepCount(SharedInfo.Study.mList_PatternContent.size());
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) mStepIndicator.getLayoutParams();
        lp.width = Utils.dp2px(mContext, 16)*SharedInfo.Study.mList_PatternContent.size() + Utils.dp2px(mContext, 16)*(SharedInfo.Study.mList_PatternContent.size()-1);
        mStepIndicator.setVisibility(View.VISIBLE);
        mOnPageChange.onPageSelected(0);
    }

    ViewPager.OnPageChangeListener mOnPageChange = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) { }

        @Override
        public void onPageScrollStateChanged(int i) { }

        @Override
        public void onPageSelected(int pageNo) {
            Log.e("AAA", "onPageSelected() ....................... pageNo = " + pageNo);
            ((StudyRepeat_Sub___Fragment) mAdapter.getItem(pageNo)).study();

            if(pageNo < SharedInfo.Study.mList_PatternContent.size()) {
                mHandler.sendMessage(mHandler.obtainMessage(Study___Activity.MSG___OFF_MIC, SharedInfo.Study.mList_PatternContent.get(pageNo).getSentence_eval()));
            }
        }
    };
}
