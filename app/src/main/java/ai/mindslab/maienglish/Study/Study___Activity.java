package ai.mindslab.maienglish.Study;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ai.mindslab.maienglish.Book.Books___Activity;
import ai.mindslab.maienglish.Common.ConstDef;
import ai.mindslab.maienglish.Common.MaiDialog;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.Model.RestApiVO;
import ai.mindslab.maienglish.Model.StudyHistoryVO;
import ai.mindslab.maienglish.Model.StudyResultVO;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.RestAPI.ContentAPI;
import ai.mindslab.maienglish.RestAPI.ResCode;
import ai.mindslab.maienglish.RestAPI.StudyAPI;
import ai.mindslab.maienglish.STT.SttHandler;
import ai.mindslab.maienglish.SideMenu.SideMenu___Activity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Study___Activity extends AppCompatActivity {

    String mAnswerText = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.study___activity);
        ButterKnife.bind(this);
        Utils.setColor_StatusBarIcon(this, true, this.getResources().getColor(R.color.sys_bg___ad));

        initAPI();
        initPatternInfo();

        loadData();
    }

    @Override
    public void onPause() {
        super.onPause();

        /* 마이크가 활성화 되어 있으면 Off 상태로 설정한다. */
        if(mSttHandler.isIdle() == false) {
            mSttHandler.setReady(mAnswerText);
        }
    }

    /* ###################################################################################################### */
    // PATTERN INFO

    @BindView(R.id.Text_PatternNo) TextView mText_PatternNo;
    @BindView(R.id.Text_Pattern) TextView mText_Pattern;

    void initPatternInfo() {
        mText_PatternNo.setText("Pattern " + SharedInfo.Study.mPattern.getOrder_no());
        mText_Pattern.setText(SharedInfo.Study.mPattern.getSentence());
    }


    /* ###################################################################################################### */

    public static final int PAGE___REPEAT = 0;
    public static final int PAGE___PRACTICE = 1;
    public static final int PAGE___CHALLENGE = 2;

    @BindView(R.id.Tabs) SmartTabLayout mTabs;
    @BindView(R.id.ViewPager) ViewPager_NoSwiping mViewPager;

    void initViewPager() {
        Study___Fragment.mContext = this;
        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add("Repeat", StudyRepeat___Fragment.class)
                .add("Practice", StudyPractice___Fragment.class)
                .add("Challenge", StudyChallenge___Fragment.class)
                .create());
        mViewPager.setAdapter(adapter);
        mTabs.setViewPager(mViewPager);

        Typeface typeface = ResourcesCompat.getFont(this, R.font.noto_medium);
        TextView textView = (TextView) mTabs.getTabAt(0);
        textView.setTypeface(typeface);
        textView.setIncludeFontPadding(false);
        textView.setTextColor(getResources().getColor(R.color.study___tab_text___active));

        textView = (TextView) mTabs.getTabAt(1);
        textView.setTypeface(typeface);
        textView.setIncludeFontPadding(false);
        textView.setTextColor(getResources().getColor(R.color.study___tab_text___normal));

        textView = (TextView) mTabs.getTabAt(2);
        textView.setTypeface(typeface);
        textView.setIncludeFontPadding(false);
        textView.setTextColor(getResources().getColor(R.color.study___tab_text___normal));

        mTabs.setVisibility(View.VISIBLE);
    }

    void goNextStep() {
        mViewPager.setCurrentItem(mViewPager.getCurrentItem()+1);
    }

    /* ###################################################################################################### */
    // MIC UI

    @BindView(R.id.EffectMic) ImageView mEffectMic;
    @BindView(R.id.Button_Mic) ImageView mButton_Mic;

    SttHandler mSttHandler;
    void initMic() {
        mSttHandler = new SttHandler(this, mHandler, mButton_Mic, mEffectMic);
    }



    /* ###################################################################################################### */
    // LOAD DATA

    void loadData() {
        mAPI_Content.getContentList(SharedInfo.User.mAuthKey, SharedInfo.Study.mPattern.getPattern_id()).enqueue(new Callback<RestApiVO>() {
            @Override
            public void onResponse(@NonNull Call<RestApiVO> call, @NonNull Response<RestApiVO> response) {
                if(response.code() == 401) {
                    Toast.makeText(Study___Activity.this, "인증이 되지 않았습니다. ", Toast.LENGTH_SHORT).show();
                    return;
                }
                RestApiVO body = response.body();
                if (body != null && body.getRes_code() == ResCode.SUCC) {
                    SharedInfo.Study.mList_PatternContent.clear();
                    SharedInfo.Study.mList_PatternContent.addAll(body.getContent_list());

                    Log.e("AAA", "Content List = " + SharedInfo.Study.mList_PatternContent.toString());

                    initViewPager();
                    initMic();
                    mHandler.sendEmptyMessage(MSG___START_REPEAT);
                } else {
                    Toast.makeText(Study___Activity.this, "네트워크가 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestApiVO> call, @NonNull Throwable t) {
                Log.e("AAA", "API.login() >> onFailure() >> " + t.toString());
                Toast.makeText(Study___Activity.this, "네트워크가 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
            }
        });
    }



    /* ###################################################################################################### */
    // HANDLER

    static public final int MSG___START_REPEAT = 1;
    static public final int MSG___START_PRACTICE = 2;
    static public final int MSG___START_CHALLENGE = 3;
    static public final int MSG___END_PATTERN = 4;
    static public final int MSG___DISABLE_MIC = 10;
    static public final int MSG___OFF_MIC = 11;
    static public final int MSG___ON_MIC = 12;
    static public final int MSG___REPEAT_RESULT = 21;
    static public final int MSG___PRACTICE_RESULT = 22;
    static public final int MSG___CHALLENGE_RESULT = 23;

    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            TextView textView = null;

            switch(msg.what) {
                case MSG___START_REPEAT:
                    mCurrentStep = PAGE___REPEAT;
                    mViewPager.setCurrentItem(PAGE___REPEAT);
                    ((Study___Fragment) ((FragmentPagerItemAdapter) mViewPager.getAdapter()).getPage(PAGE___REPEAT)).start(mHandler);

                    textView = (TextView) mTabs.getTabAt(0);
                    textView.setTextColor(getResources().getColor(R.color.study___tab_text___active));
                    break;

                case MSG___START_PRACTICE:
                    mCurrentStep = PAGE___PRACTICE;
                    mViewPager.setCurrentItem(PAGE___PRACTICE);
                    ((Study___Fragment) ((FragmentPagerItemAdapter) mViewPager.getAdapter()).getPage(PAGE___PRACTICE)).start(mHandler);

                    textView = (TextView) mTabs.getTabAt(0);
                    textView.setTextColor(getResources().getColor(R.color.study___tab_text___normal));
                    textView = (TextView) mTabs.getTabAt(1);
                    textView.setTextColor(getResources().getColor(R.color.study___tab_text___active));
                    break;

                case MSG___START_CHALLENGE:
                    mCurrentStep = PAGE___CHALLENGE;
                    mViewPager.setCurrentItem(PAGE___CHALLENGE);
                    ((Study___Fragment) ((FragmentPagerItemAdapter) mViewPager.getAdapter()).getPage(PAGE___CHALLENGE)).start(mHandler);

                    textView = (TextView) mTabs.getTabAt(1);
                    textView.setTextColor(getResources().getColor(R.color.study___tab_text___normal));
                    textView = (TextView) mTabs.getTabAt(2);
                    textView.setTextColor(getResources().getColor(R.color.study___tab_text___active));
                    break;

                case MSG___END_PATTERN:
                    endPattern();
                    break;

                case MSG___DISABLE_MIC:
                    mSttHandler.setIdle();
                    break;

                case MSG___OFF_MIC:
                    mAnswerText = (String) msg.obj;
                    mSttHandler.setReady((String) msg.obj);
                    break;

                case MSG___ON_MIC:
                    mAnswerText = (String) msg.obj;
                    mSttHandler.setStart((String) msg.obj);
                    break;

                case SttHandler.MSG___STT_RESPONSE:
                    ((Study___Fragment) ((FragmentPagerItemAdapter) mViewPager.getAdapter()).getPage(mViewPager.getCurrentItem())).onResult((Map<String, Object>) msg.obj);
                    break;

                case MSG___REPEAT_RESULT:
                    regiResult(mList_RepeatResult, msg.arg1, (StudyResultVO) msg.obj);
                    break;
                case MSG___PRACTICE_RESULT:
                    regiResult(mList_PracticeResult, msg.arg1, (StudyResultVO) msg.obj);
                    break;
                case MSG___CHALLENGE_RESULT:
                    regiResult(mList_ChallengeResult, msg.arg1, (StudyResultVO) msg.obj);
                    break;
            }
        }
    };


    /* ============================================================================================================================================== */
    /* API */
    /* ============================================================================================================================================== */

    ContentAPI mAPI_Content;
    StudyAPI mAPI_Study;

    void initAPI() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConstDef.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mAPI_Content = retrofit.create(ContentAPI.class);
        mAPI_Study = retrofit.create(StudyAPI.class);
    }


    /* ############################################################################################################ */
    /* 상단 공통 버턴 */

    @OnClick(R.id.Button_TopLogo)
    void onCLick_TopLog(View view) {
        Intent it = new Intent(this, Books___Activity.class);
        startActivity(it);
        finish();
    }

    @OnClick(R.id.Button_Hambuger)
    void onCLick_SideMenu(View view) {
        Intent it = new Intent(this, SideMenu___Activity.class);
        startActivity(it);
    }

    /* =================================================================================================================================== */
    // 학습 결과

    int mCurrentStep ;

    List<StudyResultVO> mList_RepeatResult = new ArrayList<StudyResultVO>();
    List<StudyResultVO> mList_PracticeResult = new ArrayList<StudyResultVO>();
    List<StudyResultVO> mList_ChallengeResult = new ArrayList<StudyResultVO>();

    void regiResult(List<StudyResultVO> list, int contentIdx, StudyResultVO study) {
        if(contentIdx == list.size()) list.add(study);
        else list.set(contentIdx, study);
    }


    void endPattern() {
        if(SharedInfo.User.isLogined()) registerStudyHistory();
        SharedInfo.Study.mPattern.setStudied_flag(1);

        MaiDialog.build(this)
                .setMessage(getString(R.string.study_completed))
                .setImage(R.mipmap.info)
                .setCancel(getString(R.string.go_list), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                })
                .show();
    }

    void registerStudyHistory() {
        List<StudyResultVO> list = new ArrayList<StudyResultVO>();

        list.addAll(mList_RepeatResult);
        list.addAll(mList_PracticeResult);
        list.addAll(mList_ChallengeResult);

        Gson gson = new Gson();
        String json_str = gson.toJson(list);

        mAPI_Study.addStudyHistory(SharedInfo.User.mAuthKey, list).enqueue(new Callback<RestApiVO>() {
            @Override
            public void onResponse(@NonNull Call<RestApiVO> call, @NonNull Response<RestApiVO> response) {
                if(response.code() == 401) {
                    Toast.makeText(Study___Activity.this, "인증이 되지 않았습니다.", Toast.LENGTH_SHORT).show();
                    return;
                }
                RestApiVO body = response.body();
                if (body != null && body.getRes_code() == ResCode.SUCC) {
//                    Toast.makeText(Study___Activity.this, "학습 이력 등록되었습니다.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(Study___Activity.this, "네트워크가 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestApiVO> call, @NonNull Throwable t) {
                Log.e("AAA", "API.login() >> onFailure() >> " + t.toString());
                Toast.makeText(Study___Activity.this, "네트워크가 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
