package ai.mindslab.maienglish.Study;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.squareup.picasso.Picasso;

import java.security.KeyStore;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;

import ai.mindslab.maienglish.Common.ConstDef;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SttResultDialog {
    private Context mContext;
    private Dialog mDialog;

    TextView mText_Grade;
    TextView mText_Answer;
    TextView mText_User;
    TextView mText_AccScore;
    TextView mText_PronScore;
    MaterialRippleLayout mButton_User;
    MaterialRippleLayout mButton_Retry;
    MaterialRippleLayout mButton_Ok;

    MediaPlayer mPlayer = new MediaPlayer();

    public static SttResultDialog build(Context context) {
        SttResultDialog maiDialog = new SttResultDialog();
        maiDialog.mContext = context;

        maiDialog.mDialog = new Dialog(maiDialog.mContext);
        maiDialog.mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        maiDialog.mDialog.setContentView(R.layout.stt_result_dialog);

        maiDialog.mText_Grade = maiDialog.mDialog.findViewById(R.id.Text_Grade);
        maiDialog.mText_Answer = maiDialog.mDialog.findViewById(R.id.Text_Answer);
        maiDialog.mText_User = maiDialog.mDialog.findViewById(R.id.Text_User);
        maiDialog.mText_AccScore = maiDialog.mDialog.findViewById(R.id.Text_AccScore);
        maiDialog.mText_PronScore = maiDialog.mDialog.findViewById(R.id.Text_PronScore);
        maiDialog.mButton_Retry = maiDialog.mDialog.findViewById(R.id.Button_Retry);
        maiDialog.mButton_Ok = maiDialog.mDialog.findViewById(R.id.Button_Ok);
        maiDialog.mButton_User = maiDialog.mDialog.findViewById(R.id.Button_User);
//        maiDialog.mButton_User.setOnClickListener(maiDialog.mOnClick_User);

        maiDialog.mButton_Ok.setOnClickListener(maiDialog.mOnClick___DefaultOk);
        maiDialog.mButton_Retry.setOnClickListener(maiDialog.mOnClick___DefaultRetry);

        Window window = maiDialog.mDialog.getWindow();
        window.setLayout(Utils.getScreenWidth((Activity)(maiDialog.mContext)) - Utils.dp2px(context, 32)*2,-2);
        maiDialog.mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(context.getResources().getColor(R.color.dialog___outside___bg)));

        maiDialog.mDialog.setCancelable(false);
        return maiDialog;
    }

    public SttResultDialog setGrade(String grade) {
        mText_Grade.setText(grade);
        return this;
    }

    public SttResultDialog setAnswerText(String answerText) {
        mText_Answer.setText(answerText);
        return this;
    }

    public SttResultDialog setUserText(String userText, final String url) {

        try {
            Log.e("AAA", "Record URL = " + ConstDef.BASE_STT_AUDIO_URL + url);
            Uri uri = Uri.parse(ConstDef.BASE_STT_AUDIO_URL + url);

            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mPlayer.setDataSource(mContext, uri);
            mPlayer.prepareAsync();

        } catch (Exception e) { e.printStackTrace();
            Log.e("AAA", "Exception = " + e.toString());
            Log.e("AAA", "Exception = " + e.getMessage());
        }

        mText_User.setText(Html.fromHtml(userText + " &nbsp; <img src='sound_sky.png'>", new ImageGetter(), null));
        mButton_User.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPlayer.seekTo(0);
                mPlayer.start();
            }
        });
        return this;
    }

    public SttResultDialog setAccScore(int accScore) {
        mText_AccScore.setText("" + accScore);
        return this;
    }

    public SttResultDialog setPronScore(int pronScore) {
        mText_PronScore.setText("" + pronScore);
        return this;
    }

    public SttResultDialog setOk(View.OnClickListener listener) {
        mOnClick_Ok = listener;
        return this;
    }

    public SttResultDialog setRetry(View.OnClickListener listener) {
        mOnClick_Retry = listener;
        return this;
    }

    public SttResultDialog show() {
        mDialog.show();
        return this;
    }

    public SttResultDialog hide() {
        mDialog.hide();
        return this;
    }

    public SttResultDialog dismiss() {
        mDialog.dismiss();
        return this;
    }

    /* ############################################################################################ */
    // CALLBACK

    View.OnClickListener mOnClick___DefaultOk = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mPlayer.stop();
            if(mOnClick_Ok != null) mOnClick_Ok.onClick(v);
            mDialog.dismiss();
        }
    };
    View.OnClickListener mOnClick___DefaultRetry = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mPlayer.stop();
            if(mOnClick_Retry != null) mOnClick_Retry.onClick(v);
            mDialog.dismiss();
        }
    };


    View.OnClickListener mOnClick_Ok = null;
    View.OnClickListener mOnClick_Retry = null;

    /* ############################################################################################ */
    // HTML Image getter

    private class ImageGetter implements  Html.ImageGetter {
        public Drawable getDrawable(String source) {
            int id;
            if (source.equals("sound_sky.png")) {
                id = R.mipmap.sound_sky;
            }
            else {
                return null;
            }

            Drawable d = mContext.getResources().getDrawable(id);
            d.setBounds(0,0,d.getIntrinsicWidth(),d.getIntrinsicHeight());
            return d;
        }
    }
}
