package ai.mindslab.maienglish.SideMenu;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.balysv.materialripple.MaterialRippleLayout;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import ai.mindslab.maienglish.Common.ConstDef;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.Coupon.Coupon___Activity;
import ai.mindslab.maienglish.ExternLib.RippleLayout;
import ai.mindslab.maienglish.Model.RestApiVO;
import ai.mindslab.maienglish.My.My___Activity;
import ai.mindslab.maienglish.Notice.Notice___Activity;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.Realm.Realm_User;
import ai.mindslab.maienglish.Realm.UserFactory;
import ai.mindslab.maienglish.RestAPI.Param;
import ai.mindslab.maienglish.RestAPI.ResCode;
import ai.mindslab.maienglish.RestAPI.UserAPI;
import ai.mindslab.maienglish.SchedulerTask.Worker_FreeCoupon;
import ai.mindslab.maienglish.Setup.Setup___Activity;
import ai.mindslab.maienglish.Subscribe.SubsInfo___Activity;
import ai.mindslab.maienglish.Subscribe.Subs___Activity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SideMenu___Activity extends AppCompatActivity implements  GoogleApiClient.OnConnectionFailedListener {

    final int RC___GOOGLE_LOGIN = 100;

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.side_menu___activity);
        Utils.setColor_StatusBarIcon(this, true, this.getResources().getColor(R.color.sys_bg___side_menu));
        ButterKnife.bind(this);

        Realm.init(this);
        realm = Realm.getDefaultInstance();

        /* 구글 로그인 초기화 */
        initGoogleLogin();
        initCloseButton();
        initAPI();
        initRule();
    }

    @Override
    public void onResume() {
        super.onResume();

        initUI();
    }

    void initUI() {
        initLogin();
        initLogout();
        initMyInfo();
        initSubs();
        initNotice();
        initCoupon();
        initSetup();
        initSupport();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("AAA", String.format("onActivityResult.................... requestCode = %d, result=%d", requestCode, resultCode) );
        // 구글 로그인 결과
        if (requestCode == RC___GOOGLE_LOGIN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleGoogleLogin(task);
        }
    }


    /* ############################################################################################# */
    // Close

    @BindView(R.id.Button_Close) MaterialRippleLayout mButton_Close;

    void initCloseButton() {
        mButton_Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }




    /* ############################################################################################# */
    // 로그아웃

    @BindView(R.id.Button_Logout) MaterialRippleLayout mButton_Logout;

    void initLogout() {
        if(SharedInfo.User.isLogined()) {
            Log.e("AAA", "[ 로그아웃 버튼 ] 현재 사용자님은 로그인 중입니다.");
            mButton_Logout.setVisibility(View.VISIBLE);
        }
        else {
            Log.e("AAA", "[ 로그아웃 버튼 ] 현재 사용자님은 비로그인 중입니다.");
            mButton_Logout.setVisibility(View.GONE);
        }

        mButton_Logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
    }

    void logout() {
        /* 사용자 정보 초기화 */
        SharedInfo.User.reset();

        /* DB 로그인 정보 초기화 */
        UserFactory.logout(realm);

        /* 구독 여부 초기화 */
        SharedInfo.Subscription.reset();

        SharedInfo.Coupon.setFreeCoupon(null);
        Worker_FreeCoupon.release();

        /* UI 초기화 */
        initUI();

        mAPI_User.logout(SharedInfo.User.mAuthKey).enqueue(new Callback<RestApiVO>() {
            @Override
            public void onResponse(@NonNull Call<RestApiVO> call, @NonNull Response<RestApiVO> response) {
                /* 처리 결과에 상관 없음 */
            }

            @Override
            public void onFailure(@NonNull Call<RestApiVO> call, @NonNull Throwable t) {
                /* 처리 결과에 상관 없음 */
            }
        });

        Toast.makeText(SideMenu___Activity.this, "로그아웃 되셨습니다.", Toast.LENGTH_SHORT).show();
    }



    /* ############################################################################################# */
    // 내정보

    @BindView(R.id.Button_MyInfo) MaterialRippleLayout mButton_MyInfo;

    void initMyInfo() {
        if(SharedInfo.User.isLogined()) mButton_MyInfo.setVisibility(View.VISIBLE);
        else mButton_MyInfo.setVisibility(View.GONE);
    }

    @OnClick(R.id.Button_MyInfo)
    void goMyInfo(View v) {
        Intent it = new Intent(this, My___Activity.class);
        startActivity(it);
    }

    /* ############################################################################################# */
    // 구독하기

    @BindView(R.id.Text_Subs) TextView mText_Subs;

    void initSubs() {
        if(SharedInfo.Subscription.isSubscribed()) {
            mText_Subs.setText(getString(R.string.subscribing));
        }
        else {
            mText_Subs.setText(getString(R.string.subscribe));
        }
    }

    @OnClick({R.id.Button_Subs, R.id.Button_Subscribe2})
    void goSubs(View v) {
        if(SharedInfo.Subscription.isSubscribed()) {
            Intent it = new Intent(SideMenu___Activity.this, SubsInfo___Activity.class);
            startActivity(it);
        }
        else {
            Intent it = new Intent(SideMenu___Activity.this, Subs___Activity.class);
            startActivity(it);
        }
    }


    /* ############################################################################################# */
    // 공지사항

    @BindView(R.id.Button_Notice) MaterialRippleLayout mButton_Notice;
    @BindView(R.id.Text_NoticeCount) TextView mText_NoticeCount;

    void initNotice() {
        mText_NoticeCount.setText("" + SharedInfo.Notice.mNewNoticeCount);
        if(SharedInfo.Notice.mNewNoticeCount == 0) {
            mText_NoticeCount.setVisibility(View.GONE);
        }
        else mText_NoticeCount.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.Button_Notice)
    void goNotice(View v) {
        Intent it = new Intent(this, Notice___Activity.class);
        startActivity(it);
    }


    /* ==================================================================================================================== */
    // 쿠폰
    /* ==================================================================================================================== */

    @BindView(R.id.Button_Coupon) MaterialRippleLayout mButton_Coupon;
    @BindView(R.id.Text_CouponCount) TextView mText_CouponCount;

    void initCoupon() {
        mText_CouponCount.setText("" + SharedInfo.Coupon.mCouponCnt);
        if(SharedInfo.Coupon.mCouponCnt == 0) {
            mText_CouponCount.setVisibility(View.GONE);
        }
        else mText_CouponCount.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.Button_Coupon)
    void goCoupon(View v) {
        Intent it = new Intent(this, Coupon___Activity.class);
        startActivity(it);
    }


    /* ==================================================================================================================== */
    // 설정
    /* ==================================================================================================================== */

    @BindView(R.id.Button_Alarm) MaterialRippleLayout mButton_Alarm;

    void initSetup() {

    }

    @OnClick(R.id.Button_Alarm)
    void goAlarm(View v) {
        Intent it = new Intent(this, Setup___Activity.class);
        startActivity(it);
    }

    /* ############################################################################################# */
    // 고객지원센터

    @BindView(R.id.Text_SupportEmail) TextView mText_SupportEmail;
    @BindView(R.id.Text_SupportPhone) TextView mText_SupportPhone;

    void initSupport() {
        mText_SupportEmail.setText(SharedInfo.Support.mEmail);
        mText_SupportPhone.setText(SharedInfo.Support.mPhone);
    }


    /* ############################################################################################# */
    // 규약

    @BindView(R.id.Button_TermOfService) TextView mButton_TermOfServer;
    @BindView(R.id.Button_Privacy) TextView mButton_Privacy;

    void initRule() {
        mButton_TermOfServer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(SideMenu___Activity.this, Web___Activity.class);
                it.putExtra(Param.TITLE, "이용약관");
                it.putExtra(Param.URL, ConstDef.BASE_API_URL + "/resources/html/term_of_service_maienglish.htm");
                startActivity(it);
            }
        });

        mButton_Privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(SideMenu___Activity.this, Web___Activity.class);
                it.putExtra(Param.TITLE, "개인정보처리방침");
                it.putExtra(Param.URL, ConstDef.BASE_API_URL + "/resources/html/privacy_policy_maienglish.htm");
                startActivity(it);
            }
        });
    }



    /* ############################################################################################# */
    // API

    UserAPI mAPI_User;

    void initAPI() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConstDef.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mAPI_User = retrofit.create(UserAPI.class);
    }


    /* ============================================================================================================================== */
    // 로그인 상태
    /* ============================================================================================================================== */

    @BindView(R.id.Button_Login) RippleLayout mButton_Login;
    @BindView(R.id.Image_User) ImageView mImage_User;
    @BindView(R.id.Text_Name) TextView mText_Name;
    @BindView(R.id.Text_Email) TextView mText_Email;

    Realm_User mUser;

    void initLogin() {
        mUser = UserFactory.get(realm);

        /* 사용자 정보 설정 */
        if(SharedInfo.User.isLogined()) {
            Picasso.with(this)
                    .load(SharedInfo.User.mPhotoUrl)
                    .placeholder(R.mipmap.male)
                    .into(mImage_User);

            mText_Name.setText(SharedInfo.User.mName);
            mText_Email.setText(SharedInfo.User.mEmail);

            mButton_Login.setVisibility(View.GONE);
        }
        else {
            Picasso.with(this)
                    .load(R.mipmap.male)
                    .into(mImage_User);
            mText_Name.setText("Guest");
            mText_Email.setText(getString(R.string.welcome));

            mButton_Login.setVisibility(View.VISIBLE);
        }

        mButton_Login.setOnRippleCompleteListener(mOnClick_Login);
    }

    RippleLayout.OnRippleCompleteListener mOnClick_Login = new RippleLayout.OnRippleCompleteListener() {
        @Override
        public void onComplete(RippleLayout rippleView) {
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, RC___GOOGLE_LOGIN);
        }
    };


    /* ============================================================================================================================== */
    // 구글 로그인
    /* ============================================================================================================================== */

    private FirebaseAuth mAuth;
    private GoogleApiClient mGoogleApiClient;
    GoogleSignInClient mGoogleSignInClient;

    /* 구글 API 초기화 */
    void initGoogleLogin() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestIdToken(String.valueOf(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mAuth = FirebaseAuth.getInstance();
    }


    /* 구글로부터 결과 수신 처리*/
    void handleGoogleLogin(Task<GoogleSignInAccount> completedTask) {

        GoogleSignInAccount account = null;
        try {
            account = completedTask.getResult(ApiException.class);

            /* 구글에서 수신한 정보 등록 */
            SharedInfo.User.mName = account.getDisplayName();
            SharedInfo.User.mEmail = account.getEmail();
            if(account.getPhotoUrl() != null) SharedInfo.User.mPhotoUrl = account.getPhotoUrl().toString();

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "구글 로그인이 되지 않습니다.", Toast.LENGTH_SHORT).show();
            return;
        }

        registerUser();
    }


    /* 구글 로그인 후에 서버 등록 */
    private void registerUser() {
        /* 서버에 정보 저장 */
        mAPI_User.registerUser(SharedInfo.User.mEmail).enqueue(new Callback<RestApiVO>() {
            @Override
            public void onResponse(@NonNull Call<RestApiVO> call, @NonNull Response<RestApiVO> response) {
                RestApiVO body = response.body();
                if (body != null && body.getRes_code() == ResCode.SUCC) {
                    /* 인증 정보 등록 */
                    UserFactory.login(realm, SharedInfo.User.mEmail, SharedInfo.User.mName, SharedInfo.User.mPhotoUrl, body.getAuth_key());
                    SharedInfo.User.mAuthKey = body.getAuth_key();
                    SharedInfo.User.mCreateDate = body.getDatetime();

                    SharedInfo.Coupon.mFreeCoupon = body.getCoupon();
                    Worker_FreeCoupon.build();

                    Toast.makeText(SideMenu___Activity.this, "로그인 되셨습니다.", Toast.LENGTH_SHORT).show();
                } else {
                    SharedInfo.User.reset();
                    Toast.makeText(SideMenu___Activity.this, "(1) 서버와 연결이 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
                }
                initUI();
            }

            @Override
            public void onFailure(@NonNull Call<RestApiVO> call, @NonNull Throwable t) {
                SharedInfo.User.reset();
                Toast.makeText(SideMenu___Activity.this, "(2) 서버와 연결이 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
                initUI();
            }
        });
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(SideMenu___Activity.this, "구글 로그인 연결 실패", Toast.LENGTH_SHORT).show();
    }
}
