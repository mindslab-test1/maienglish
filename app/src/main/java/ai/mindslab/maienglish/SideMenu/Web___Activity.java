package ai.mindslab.maienglish.SideMenu;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ai.mindslab.maienglish.Common.ConstDef;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.ExternLib.RippleLayout;
import ai.mindslab.maienglish.Model.RestApiVO;
import ai.mindslab.maienglish.Notice.NoticeItem;
import ai.mindslab.maienglish.Notice.Notice_Adapter;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.RestAPI.NoticeAPI;
import ai.mindslab.maienglish.RestAPI.Param;
import ai.mindslab.maienglish.RestAPI.ResCode;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Web___Activity extends AppCompatActivity {
    String mTitle;
    String mUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web___activity);
        ButterKnife.bind(this);
        Utils.setColor_StatusBarIcon(this, true, this.getResources().getColor(R.color.sys_bg___subscribe));

        mTitle = (String) getIntent().getStringExtra(Param.TITLE);
        mUrl = (String) getIntent().getStringExtra(Param.URL);

        initTopLayer();
        initWeb();
    }

    /* ============================================================================================================================ */
    // TOP Layer
    /* ============================================================================================================================ */

    @BindView(R.id.Text_TopTitle) TextView mText_Title;
    @BindView(R.id.Button_Back) RippleLayout mButton_Back;

    void initTopLayer() {
        mText_Title.setText(mTitle);

        mButton_Back.setOnRippleCompleteListener(mOnClick_Back);
    }

    RippleLayout.OnRippleCompleteListener mOnClick_Back = new RippleLayout.OnRippleCompleteListener() {
        @Override
        public void onComplete(RippleLayout rippleView) {
            finish();
        }
    };


    /* ============================================================================================================================ */
    // RecyclerView
    /* ============================================================================================================================ */

    @BindView(R.id.WebView) WebView mWebView;

    void initWeb() {
        mWebView.loadUrl(mUrl);
    }

}
