package ai.mindslab.maienglish.Common;

public class ConstDef {
    /* APP VERSION */
    public static class AppVersion {
        public static final int MAJOR                   = 1;
        public static final int MINOR                   = 4;    // MINOR 이상 다를 경우, 앱 무조건 업데이트 수행.
        public static final int PATCH                   = 1;
    }

    static public final int USER_ROLE___QUESTION          = 0;
    static public final int USER_ROLE___ANSWER            = 1;


    static public final String STT_RES_CODE___SUCCESS     = "200";
    static public final String STT_RES_CODE___PROCESSING  = "102";

//    static public final String BASE_API_URL = "http://10.122.66.93:9095";
//    static public final String BASE_API_URL = "http://10.122.64.71:7002";
//    static public final String BASE_IMAGE_URL = "http://10.122.64.71:7002";
//    static public final String BASE_AUDIO_URL = "http://10.122.64.71:7002";
//    static public final String BASE_MAI_AUDIO_URL = "http://10.122.64.71:7002";
//    static public final String BASE_WSS_URL = "wss://10.122.64.71:7001/uapi/stt/websocket/evaluationStt";
//    static public final String BASE_STT_AUDIO_URL = "http://10.122.64.71:7002";

    /* 개발 */
//    static public final String BASE_API_URL = "http://10.122.64.71:7002";
//    static public final String BASE_IMAGE_URL = "http://10.122.64.71:7002";
//    static public final String BASE_AUDIO_URL = "http://10.122.64.71:7002";
//    static public final String BASE_MAI_AUDIO_URL = "http://10.122.64.71:7002";
//    static public final String BASE_WSS_URL = "wss://10.122.64.71:7001/uapi/stt/websocket/evaluationStt";
//    static public final String BASE_STT_AUDIO_URL = "http://10.122.64.71:7002";

    /* 상용 */
    static public final String BASE_API_URL = "http://api.maienglish.ai:7002";
    static public final String BASE_IMAGE_URL = "http://api.maienglish.ai:7002";
    static public final String BASE_MAI_AUDIO_URL = "http://api.maienglish.ai:7002";
    static public final String BASE_WSS_URL = "wss://stt.maienglish.ai:7001/uapi/stt/websocket/evaluationStt";
    static public final String BASE_STT_AUDIO_URL = "http://stt.maienglish.ai:7002";

    static public final String BILLING_LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqR2j4dkFKzLaCaBc+90LXzGbe5OaRnzNwrQdfNEJydjnYy49kiaT/WROkmruTp70yy7L79dMBIMrqEDkuBVnntRgoAGc+3C3iTKM5FdJP78AYfNW5vI0y4BHJLMXRy4HMLnn2DH4jOsrrvDzBrI6aU1KkOMuR3Ia0xNQGm2rjOEjzcPXCCn/JR5RdP9ps5hoDh+TA+Gcn1hDOh75mb8FdeM106UgJe+tmzSnQbK/E/YoiEsmjfrHbX7cJCuBBkwdYm/fkj3w/K6AGaCn3+RuB4Ge05HM2L9t4RU5FaA24/hYbiVqz7dwbwLtiUxKgU4PfIbth+h3JsWoOlcpndmQSwIDAQAB";
    static public final String SUBS_CODE = "maieng_3900";

    static public final String SPREF_NAME = "maienglish";
    static public final String SPREF_PARAM___LAST_NOTICE_DATE = "last_notice_date";

    static public final int HINT_FONT_SIZE = 12; // dp

    /* COUPON */
    static public final int COUPON_NUM_LENGTH = 16;
    static public final int COUPON_ID___FREE_EXPERIENCE_EVENT = 1;
    static public final int COUPON_ID___FREE_7 = 2;

    /* SHARED PREFERENCE NAME */
    static public final String SP_SETUP = "setup";
}

