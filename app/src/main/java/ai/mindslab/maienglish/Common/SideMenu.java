package ai.mindslab.maienglish.Common;

import android.app.Activity;
import android.content.Intent;

import ai.mindslab.maienglish.SideMenu.SideMenu___Activity;

public class SideMenu {
    static public void open(Activity act) {
        Intent it = new Intent(act, SideMenu___Activity.class);
        act.startActivity(it);
    }
}
