package ai.mindslab.maienglish.Common;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

public class Utils {

    /* ========================================================================================================== */
    /* 상단 시스템 상태바의 배경 색상 설정 */
    public static void setColor_StatusBarIcon(Activity act, boolean b_light, int color) {
        View view = act.getWindow().getDecorView();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (view != null) {
                // 23 버전 이상일 때 상태바 하얀 색상에 회색 아이콘 색상을 설정
                if(b_light) view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                act.getWindow().setStatusBarColor(color);
            }
        }else if (Build.VERSION.SDK_INT >= 21) {
            // 21 버전 이상일 때
            act.getWindow().setStatusBarColor(Color.BLACK);

        }
    }

    /* ========================================================================================================== */
    /* 단말 화면 크기 조회 */
    public static int getScreenWidth(Activity act) {
        return act.getApplicationContext().getResources().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight(Activity act) {
        return act.getApplicationContext().getResources().getDisplayMetrics().heightPixels;
    }

    /* ========================================================================================================== */
    /* 디자인의 크기를 현재 단말의 크기로 변환 */

    final static int WIDTH_DESIGN =             1080;
    final static int HEIGHT_DESIGN =            1920;

    public static int convSizeByWidth(Activity act, int width) {
        return width*getScreenWidth(act) / WIDTH_DESIGN;
    }

    public static int convSizeByHeight(Activity act, int height) {
        return height*getScreenWidth(act) / HEIGHT_DESIGN;
    }

    public static int convSizeByRatio(Activity act, int size, float ratio) {
        return (int) (size*ratio);
    }

    /* ========================================================================================================== */
    /* 수를 천단위로 ,를 삽입한다. */

    public static String addCommaInNumber(int number) {
        DecimalFormat fmt = new DecimalFormat("###,###");
        return fmt.format(number);
    }

    /* ========================================================================================================== */
    // 수를 천단위로 ,를 삽입한다.

    public static int dp2px(Context ctx, float dp) {
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, ctx.getResources().getDisplayMetrics());
        return px;
    }

    /* ========================================================================================================== */
    // JSON UTIL

    public static Map<String, Object> jsonString2Map(String jsonString) {
        HashMap<String, Object> map = null;

        try {
            map = new Gson().fromJson(jsonString, new TypeToken<HashMap<String, Object>>() {}.getType());
        } catch (Exception e) {
            Log.e("AAA", e.toString());
            Log.e("AAA", e.getMessage());
            e.printStackTrace();
        }

        return map;
    }

    /* ========================================================================================================== */
    // MAP TO VO

    public static Object map2vo(Map map, Object objClass) {
        String keyAttribute = null;
        String setMethodString = "set";
        String methodString = null;

        Iterator itr = map.keySet().iterator();
        while(itr.hasNext()) {
            keyAttribute = (String) itr.next();
            methodString = setMethodString+keyAttribute.substring(0,1).toUpperCase()+keyAttribute.substring(1);
            try {
                Method[] methods = objClass.getClass().getDeclaredMethods();
                for(int i = 0; i<=methods.length-1; i++) {
                    if(methodString.equals(methods[i].getName())) {
                        System.out.println("invoke : "+methodString);
                        methods[i].invoke(objClass, map.get(keyAttribute));
                    }
                }
            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }

        return objClass;
    }

    /* ========================================================================================================== */
    // 현재 날짜 시간을 문자열로

    public static String getCurrentDateTime() {
        SimpleDateFormat fmt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date time = new Date();
        return fmt1.format(time);
    }

    /* ========================================================================================================== */
    //  구독 / 무료 쿠폰
    /* ========================================================================================================== */

    public static boolean isAllowedAllContent() {
        if(SharedInfo.Subscription.isSubscribed()) return true;
        else if(SharedInfo.Coupon.mFreeCoupon != null) return true;

        return false;
    }

    public static int getStateAllowedAllContent() {
        if(SharedInfo.Subscription.isSubscribed()) return 1;
        else if(SharedInfo.Coupon.mFreeCoupon != null) return 1;

        return 0;
    }


    /* ========================================================================================================== */
    // 시스템 언어
    /* ========================================================================================================== */
    static public String getSystemLanguage(Context context) {
        Locale locale = context.getResources().getConfiguration().locale;
        String language = locale.getLanguage();
        return language;
    }
}
