package ai.mindslab.maienglish.Common;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import ai.mindslab.maienglish.R;
import butterknife.BindView;

public class MaiProgress {
    private Context mContext;
    private Dialog mDialog;
    private AVLoadingIndicatorView mProgress;

    public static MaiProgress build(Context context) {
        MaiProgress maiDialog = new MaiProgress();
        maiDialog.mContext = context;

        maiDialog.mDialog = new Dialog(maiDialog.mContext);
        maiDialog.mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        maiDialog.mDialog.setContentView(R.layout.mai_progress);

        maiDialog.mDialog.setCancelable(false);
//        maiDialog.mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(context.getResources().getColor(R.color.dialog___outside___bg)));
        maiDialog.mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        maiDialog.mProgress = (AVLoadingIndicatorView) maiDialog.mDialog.findViewById(R.id.Progress);
        maiDialog.mProgress.hide();
        return maiDialog;
    }


    public MaiProgress show() {
        mProgress.smoothToShow();
        mDialog.show();
        return this;
    }

    public MaiProgress hide() {
        mProgress.smoothToHide();
        mDialog.hide();
        return this;
    }

    public MaiProgress dismiss() {
        mProgress.smoothToHide();
        mDialog.dismiss();
        return this;
    }

}
