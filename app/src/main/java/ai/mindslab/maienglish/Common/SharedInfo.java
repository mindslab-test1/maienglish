package ai.mindslab.maienglish.Common;

import android.view.View;

import androidx.work.PeriodicWorkRequest;

import java.util.ArrayList;
import java.util.List;

import ai.mindslab.maienglish.Model.AdvertisementVO;
import ai.mindslab.maienglish.Model.BookVO;
import ai.mindslab.maienglish.Model.CouponVO;
import ai.mindslab.maienglish.Model.PatternContentVO;
import ai.mindslab.maienglish.Model.PatternVO;
import ai.mindslab.maienglish.Model.StudyHistoryStatVO;
import ai.mindslab.maienglish.Model.UnitVO;
import ai.mindslab.maienglish.SchedulerTask.Worker_FreeCoupon;

public class SharedInfo {

    /* 공지 정보 */
    static public class Notice {
        public static int mNewNoticeCount = 0;
    }

    /* 광고 이미지 */
    static public List<AdvertisementVO> mList_Ad = null;

    /* 로그인 후에, 업데이트 됨 */
    static public class User {
        public static boolean mFirstFlag = true;
        public static String mName = "Guest";
        public static String mNickname = null;
        public static String mPhotoUrl = null;
        public static String mEmail = null;
        public static String mAuthKey = "";
        public static String mCreateDate = "2019년 05월 02일 가입";
        public static int mFreeExpSurvey = 0; // 무료 체험의 설문지 작성 대상.

        public static boolean isLogined() { return (mAuthKey == null || mAuthKey.length() == 0 ? false : true); }

        public static void reset() {
            mName = "Guest";
            mNickname = null;
            mPhotoUrl = null;
            mEmail = null;
            mAuthKey = "";
        }
    }

    /* 타임 관련 설정 */
    static public class Time {
        public static int mMicTimeout = 10000;             // 서버로부터 설정 정보 수신 (msec)
    }

    /* 연결 정보 */
    static public class Connect {
//        public static String mAudioIpPort = "10.122.64.58:8088";
        public static String mServerIpPort = "10.122.64.58:8080";
        public static String mSttMediaHost = "https://10.122.64.58:8080";
//        public static String mSttUrl = "wss://10.122.64.58:8080/uapi/stt/websocket/evaluationStt";
//        public static String mSttUrl = "wss://10.122.64.71:7001/uapi/stt/websocket/evaluationStt";

    }

    /* 구독 정보 */
    static public class Subscription {
        public static int mPrice = 0;                   // 월 정기 구독료.

        public static int mSubsFlag = 0;                // 구독 여부
        public static boolean isSubscribed() { return (User.isLogined() && mSubsFlag == 1 ? true : false); }
        public static int getSubsStatus() { return (isSubscribed() ? 1 : 0);}

        public static void reset() {
            mSubsFlag = 0;
        }
    }

    /* 학습 데이타 */
    static public class Study {
        public static BookVO mBook = null;
        public static PatternVO mPattern = null;
        public static List<PatternContentVO> mList_PatternContent = new ArrayList<PatternContentVO>();

        public static StudyHistoryStatVO mStat = null;
    }

    /* 마이톡 */
    static public class mAITalk {
        public static int mRetryCnt = 1;
    }

    /* 무료 정보 */
    static public class Free {
        public static int mFreeBookCnt = 1;
        public static int mFreePatternCnt = 2;
    }

    /* 쿠폰 정보 */
    static public class Coupon {
        public static int mCouponCnt = 0;

        public static Object mLock = new Object();
        public static CouponVO mFreeCoupon = null;
        public static void setFreeCoupon(CouponVO coupon) { synchronized (mLock) { mFreeCoupon = coupon; } }
        public static CouponVO getFreeCoupon() { synchronized (mLock) { return mFreeCoupon; } }

        public static String mSurveyUrl = "";
    }


    /* 고객 센터 정보 */
    static public class Support {
        public static String mEmail = "";
        public static String mPhone = "";
    }


    /* 백그라운 작업 */
    static public class Workers {
        public static PeriodicWorkRequest mWork_FreeCoupon = null;
    }
}
