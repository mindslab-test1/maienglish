package ai.mindslab.maienglish.Common;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.squareup.picasso.Picasso;

import ai.mindslab.maienglish.R;
import butterknife.BindView;

public class MaiDialog {
    private Context mContext;
    private Dialog mDialog;
    private boolean mOkFlag = false;
    private boolean mCancelFlag = false;

    @BindView(R.id.Image) ImageView mImage;
    @BindView(R.id.Text_Message) TextView mText_Message;
    @BindView(R.id.Button_Ok) MaterialRippleLayout mButton_Ok;
    @BindView(R.id.Text_Ok) TextView mText_Ok;
    @BindView(R.id.Button_Cancel) MaterialRippleLayout mButton_Cancel;
    @BindView(R.id.Text_Cancel) TextView mText_Cancel;
    @BindView(R.id.Button_Common) MaterialRippleLayout mButton_Common;
    @BindView(R.id.Text_Common) TextView mText_Common;

    public static MaiDialog build(Context context) {
        MaiDialog maiDialog = new MaiDialog();
        maiDialog.mContext = context;

        maiDialog.mDialog = new Dialog(maiDialog.mContext);
        maiDialog.mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        maiDialog.mDialog.setContentView(R.layout.mai_dialog);
//        maiDialog.mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(context.getResources().getColor(R.color.dialog___outside___bg)));
        maiDialog.mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        maiDialog.mImage = (ImageView) maiDialog.mDialog.findViewById(R.id.Image);
        maiDialog.mText_Message = (TextView) maiDialog.mDialog.findViewById(R.id.Text_Message);
        maiDialog.mButton_Ok = (MaterialRippleLayout) maiDialog.mDialog.findViewById(R.id.Button_Ok);
        maiDialog.mText_Ok = (TextView) maiDialog.mDialog.findViewById(R.id.Text_Ok);
        maiDialog.mButton_Cancel = (MaterialRippleLayout) maiDialog.mDialog.findViewById(R.id.Button_Cancel);
        maiDialog.mText_Cancel = (TextView) maiDialog.mDialog.findViewById(R.id.Text_Cancel);
        maiDialog.mButton_Common = (MaterialRippleLayout) maiDialog.mDialog.findViewById(R.id.Button_Common);
        maiDialog.mText_Common = (TextView) maiDialog.mDialog.findViewById(R.id.Text_Common);

        maiDialog.mButton_Ok.setOnClickListener(maiDialog.mOnClick___DefaultOk);
        maiDialog.mButton_Cancel.setOnClickListener(maiDialog.mOnClick___DefaultCancel);
        maiDialog.mButton_Common.setOnClickListener(maiDialog.mOnClick___DefaultCommon);

        maiDialog.mDialog.setCancelable(false);
        return maiDialog;
    }

    public MaiDialog setMessage(String title) {
        mText_Message.setText(title);
        return this;
    }

    public MaiDialog setImage(int rscId) {
        Picasso.with(mContext).load(rscId).into(mImage);
        return this;
    }

    public MaiDialog setOk(String title, View.OnClickListener listener) {
        mText_Ok.setText(title);
        mOnClick_Ok = listener;

        mText_Common.setText(title);
        mOnClick_Common = listener;

        mOkFlag = true;
        return this;
    }

    public MaiDialog setCancel(String title, View.OnClickListener listener) {
        mText_Cancel.setText(title);
        mOnClick_Cancel = listener;

        mText_Common.setText(title);
        mOnClick_Common = listener;

        mCancelFlag = true;
        return this;
    }

    public MaiDialog show() {
        if(mOkFlag == false || mCancelFlag == false) {
            mButton_Ok.setVisibility(View.GONE);
            mButton_Cancel.setVisibility(View.GONE);
            mButton_Common.setVisibility(View.VISIBLE);
        }
        else {
            mButton_Ok.setVisibility(View.VISIBLE);
            mButton_Cancel.setVisibility(View.VISIBLE);
            mButton_Common.setVisibility(View.GONE);
        }

        mDialog.show();
        return this;
    }

    public MaiDialog hide() {
        mDialog.hide();
        return this;
    }

    public MaiDialog dismiss() {
        mDialog.dismiss();
        return this;
    }

    /* ############################################################################################ */
    // CALLBACK

    View.OnClickListener mOnClick___DefaultOk = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(mOnClick_Ok != null) mOnClick_Ok.onClick(v);
            mDialog.dismiss();
        }
    };
    View.OnClickListener mOnClick___DefaultCancel = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(mOnClick_Cancel != null) mOnClick_Cancel.onClick(v);
            mDialog.dismiss();
        }
    };
    View.OnClickListener mOnClick___DefaultCommon = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(mOnClick_Common != null) mOnClick_Common.onClick(v);
            mDialog.dismiss();
        }
    };

    View.OnClickListener mOnClick_Ok = null;
    View.OnClickListener mOnClick_Cancel = null;
    View.OnClickListener mOnClick_Common = null;
}
