package ai.mindslab.maienglish.StudyHistory;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ai.mindslab.maienglish.Common.ConstDef;
import ai.mindslab.maienglish.Common.MaiDialog;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.ExternLib.RippleLayout;
import ai.mindslab.maienglish.Model.RestApiVO;
import ai.mindslab.maienglish.Model.StudyHistoryStatVO;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.Realm.UserFactory;
import ai.mindslab.maienglish.RestAPI.CommonAPI;
import ai.mindslab.maienglish.RestAPI.ResCode;
import ai.mindslab.maienglish.RestAPI.StudyAPI;
import ai.mindslab.maienglish.RestAPI.UserAPI;
import ai.mindslab.maienglish.Study.StudyChallenge___Fragment;
import ai.mindslab.maienglish.Study.StudyPractice___Fragment;
import ai.mindslab.maienglish.Study.StudyRepeat___Fragment;
import ai.mindslab.maienglish.Study.Study___Fragment;
import ai.mindslab.maienglish.Study.ViewPager_NoSwiping;
import ai.mindslab.maienglish.Unit.Units___Apdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class StudyHistory___Activity extends AppCompatActivity {

    private Realm realm;
    StudyHistoryStatVO mStat = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.study_history___activity);
        ButterKnife.bind(this);
        Utils.setColor_StatusBarIcon(this, true, this.getResources().getColor(R.color.sys_bg___books));

        Realm.init(this);
        realm = Realm.getDefaultInstance();

        initAPI();
        initTopLayer();
        initViewPager();

        loadStat();
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        SharedInfo.Study.mStat = null;
    }

    /* ============================================================================================================================== */
    // TOP LAYER
    /* ============================================================================================================================== */

    @BindView(R.id.Text_TopTitle) TextView mText_Title;
    @BindView(R.id.Button_Back) RippleLayout mButton_Back;

    void initTopLayer() {
        mText_Title.setText(getString(R.string.study_history___title));

        mButton_Back.setOnRippleCompleteListener(new RippleLayout.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleLayout rippleView) {
                onBackPressed();
            }
        });
    }



    /* ###################################################################################################### */

    public static final int PAGE___PROGRESS = 0;
    public static final int PAGE___SCORE = 1;

    @BindView(R.id.Tabs) SmartTabLayout mTabs;
    @BindView(R.id.ViewPager) ViewPager mViewPager;

    FragmentPagerItemAdapter mAdapter;

    void initViewPager() {
        Study___Fragment.mContext = this;
        mAdapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add(getString(R.string.study_history___progress_title), StudyHistory___Progress___Fragment.class)
                .add(getString(R.string.study_history___score_title), StudyHistory___Score___Fragment.class)
                .create());
        mViewPager.setAdapter(mAdapter);
        mTabs.setViewPager(mViewPager);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int page_no) {
                Log.e("AAA", "onPageSelected............ " + page_no);
                TextView textView = (TextView) mTabs.getTabAt(page_no);
                textView.setTextColor(getResources().getColor(R.color.study___tab_text___active));

                textView = (TextView) mTabs.getTabAt((page_no + 1)%2);
                textView.setTextColor(getResources().getColor(R.color.study___tab_text___normal));
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        Typeface typeface = ResourcesCompat.getFont(this, R.font.noto_medium);
        TextView textView = (TextView) mTabs.getTabAt(PAGE___PROGRESS);
        textView.setTypeface(typeface);
        textView.setIncludeFontPadding(false);
        textView.setTextColor(getResources().getColor(R.color.study___tab_text___active));

        textView = (TextView) mTabs.getTabAt(PAGE___SCORE);
        textView.setTypeface(typeface);
        textView.setIncludeFontPadding(false);
        textView.setTextColor(getResources().getColor(R.color.study___tab_text___normal));

        mTabs.setVisibility(View.VISIBLE);
    }


    /* ============================================================================================================= */
    // API
    /* ============================================================================================================= */

    CommonAPI mAPI_Common;
    UserAPI mAPI_User;
    StudyAPI mAPI_Study;

    void initAPI() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConstDef.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mAPI_Common = retrofit.create(CommonAPI.class);
        mAPI_User = retrofit.create(UserAPI.class);
        mAPI_Study = retrofit.create(StudyAPI.class);
    }

    void loadStat() {
        mAPI_Study.getStudyHistoryStat(SharedInfo.User.mAuthKey).enqueue(new Callback<RestApiVO>() {
            @Override
            public void onResponse(@NonNull Call<RestApiVO> call, @NonNull Response<RestApiVO> response) {
                if(response.code() == 401) {
                    Toast.makeText(StudyHistory___Activity.this, "인증이 되지 않았습니다. ", Toast.LENGTH_SHORT).show();
//                    if(mProgress != null) mProgress.hide();
                    return;
                }
                RestApiVO body = response.body();
                if (body != null && body.getRes_code() == ResCode.SUCC) {
                    buildStat(body.getStudy_history_stat());
                } else {
                    Toast.makeText(StudyHistory___Activity.this, "네트워크가 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
//                    if(mProgress != null) mProgress.hide();
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestApiVO> call, @NonNull Throwable t) {
                Log.e("AAA", "API.getStudyHistoryStat() >> onFailure() >> " + t.toString());
                Toast.makeText(StudyHistory___Activity.this, "네트워크가 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
//                if(mProgress != null) mProgress.hide();
            }
        });
    }


    /* ============================================================================================================= */
    // 데이타
    /* ============================================================================================================= */

    void buildStat(StudyHistoryStatVO stat) {
        StudyHistory___Progress___Fragment fragProgress = (StudyHistory___Progress___Fragment) mAdapter.getPage(0);
        fragProgress.buildData(stat);

        StudyHistory___Score___Fragment fragScore = (StudyHistory___Score___Fragment) mAdapter.getPage(1);
        fragScore.buildData(stat);

        mStat = stat;
        SharedInfo.Study.mStat = stat;
    }
}
