package ai.mindslab.maienglish.StudyHistory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import ai.mindslab.maienglish.Common.ConstDef;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.ExternLib.RippleLayout;
import ai.mindslab.maienglish.Model.PatternVO;
import ai.mindslab.maienglish.Model.RestApiVO;
import ai.mindslab.maienglish.Model.StudyHistoryContentVO;
import ai.mindslab.maienglish.Model.StudyHistoryStatVO;
import ai.mindslab.maienglish.Model.StudyResultVO;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.RestAPI.CommonAPI;
import ai.mindslab.maienglish.RestAPI.ResCode;
import ai.mindslab.maienglish.RestAPI.StudyAPI;
import ai.mindslab.maienglish.RestAPI.UserAPI;
import ai.mindslab.maienglish.Study.Study___Activity;
import ai.mindslab.maienglish.Study.Study___Fragment;
import ai.mindslab.maienglish.Unit.Unit___Activity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import lombok.Data;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class StudyHistory_Content___Activity extends AppCompatActivity {

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.study_history___content___activity);
        ButterKnife.bind(this);
        Utils.setColor_StatusBarIcon(this, true, this.getResources().getColor(R.color.sys_bg___books));

        Realm.init(this);
        realm = Realm.getDefaultInstance();

        Intent it = getIntent();
        String date = it.getStringExtra("date");

        initAPI();
        initTopLayer();
        initRecyclerView(date);
    }


    /* ============================================================================================================================== */
    // TOP LAYER
    /* ============================================================================================================================== */

    @BindView(R.id.Text_TopTitle) TextView mText_Title;
    @BindView(R.id.Button_Back) RippleLayout mButton_Back;

    void initTopLayer() {
        mText_Title.setText(getString(R.string.study_history___content___title));

        mButton_Back.setOnRippleCompleteListener(new RippleLayout.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleLayout rippleView) {
                onBackPressed();
            }
        });
    }



    /* ======================================================================================================================= */
    // API
    /* ======================================================================================================================= */

    CommonAPI mAPI_Common;
    UserAPI mAPI_User;
    StudyAPI mAPI_Study;

    void initAPI() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConstDef.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mAPI_Common = retrofit.create(CommonAPI.class);
        mAPI_User = retrofit.create(UserAPI.class);
        mAPI_Study = retrofit.create(StudyAPI.class);
    }

    void loadContent(final int position) {
        mList.get(position).loading_flag = true;
        mAPI_Study.getStudyHistoryContent(SharedInfo.User.mAuthKey, mList.get(position).getDate()).enqueue(new Callback<RestApiVO>() {
            @Override
            public void onResponse(@NonNull Call<RestApiVO> call, @NonNull Response<RestApiVO> response) {
                if(response.code() == 401) {
                    Toast.makeText(StudyHistory_Content___Activity.this, "인증이 되지 않았습니다. ", Toast.LENGTH_SHORT).show();
//                    if(mProgress != null) mProgress.hide();

                    mList.get(position).loading_flag = false;
                    return;
                }
                RestApiVO body = response.body();
                if (body != null && body.getRes_code() == ResCode.SUCC) {
//                    buildStat(body.getStudy_history_stat());

                    mList.get(position).setList(body.getStudy_history_list());
                } else {
                    Toast.makeText(StudyHistory_Content___Activity.this, "네트워크가 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
//                    if(mProgress != null) mProgress.hide();

                    mList.get(position).loading_flag = false;
                }

                mAdapter.notifyItemChanged(position);
            }

            @Override
            public void onFailure(@NonNull Call<RestApiVO> call, @NonNull Throwable t) {
                Log.e("AAA", "API.getStudyHistoryStat() >> onFailure() >> " + t.toString());
                Toast.makeText(StudyHistory_Content___Activity.this, "네트워크가 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
//                if(mProgress != null) mProgress.hide();
                mList.get(position).loading_flag = false;
                mAdapter.notifyItemChanged(position);
            }
        });
    }

    /* ======================================================================================================================= */
    // RECYCLER VIEW
    /* ======================================================================================================================= */

    @BindView(R.id.RecyclerView) RecyclerView mRecyclerView;
    Adapter_StudyHistory mAdapter;
    List<StudyHistoryContent> mList = new ArrayList<StudyHistoryContent>();


    void initRecyclerView(String date) {

        for(int xx = 0; xx < SharedInfo.Study.mStat.getDaily_list().size(); xx++) {
            StudyHistoryContent item = new StudyHistoryContent();
            item.date = SharedInfo.Study.mStat.getDaily_list().get(xx).getDate_str();
            item.setList(null);
            mList.add(item);

            if(date.equals(item.date)) item.setOpen_flag(true);
        }

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this)) ;
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getApplicationContext(),new LinearLayoutManager(this).getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.recycler_divider));
        mRecyclerView.addItemDecoration(dividerItemDecoration);

        mAdapter = new Adapter_StudyHistory() ;
        mRecyclerView.setAdapter(mAdapter) ;
        mAdapter.notifyDataSetChanged();

    }

    /* ======================================================================================================================= */
    // ADAPTER FOR RECYCLER VIEW
    /* ======================================================================================================================= */

    public class Adapter_StudyHistory extends RecyclerView.Adapter<Adapter_StudyHistory.ViewHolder> {

        // 생성자에서 데이터 리스트 객체를 전달받음.
        Adapter_StudyHistory() {
        }

        // onCreateViewHolder() - 아이템 뷰를 위한 뷰홀더 객체 생성하여 리턴.
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            Context context = parent.getContext() ;
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) ;

            View view = inflater.inflate(R.layout.study_history___content___row_layer, parent, false) ;
            ViewHolder holder = new ViewHolder(view) ;

            return holder ;
        }

        // onBindViewHolder() - position에 해당하는 데이터를 뷰홀더의 아이템뷰에 표시.
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.mLayer_Title.setTag(R.string.tag___index, position);
            holder.mText_Date.setText(mList.get( position).date);

            /* 열려 있을 경우 */
            if(mList.get(position).open_flag) {
                holder.mImage_Arrow.setImageResource(R.mipmap.arrow___to_up);

                if(mList.get(position).getList() == null) {
                    holder.mLayer_Progress.setVisibility(View.VISIBLE);
                    holder.mProgress.show();
                }
                else {
                    holder.mLayer_Progress.setVisibility(View.GONE);
                    holder.mProgress.hide();

                    holder.mLayer_Item.setVisibility(View.VISIBLE);
                }
            }
            /* 닫혀 있을 경우 */
            else {
                holder.mImage_Arrow.setImageResource(R.mipmap.arrow___to_down);

                holder.mLayer_Progress.setVisibility(View.GONE);
                holder.mProgress.hide();

                holder.mLayer_Item.setVisibility(View.GONE);
            }

            if(mList.get(position).getList() == null && mList.get(position).loading_flag == false) loadContent(position);
            if(mList.get(position).getList() != null) rebuildContent(holder.mLayer_Item, mList.get(position).getList());
        }

        @Override
        public int getItemCount() {
            return (mList == null ? 0 : mList.size()) ;
        }


        // 아이템 뷰를 저장하는 뷰홀더 클래스.
        public class ViewHolder extends RecyclerView.ViewHolder {
            LinearLayout mLayer_Title;
            ImageView mImage_Arrow;
            TextView mText_Date;

            LinearLayout mLayer_Progress;
            AVLoadingIndicatorView mProgress;

            LinearLayout mLayer_Item;



            ViewHolder(View layer) {
                super(layer) ;

                mLayer_Title = (LinearLayout) layer.findViewById(R.id.Layer_Title);
                mLayer_Title.setOnClickListener(mOnClick_Title);

                mImage_Arrow = (ImageView) layer.findViewById(R.id.Image_Arrow);
                mText_Date = (TextView) layer.findViewById(R.id.Text_Date);

                mLayer_Progress = (LinearLayout) layer.findViewById(R.id.Layer_Progress);
                mProgress = layer.findViewById(R.id.Progress);

                mLayer_Item = (LinearLayout) layer.findViewById(R.id.Layer_Item);


            }
        }
    }


    View.OnClickListener mOnClick_Title = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (int) v.getTag(R.string.tag___index);
            mList.get(position).open_flag = !mList.get(position).open_flag;
            mAdapter.notifyItemChanged(position);
        }
    };

    RippleLayout.OnRippleCompleteListener mOnClick_Study = new RippleLayout.OnRippleCompleteListener() {
        @Override
        public void onComplete(RippleLayout rippleView) {
            PatternVO pattern= new PatternVO();
            pattern.setPattern_id((int) rippleView.getTag(R.string.tag___index));
            pattern.setOrder_no((int) rippleView.getTag(R.string.tag___order_no));
            pattern.setSentence((String) rippleView.getTag(R.string.tag___sentence));

            SharedInfo.Study.mPattern = pattern;

            Intent it = new Intent(StudyHistory_Content___Activity.this, Study___Activity.class);
            startActivityForResult(it, Unit___Activity.RC___STUDY);
        }
    };


    void rebuildContent(LinearLayout layer, List<StudyHistoryContentVO> list) {
        TextView textView;
        RippleLayout button_study;

        layer.removeAllViews();

        for(int xx = 0; xx < list.size()/3; xx++) {
            LayoutInflater inflater = (LayoutInflater) getSystemService( Context.LAYOUT_INFLATER_SERVICE );
            LinearLayout linearLayout = (LinearLayout) inflater.inflate( R.layout.study_history___content___row_item, null );
            layer.addView(linearLayout);

            textView = (TextView) linearLayout.findViewById(R.id.Text_PatternNo);
            textView.setText("Pattern #" + list.get(xx*3 + 0).getPattern_id());

            textView = (TextView) linearLayout.findViewById(R.id.Text_PatternTitle);
            textView.setText("" + list.get(xx*3 + 0).getPattern_title());

            textView = (TextView) linearLayout.findViewById(R.id.Text_Result1);
            textView.setText( "1) " + list.get(xx*3 + 0).getSentence() + "" + "\n"
                            + "You said: " + list.get(xx*3 + 0).getUser_text() + "\n"
                            + "> " + StudyResultVO.convGrade(list.get(xx*3 + 0).getGrade()) + "\n"
                            + "  (Accuracy: " + list.get(xx*3 + 0).getAscore() + " / Pronunciation: " + list.get(xx*3 + 0).getPscore() + ")"
            );

            textView = (TextView) linearLayout.findViewById(R.id.Text_Result2);
            textView.setText( "1) " + list.get(xx*3 + 1).getSentence() + "" + "\n"
                    + "You said: " + list.get(xx*3 + 1).getUser_text() + "\n"
                    + "> " + StudyResultVO.convGrade(list.get(xx*3 + 1).getGrade()) + "\n"
                    + "  (Accuracy: " + list.get(xx*3 + 1).getAscore() + " / Pronunciation: " + list.get(xx*3 + 1).getPscore() + ")"
            );

            textView = (TextView) linearLayout.findViewById(R.id.Text_Result3);
            textView.setText( "3) " + list.get(xx*3 + 2).getSentence() + "" + "\n"
                    + "You said: " + list.get(xx*3 + 2).getUser_text() + "\n"
                    + "> " + StudyResultVO.convGrade(list.get(xx*3 + 2).getGrade()) + "\n"
                    + "  (Accuracy: " + list.get(xx*3 + 2).getAscore() + " / Pronunciation: " + list.get(xx*3 + 2).getPscore() + ")"
            );

            button_study = (RippleLayout) linearLayout.findViewById(R.id.Button_Study);
            button_study.setOnRippleCompleteListener(mOnClick_Study);
            button_study.setTag(R.string.tag___index, list.get(xx*3 + 0).getPattern_id());
            button_study.setTag(R.string.tag___order_no, list.get(xx*3 + 0).getPattern_no());
            button_study.setTag(R.string.tag___sentence, list.get(xx*3 + 0).getSentence());
        }
    }

    /* ======================================================================================================================= */
    //
    /* ======================================================================================================================= */

    @Data
    class StudyHistoryContent {
        String date;
        List<StudyHistoryContentVO> list = null;

        boolean open_flag = false;                  // 열림/닫힘
        boolean loading_flag = false;               // 데이타 로딩 여부 (list == null && loading_flag == false 일때 데이타 로딩 시도)
    }
}
