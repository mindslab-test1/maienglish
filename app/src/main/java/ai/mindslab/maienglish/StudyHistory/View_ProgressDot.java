package ai.mindslab.maienglish.StudyHistory;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

import ai.mindslab.maienglish.Common.Utils;

public class View_ProgressDot extends View {
    final int WIDTH___CIRCLE_BORDER = 5;
    final int WIDTH___DOT = 8;
    final int WIDTH___DOT_BORDER = 3;

    final int COLOR___FOREGROUND = 0xFF13c0d7;
    final int COLOR___BACKGROUND = 0xFFb1bed5;

    Context mContext;
    int mColor_Foreground = COLOR___FOREGROUND;
    int mColor_Background = COLOR___BACKGROUND;

    /* ==================================================================================================================== */
    // 생성자
    /* ==================================================================================================================== */

    public View_ProgressDot(Context context) {
        this(context, null, 0);
    }

    public View_ProgressDot(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public View_ProgressDot(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    /* ==================================================================================================================== */
    // 그리기
    /* ==================================================================================================================== */

    Paint mPaint_Dot_background = null;
    Paint mPaint_Foreground = null;
    Paint mPaint_Background = null;

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(mPaint_Foreground == null) {
            mPaint_Foreground = new Paint();
            mPaint_Foreground.setAntiAlias(true);
            mPaint_Foreground.setColor(mColor_Foreground);
            mPaint_Foreground.setStyle(Paint.Style.STROKE);
            mPaint_Foreground.setStrokeWidth(Utils.dp2px(mContext, WIDTH___CIRCLE_BORDER));
            mPaint_Foreground.setStrokeCap(Paint.Cap.ROUND);

            mPaint_Background = new Paint();
            mPaint_Background.setAntiAlias(true);
            mPaint_Background.setColor(mColor_Background);
            mPaint_Background.setStyle(Paint.Style.STROKE);
            mPaint_Background.setStrokeWidth(Utils.dp2px(mContext, WIDTH___CIRCLE_BORDER));

            mPaint_Dot_background = new Paint();
            mPaint_Dot_background.setAntiAlias(true);
            mPaint_Dot_background.setColor(0xFFFFFFFF);
            mPaint_Dot_background.setStyle(Paint.Style.FILL);
        }

        canvas.drawArc(0 + Utils.dp2px(mContext, WIDTH___CIRCLE_BORDER), 0 + Utils.dp2px(mContext, WIDTH___CIRCLE_BORDER), getWidth() - Utils.dp2px(mContext, WIDTH___CIRCLE_BORDER), getHeight() - Utils.dp2px(mContext, WIDTH___CIRCLE_BORDER), 0, 360, false, mPaint_Background);
        canvas.drawArc(0 + Utils.dp2px(mContext, WIDTH___CIRCLE_BORDER), 0 + Utils.dp2px(mContext, WIDTH___CIRCLE_BORDER), getWidth() - Utils.dp2px(mContext, WIDTH___CIRCLE_BORDER), getHeight() - Utils.dp2px(mContext, WIDTH___CIRCLE_BORDER), -90, 120, false, mPaint_Foreground);
    }
}
