package ai.mindslab.maienglish.StudyHistory;

import android.content.Intent;
import android.graphics.DashPathEffect;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.Model.StudyHistoryStatVO;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.Study.Study___Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;

public class StudyHistory___Score___Fragment extends Study___Fragment {
    View mLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mLayout = inflater.inflate(R.layout.study_history___score___fragment, container, false);
        ButterKnife.bind(this, mLayout);

        initTotalStatus();
        initChart();

        return mLayout;
    }

    /* ====================================================================================================================== */
    // 누적 현황
    /* ====================================================================================================================== */

    @BindView(R.id.Progress_Sentence) CircleSeekBar mProgress_Sentence;
    @BindView(R.id.Text_SentenceScore) TextView mText_SentenceScore;

    @BindView(R.id.Progress_Pron) CircleSeekBar mProgress_Pron;
    @BindView(R.id.Text_PronScore) TextView mText_PronScore;

    void initTotalStatus() {
        mProgress_Sentence.setProgressDisplay(0);
        mProgress_Sentence.setEnabled(false);
        mText_SentenceScore.setText("0");

        mProgress_Pron.setProgressDisplay(0);
        mProgress_Pron.setEnabled(false);
        mText_PronScore.setText("0");
    }


    /* ====================================================================================================================== */
    // 데이타
    /* ====================================================================================================================== */

    ArrayList<Entry> mGraphData_Sentence = new ArrayList<>();
    ArrayList<Entry> mGraphData_Pron = new ArrayList<>();

    ArrayList<ChartData> mList = new ArrayList<ChartData>();

    Calendar mCal = Calendar.getInstance();

    void buildData(StudyHistoryStatVO stat) {
        SimpleDateFormat date_fmt = new SimpleDateFormat("yyyy-MM-dd");

        ChartData stat_data;

        Entry entry_pron;
        LineDataSet data_set_pron;

        Entry entry_sentence;
        LineDataSet data_set_sentence;

        mText_SentenceScore.setText("" + (int) stat.getAscore());
        mProgress_Sentence.setProgressDisplay((int) stat.getAscore());

        mText_PronScore.setText("" + (int) stat.getPscore());
        mProgress_Pron.setProgressDisplay((int) stat.getPscore());

        mCal.setTime(new Date());
        mCal.add(Calendar.DAY_OF_YEAR, 1);

        /* 수신된 통계 데이타가 없을 경우, 오늘 날짜의 빈 데이타만 기록한다. */
        if(stat.getDaily_list().size() == 0) {
            mCal.add(Calendar.DAY_OF_YEAR, -1);
            String cur_date = date_fmt.format(mCal.getTime());

            entry_sentence = new Entry(0, 0, getResources().getDrawable(R.drawable.frame___graph___dot___sky));
            entry_pron = new Entry(0, 0, getResources().getDrawable(R.drawable.frame___graph___dot___sky));

            stat_data = new ChartData();
            stat_data.setDate(cur_date);
            stat_data.setPscore(0);
            stat_data.setAscore(0);
            stat_data.setPattern_num(0);

            mGraphData_Sentence.add(entry_sentence);
            mGraphData_Pron.add(entry_pron);
            mList.add(stat_data);
        }

        /* 오늘 날짜부터 수신된 데이타의 마지막 날짜까지 일별로 데이타 기록 */
        for(int xx = 0; xx < stat.getDaily_list().size(); xx++) {
            mCal.add(Calendar.DAY_OF_YEAR, -1);
            String cur_date = date_fmt.format(mCal.getTime());

            stat_data = new ChartData();

            /* 수신된 통계 데이타가 있으면 수신된 데이타로 기록 */
            if(stat.getDaily_list().get(xx).getDate_str().equals(cur_date)) {

                stat_data.setDate(cur_date);
                stat_data.setPscore(stat.getDaily_list().get(xx).getPscore());
                stat_data.setAscore(stat.getDaily_list().get(xx).getAscore());
                stat_data.setPattern_num(stat.getDaily_list().get(xx).getNum_pattern());
            }
            /* 수신된 데이타가 없으면 0 값으로 데이타 기록 */
            else {
                stat_data.setDate(cur_date);
                stat_data.setPscore(0);
                stat_data.setAscore(0);
                stat_data.setPattern_num(0);
                xx--;
            }

            mList.add(0, stat_data);
        }

        for(int xx = 0; xx < mList.size(); xx++) {
            entry_pron = new Entry(xx, mList.get(xx).getPscore(), getResources().getDrawable(R.drawable.frame___graph___dot___sky));
            mGraphData_Pron.add(entry_pron);

            entry_sentence = new Entry(xx, mList.get(xx).getAscore(), getResources().getDrawable(R.drawable.frame___graph___dot___sky));
            mGraphData_Sentence.add(entry_sentence);
        }

        setChartData();
    }

    LineDataSet assignLineDataSet(ArrayList<Entry> graph_data, String label, int color) {
        LineDataSet data_set;

        data_set = new LineDataSet(graph_data, label);
        data_set.setFillFormatter(new IFillFormatter() {
            @Override
            public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                return mChart.getAxisLeft().getAxisMinimum();
            }
        });

        data_set.setDrawIcons(true);
        data_set.setDrawCircles(true);
        data_set.setDrawCircleHole(true);
        data_set.setValueTextSize(10);

        // black lines and points
        data_set.setColor(color);
        data_set.setCircleColor(color);

        // line thickness and point size
        data_set.setCircleRadius(Utils.dp2px(mContext, 2));

        // draw points as solid circles
        data_set.setDrawCircleHole(true);

        data_set.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return "" + (int) value;
            }
        });

        return data_set;
    }

    void setChartData() {
        ArrayList<ILineDataSet> data_sets = new ArrayList<>();
        data_sets.add(assignLineDataSet(mGraphData_Sentence, "sentence", getResources().getColor(R.color.sky)));
        data_sets.add(assignLineDataSet(mGraphData_Pron, "pron", getResources().getColor(R.color.purple)));

        LineData data = new LineData(data_sets);

        // set data
        mChart.setData(data);

        mChart.setOnChartValueSelectedListener(mChart_Click);

        mChart.getData().notifyDataChanged();
        mChart.notifyDataSetChanged();

        mChart.setVisibleXRangeMaximum(14);
        mChart.moveViewToX(mList.size());

    }


    /* ====================================================================================================================== */
    // 챠트 설정
    /* ====================================================================================================================== */

    @BindView(R.id.Chart) LineChart mChart;

    void initChart() {
        mChart.setTouchEnabled(true);
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        mChart.setDrawGridBackground(false);
        mChart.getLegend().setEnabled(false);

        mChart.getDescription().setEnabled(false);

        mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                Toast.makeText(mContext, "select col = " + e.getX(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected() {

            }
        });

        ValueFormatter xAxisFormatter = new DateFormatter();
        XAxis xl = mChart.getXAxis();
        xl.setTextColor(0xFF7c8892);
        xl.setDrawGridLines(false);
        xl.setAvoidFirstLastClipping(true);
        xl.setEnabled(true);
        xl.setPosition(XAxis.XAxisPosition.BOTTOM);
        xl.setValueFormatter(xAxisFormatter);

        YAxis yl = mChart.getAxisLeft();
        yl.setTextColor(0xFF7c8892);
        yl.setAxisLineDashedLine(new DashPathEffect(new float[]{6f, 2f}, 0f));
        yl.setAxisMaximum(100);
        yl.setAxisMinimum(-10);

        yl = mChart.getAxisRight();
        yl.setEnabled(false);
    }

    /* ====================================================================================================================== */
    // 차트 X 포캣
    /* ====================================================================================================================== */

    public class DateFormatter extends ValueFormatter {
        @Override
        public String getFormattedValue(float index) {
            return mList.get((int) index).getDate().substring(5);
        }
    }


    /* ====================================================================================================================== */
    // 차트 이벤트
    /* ====================================================================================================================== */

    OnChartValueSelectedListener mChart_Click = new OnChartValueSelectedListener() {
        @Override
        public void onValueSelected(Entry e, Highlight h) {
            if(mList.get((int) e.getX()).getPattern_num() == 0) {
                Toast.makeText(mContext, "이 날짜에는 학습한 내용이 없습니다.", Toast.LENGTH_SHORT).show();
            }
            else {
                Intent it = new Intent(mContext, StudyHistory_Content___Activity.class);
                it.putExtra("date", mList.get((int) e.getX()).date);
                mContext.startActivity(it);
            }
        }

        @Override
        public void onNothingSelected() { }
    };
}
