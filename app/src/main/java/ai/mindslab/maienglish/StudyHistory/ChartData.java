package ai.mindslab.maienglish.StudyHistory;

import lombok.Data;

@Data
public class ChartData {
    String date;            // 학습 날짜

    int pattern_num;        // 학습 패턴 수

    int pscore;             // 발음점수
    int ascore;             // 문장점수
}
