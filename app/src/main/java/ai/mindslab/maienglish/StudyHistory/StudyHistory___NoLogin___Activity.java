package ai.mindslab.maienglish.StudyHistory;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ai.mindslab.maienglish.Book.Books___Activity;
import ai.mindslab.maienglish.Common.ConstDef;
import ai.mindslab.maienglish.Common.MaiDialog;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.ExternLib.RippleLayout;
import ai.mindslab.maienglish.Home___Activity;
import ai.mindslab.maienglish.Model.RestApiVO;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.Realm.UserFactory;
import ai.mindslab.maienglish.RestAPI.ResCode;
import ai.mindslab.maienglish.RestAPI.UserAPI;
import ai.mindslab.maienglish.SideMenu.SideMenu___Activity;
import ai.mindslab.maienglish.Subscribe.Subs___Activity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class StudyHistory___NoLogin___Activity extends AppCompatActivity {

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.study_history___nologin___activity);
        ButterKnife.bind(this);
        Utils.setColor_StatusBarIcon(this, true, this.getResources().getColor(R.color.sys_bg___books));

        Realm.init(this);
        realm = Realm.getDefaultInstance();

        initTopButton();
        initUI();
    }


    /* ############################################################################################################ */
    /* 상단 공통 버턴 */

    @BindView(R.id.Button_Back) RippleLayout mButton_Back;
    @BindView(R.id.Text_TopTitle) TextView mText_Title;

    void initTopButton() {
        mButton_Back.setOnRippleCompleteListener(new RippleLayout.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleLayout rippleView) {
                onBackPressed();
            }
        });

        mText_Title.setText(getString(R.string.study_history___title));
    }



    /* ############################################################################################################ */
    /* UI */

    @BindView(R.id.Image_Graph) ImageView mImage_Graph;

    void initUI() {
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) mImage_Graph.getLayoutParams();
        lp.width = Utils.getScreenWidth(this);
        lp.height = Utils.getScreenWidth(this);
    }

    @OnClick(R.id.Button_Subscribe)
    void onClick_Subscribe(View view) {
        Intent it = new Intent(StudyHistory___NoLogin___Activity.this, Subs___Activity.class);
        startActivity(it);
    }
}
