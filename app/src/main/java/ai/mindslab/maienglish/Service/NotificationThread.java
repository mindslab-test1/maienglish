package ai.mindslab.maienglish.Service;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import java.util.Calendar;
import java.util.Date;

import ai.mindslab.maienglish.SharedPref.SharedPref_Notification;

public class NotificationThread extends Thread {
    Context mContext;
    Handler handler;
    boolean isRun = true;

    public NotificationThread(Context ctx, Handler handler){
        this.handler = handler;
        mContext = ctx;
    }

    public void stopForever(){
        synchronized (this) {
            this.isRun = false;
        }
    }

    public void run(){
        int lastRunTime = -1;

        //반복적으로 수행할 작업을 한다.
        while(isRun){

            /* 10초 단위로 작업 수행 */
            try{
                Thread.sleep(10000); //10초씩 쉰다.
            }catch (Exception e) {}

            /* 현재 시각을 금일의 24시간 단위의 분으로 계산 */
//            long curtime = (System.currentTimeMillis()/1000/60)%(24*60);
//            int hour = (int) (curtime/60);
//            int min = (int) (curtime%60);

            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            int hour = cal.get(Calendar.HOUR_OF_DAY);
            int min = cal.get(Calendar.MINUTE);

            Log.e("AAA", String.format("Cur time = %d:%d", hour, min));

            if(hour*100 + min != lastRunTime) lastRunTime = -1;

            boolean noti_flag = SharedPref_Notification.getFlag(mContext);
            int noti_time = SharedPref_Notification.getTime(mContext);

            if(noti_flag == false) continue;
            if(lastRunTime >= 0) continue;
            if(hour*100 + min != noti_time) continue;

            lastRunTime = hour*100 + min;
            handler.sendEmptyMessage(0);
        }
    }

}
