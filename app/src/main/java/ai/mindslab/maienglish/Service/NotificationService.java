package ai.mindslab.maienglish.Service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import ai.mindslab.maienglish.Common.ConstDef;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Intro.Splash___Activity;
import ai.mindslab.maienglish.Model.RestApiVO;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.RestAPI.CommonAPI;
import ai.mindslab.maienglish.RestAPI.ResCode;
import ai.mindslab.maienglish.RestAPI.StudyAPI;
import ai.mindslab.maienglish.RestAPI.UserAPI;
import ai.mindslab.maienglish.StudyHistory.StudyHistory___Activity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NotificationService extends Service {
    NotificationManager Notifi_M;
    NotificationThread mThread = null;
    Notification Notifi ;

    @Override
    public IBinder onBind(Intent intent) {
        Log.e("AAA", "[ SERVICE ] onBind()");
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("AAA", "[ SERVICE ] onStartCommand()");
        initAPI();

        if(mThread != null) return START_STICKY;

        Notifi_M = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel("channel_id", "channel_name", NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.setDescription("channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.GREEN);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 100, 200});
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            Notifi_M.createNotificationChannel(notificationChannel);
        }


        myServiceHandler handler = new myServiceHandler();
        mThread = new NotificationThread(this.getApplicationContext(), handler);
        mThread.start();
        return START_STICKY;
    }

    //서비스가 종료될 때 할 작업

    public void onDestroy() {
        mThread.stopForever();
        mThread = null;//쓰레기 값을 만들어서 빠르게 회수하라고 null을 넣어줌.
    }

    class myServiceHandler extends Handler {
        @Override
        public void handleMessage(android.os.Message msg) {
            getNotiAlarm();
        }
    };

    /* ============================================================================================================= */
    // API
    /* ============================================================================================================= */

    CommonAPI mAPI_Common = null;

    void initAPI() {

        if(mAPI_Common == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ConstDef.BASE_API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            mAPI_Common = retrofit.create(CommonAPI.class);
        }

        Log.e("AAA", "initAPI.................................. mAPI_Common = " + mAPI_Common);
    }

    void getNotiAlarm() {
        Log.e("AAA", "REST-API ............................ getNotiAlarm()");
        mAPI_Common.getNotiAlarm(SharedInfo.User.mAuthKey, SharedInfo.Subscription.getSubsStatus()).enqueue(new Callback<RestApiVO>() {
            @Override
            public void onResponse(@NonNull Call<RestApiVO> call, @NonNull Response<RestApiVO> response) {
                if(response.code() == 401) {
                    Toast.makeText(getApplicationContext(), "인증이 되지 않았습니다. ", Toast.LENGTH_SHORT).show();
                    return;
                }
                RestApiVO body = response.body();
                if (body != null && body.getRes_code() == ResCode.SUCC) {
                    regiNotiAlarm(body.getNoti_alarm().getTitle(), body.getNoti_alarm().getMessage());
                } else {
                    Toast.makeText(getApplicationContext(), "네트워크가 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestApiVO> call, @NonNull Throwable t) {
                Log.e("AAA", "API.getStudyHistoryStat() >> onFailure() >> " + t.toString());
                Toast.makeText(getApplicationContext(), "네트워크가 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    void regiNotiAlarm(String title, String message) {
        final int BASE_NOTIFICATION_ID = 1028;

        Intent intent = new Intent(NotificationService.this, Splash___Activity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(NotificationService.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Notifi = new Notification.Builder(getApplicationContext(), "channel_id")
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.mipmap.mai)
                    .setTicker("마이잉글리시 알림!!!")
                    .setContentIntent(pendingIntent)
                    .setWhen(System.currentTimeMillis())
                    .setShowWhen(true)
                    .build();
        } else {
            Notifi = new Notification.Builder(getApplicationContext())
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.mipmap.mai)
                    .setTicker("마이잉글리시 알림!!!")
                    .setContentIntent(pendingIntent)
                    .setWhen(System.currentTimeMillis())
                    .setShowWhen(true)
                    .build();
        }

        //소리추가
        Notifi.defaults = Notification.DEFAULT_SOUND;

        //알림 소리를 한번만 내도록
        Notifi.flags = Notification.FLAG_ONLY_ALERT_ONCE;

        //확인하면 자동으로 알림이 제거 되도록
        Notifi.flags = Notification.FLAG_AUTO_CANCEL;

        int notiId = BASE_NOTIFICATION_ID + (int)(Math.random()*100);
        Notifi_M.notify( notiId , Notifi);
    }

}
