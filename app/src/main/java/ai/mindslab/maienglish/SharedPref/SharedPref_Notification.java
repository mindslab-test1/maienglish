package ai.mindslab.maienglish.SharedPref;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import ai.mindslab.maienglish.Common.ConstDef;

import static android.content.Context.MODE_PRIVATE;

/*
** 알림 관련 설정 처리 클래스
*/
public class SharedPref_Notification {
    static final String NOTI_FLAG = "noti_flag";
    static final String NOTI_TIME = "noti_time";

    static Object mLock = new Object();
    public static SharedPreferences mPref = null;

    public static boolean getFlag(Context ctx) {
        synchronized (mLock) {
            if (mPref == null) mPref = ctx.getSharedPreferences(ConstDef.SP_SETUP, MODE_PRIVATE);
            return mPref.getBoolean(NOTI_FLAG, true);
        }
    }

    public static void setFlag(Context ctx, boolean flag) {
        synchronized (mLock) {
            if (mPref == null) mPref = ctx.getSharedPreferences(ConstDef.SP_SETUP, MODE_PRIVATE);
            SharedPreferences.Editor editor = mPref.edit();
            editor.putBoolean(NOTI_FLAG, flag);
            editor.commit();
        }
    }

    /*
    ** time/100 -> 시각
    ** time%100 -> 분
    */
    public static int getTime(Context ctx) {
        synchronized (mLock) {
            if (mPref == null) mPref = ctx.getSharedPreferences(ConstDef.SP_SETUP, MODE_PRIVATE);
            return mPref.getInt(NOTI_TIME, 900);
        }
    }

    public static String getTimeToString(Context ctx) {
        synchronized (mLock) {
            if (mPref == null) mPref = ctx.getSharedPreferences(ConstDef.SP_SETUP, MODE_PRIVATE);
            return String.format("%02d:%02d", mPref.getInt(NOTI_TIME, 900)/100, mPref.getInt(NOTI_TIME, 900)%100);
        }
    }

    public static void setTime(Context ctx, int time) {
        synchronized (mLock) {
            if (mPref == null) mPref = ctx.getSharedPreferences(ConstDef.SP_SETUP, MODE_PRIVATE);
            SharedPreferences.Editor editor = mPref.edit();
            editor.putInt(NOTI_TIME, time);
            editor.commit();
        }
    }
}
