package ai.mindslab.maienglish.Intro;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Study.StudyPractice_Sub___Fragment;

public class Ad___Adapter extends FragmentPagerAdapter {

    public Ad___Adapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return SharedInfo.mList_Ad.size();
    }

    @Override
    public Fragment getItem(int position) {
        return Ad___Fragment.newInstance(position);
    }
}