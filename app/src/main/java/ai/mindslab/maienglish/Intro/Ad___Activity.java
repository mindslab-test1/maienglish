package ai.mindslab.maienglish.Intro;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.Home___Activity;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.Study.StudyChallenge___Fragment;
import ai.mindslab.maienglish.Study.StudyPractice___Adapter;
import ai.mindslab.maienglish.Study.StudyPractice___Fragment;
import ai.mindslab.maienglish.Study.StudyRepeat___Fragment;
import ai.mindslab.maienglish.Study.Study___Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;
import ss.com.bannerslider.Slider;
import ss.com.bannerslider.adapters.SliderAdapter;
import ss.com.bannerslider.event.OnSlideChangeListener;
import ss.com.bannerslider.viewholder.ImageSlideViewHolder;

public class Ad___Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ad___activity);
        ButterKnife.bind(this);
        Utils.setColor_StatusBarIcon(this, true, this.getResources().getColor(R.color.sys_bg___ad));

        initPager();
    }


    /* ######################################################################################################################## */
    /* PAGER */
    /* ======================================================================================================================== */

    @BindView(R.id.ViewPager) ViewPager mViewPager;
    @BindView(R.id.Pager_Indicator) CircleIndicator mPager_Indicator;

    void initPager() {
        mViewPager.addOnPageChangeListener(mOnPageChange);
        mViewPager.setAdapter(new Ad___Adapter(getSupportFragmentManager()));
        mPager_Indicator.setViewPager(mViewPager);

        mOnPageChange.onPageSelected(0);
    }

    ViewPager.OnPageChangeListener mOnPageChange = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) { }
        @Override
        public void onPageScrollStateChanged(int i) { }

        @Override
        public void onPageSelected(int position) {
            if(position < SharedInfo.mList_Ad.size() -1) skipButton(position);
            else startButton(position);
        }
    };


    /* ######################################################################################################################## */
    /* BUTTON */
    /* ======================================================================================================================== */

    @BindView(R.id.Button_Start)
    TextView mButton_Start;

    @OnClick(R.id.Button_Start)
    public void goHome() {
        Intent it = new Intent(Ad___Activity.this, Home___Activity.class);
        startActivity(it);
        finish();
    }

    void skipButton(int position) {
        mButton_Start.setText("SKIP");
        mButton_Start.setBackgroundColor(Color.parseColor(SharedInfo.mList_Ad.get(position).getColor()));
    }

    void startButton(int position) {
        mButton_Start.setText("START");
        mButton_Start.setBackgroundColor(Color.parseColor(SharedInfo.mList_Ad.get(position).getColor()));
    }
}
