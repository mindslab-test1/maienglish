package ai.mindslab.maienglish.Intro;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.badoualy.stepperindicator.StepperIndicator;
import com.squareup.picasso.Picasso;

import java.util.Map;

import ai.mindslab.maienglish.Common.ConstDef;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.RestAPI.Param;
import ai.mindslab.maienglish.Study.SttResultDialog;
import ai.mindslab.maienglish.Study.StudyChallenge___Adapter;
import ai.mindslab.maienglish.Study.Study___Activity;
import ai.mindslab.maienglish.Study.Study___Fragment;
import ai.mindslab.maienglish.Study.ViewPager_NoSwiping;
import butterknife.BindView;
import butterknife.ButterKnife;

public class Ad___Fragment extends Study___Fragment {
    View    mLayout;
    int     mPosition;

    public static Ad___Fragment newInstance(int position) {
        Ad___Fragment frag = new Ad___Fragment();
        frag.mPosition = position;

        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mLayout = inflater.inflate(R.layout.ad___fragment, container, false);
        ButterKnife.bind(this, mLayout);

        ImageView imageView = (ImageView) mLayout.findViewById(R.id.ImageView);
        Picasso.with(this.getContext()).load(ConstDef.BASE_IMAGE_URL + SharedInfo.mList_Ad.get(mPosition).getImg_path()).into(imageView);

        return mLayout;
    }


}
