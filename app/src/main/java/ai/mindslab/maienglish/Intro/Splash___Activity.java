package ai.mindslab.maienglish.Intro;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.wang.avi.AVLoadingIndicatorView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import ai.mindslab.maienglish.Common.ConstDef;
import ai.mindslab.maienglish.Common.MaiDialog;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.Event.SurveyEvent___Activity;
import ai.mindslab.maienglish.Home___Activity;
import ai.mindslab.maienglish.Model.AppVersionVO;
import ai.mindslab.maienglish.Model.RestApiVO;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.Realm.Realm_User;
import ai.mindslab.maienglish.Realm.UserFactory;
import ai.mindslab.maienglish.RestAPI.CommonAPI;
import ai.mindslab.maienglish.RestAPI.Param;
import ai.mindslab.maienglish.RestAPI.ResCode;
import ai.mindslab.maienglish.RestAPI.UserAPI;
import ai.mindslab.maienglish.SchedulerTask.Worker_FreeCoupon;
import ai.mindslab.maienglish.Service.NotificationService;
import ai.mindslab.maienglish.Unit.Unit___RecyclerView_Adapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/* =================================================================================================

   * 앱 업데이트 로그
   * 로그인 정보 존재 시, 자동 로그인

   ================================================================================================= */

public class Splash___Activity extends AppCompatActivity implements BillingProcessor.IBillingHandler {

    private Realm realm;
    private BillingProcessor mBilling;

    boolean billing_check_flag = false;
    boolean etc_check_flag = false;

    @BindView(R.id.Text_Version) TextView mText_Version;
    @BindView(R.id.Progress) AVLoadingIndicatorView mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash___activity);
        ButterKnife.bind(this);
        Utils.setColor_StatusBarIcon(this, false, this.getResources().getColor(R.color.sys_bg___splash));

        Realm.init(Splash___Activity.this);
        realm = Realm.getDefaultInstance();

        new Task_Init().execute();
        mText_Version.setText(String.format("Ver. %d.%d.%d", ConstDef.AppVersion.MAJOR, ConstDef.AppVersion.MINOR, ConstDef.AppVersion.PATCH));

        Log.e("AAA", "Language .................................................... " + Utils.getSystemLanguage(this));

        /* 알림 서비스 기동 */
        Intent intent = new Intent(Splash___Activity.this, NotificationService.class);
        startService(intent);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!mBilling.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onDestroy() {
        if (mBilling != null) {
            mBilling.release();
        }
        mProgress.smoothToHide();
        super.onDestroy();
    }

    /* ============================================================================================================================================ */
    // 초기화
    /* ============================================================================================================================================ */

    class Task_Init extends AsyncTask<Void /* doInBackground IN */, Void /* onProgressUpdate IN */, Integer /* doInBackground OUT, onPostExecute IN */> {
        protected Integer doInBackground(Void ... values) {

            initAPI();
            initBilling();
            return 0;
        }

        protected void onProgressUpdate(Void... values) { }

        protected void onPostExecute(Integer value) {
            mProgress.setVisibility(View.VISIBLE);
            mHandler.sendEmptyMessage(MSG___GO___CHECK_VERSION);
        }
    }


    /* ############################################################################################# */
    // 메세지 핸들링

    final int MSG___GO___CHECK_VERSION = 1;
    final int MSG___COMPLETED___CHECK_VERSION = 2;
    final int MSG___GO___LOGIN = 3;
    final int MSG___COMPLETED___LOGIN = 4;
    final int MSG___COMPLETED___CHECK_BILLING = 5;

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what) {
                case MSG___GO___CHECK_VERSION:
                    beginTime = System.currentTimeMillis();
                    checkVersion();
                    break;

                case MSG___COMPLETED___CHECK_VERSION:
                    sendEmptyMessage(MSG___GO___LOGIN);
                    break;

                case MSG___GO___LOGIN:
                    login();
                    break;

                case MSG___COMPLETED___LOGIN:
                    etc_check_flag = true;
                    if(billing_check_flag == true) goNext();
                    break;

                case MSG___COMPLETED___CHECK_BILLING:
                    billing_check_flag = true;
                    if(etc_check_flag == true) goNext();
                    break;
            }
        }
    };


    /* ############################################################################################# */
    // 버전 체크

    void checkVersion() {
        String last_notice_date = Utils.getCurrentDateTime();
        SharedPreferences sp = getSharedPreferences(ConstDef.SPREF_NAME, Activity.MODE_PRIVATE);
        last_notice_date = sp.getString(ConstDef.SPREF_PARAM___LAST_NOTICE_DATE, last_notice_date);

        Log.e("AAA", "Loaded Last date = " + last_notice_date);

        mAPI_Common.checkVersion(last_notice_date).enqueue(new Callback<RestApiVO>() {
            @Override
            public void onResponse(@NonNull Call<RestApiVO> call, @NonNull Response<RestApiVO> response) {

                RestApiVO body = response.body();
                if (body != null && body.getRes_code() == ResCode.SUCC) {
                    // 설정 등록
                    SharedInfo.Subscription.mPrice = body.getSubs_price();
                    SharedInfo.Notice.mNewNoticeCount = body.getNotice_count();
                    SharedInfo.Support.mEmail = body.getSupport_email();
                    SharedInfo.Support.mPhone = body.getSupport_phone();

                    // 무료 정보
                    SharedInfo.Free.mFreeBookCnt = body.getFree_book_count();
                    SharedInfo.Free.mFreePatternCnt = body.getFree_pattern_count();
                    SharedInfo.Coupon.mSurveyUrl = body.getSurvey_url();

                    // 광고 목록 등록
                    SharedInfo.mList_Ad = body.getAd_list();

                    // 버전 체크
                    validateVersion(body.getApp_version());

                } else {
                    showNetworkFail();
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestApiVO> call, @NonNull Throwable t) {
                Log.e("AAA", "[ checkVersion ] Network fail >> " + t.getMessage());
                showNetworkFail();
            }
        });

    }

    void validateVersion(final AppVersionVO app_ver) {
        Log.e("AAA", "Last AppVer = " + app_ver.toString());

        int server_version = app_ver.getMajor_ver()*100 + app_ver.getMinor_ver();
        int app_version = ConstDef.AppVersion.MAJOR*100 + ConstDef.AppVersion.MINOR;

        if(server_version > app_version) {
            MaiDialog.build(this)
                    .setOk("업데이트 가기", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse( app_ver.getStore_url().replace("https", "market") )));
                            }
                            catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse( app_ver.getStore_url() )));
                            }

                            finish();
                        }
                    })
                    .setCancel("종료", mOnClick_Term)
                    .setMessage(app_ver.getDescription())
                    .setImage(R.mipmap.info)
                    .show();
        }
        else if(server_version == app_version && app_ver.getPatch_ver() > ConstDef.AppVersion.PATCH){
            MaiDialog.build(this)
                    .setOk("업데이트 하러 가기", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse( app_ver.getStore_url() )));
                            finish();
                        }
                    })
                    .setCancel("다음에", mOnClick_Skip)
                    .setMessage(app_ver.getDescription())
                    .setImage(R.mipmap.info)
                    .show();
        }
        else mHandler.sendEmptyMessage(MSG___COMPLETED___CHECK_VERSION);
    }


    View.OnClickListener mOnClick_Term = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    View.OnClickListener mOnClick_Skip = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mHandler.sendEmptyMessage(MSG___COMPLETED___CHECK_VERSION);
        }
    };

    /* ############################################################################################# */
    // 자동 로그인

    Realm_User mUser = null;

    void login() {
        mUser = UserFactory.get(realm);
        SharedInfo.User.reset();
        if(mUser != null) {
            SharedInfo.User.mAuthKey = mUser.getAuthKey();
            SharedInfo.User.mEmail = mUser.getEmail();
            SharedInfo.User.mPhotoUrl = mUser.getPhotoUrl();
            SharedInfo.User.mName = mUser.getName();
        }

        /* 사용자의 앱 두번째 이후 실행 */
        if(mUser != null) {
            SharedInfo.User.mFirstFlag = false;

            /* 인증키가 등록되어 있지 않으면, 자동로그인 대상 아님 */
            if(mUser.getAuthKey() == null) {
                Log.e("AAA", "[ 로그인 ] ................................... 자동 로그인 대상 아님");
                mHandler.sendEmptyMessage(MSG___COMPLETED___LOGIN);
            }
            /* 인증키가 등록되어 있으면, 자동 로그인 대상 */
            else {
                Log.e("AAA", "[ 로그인 ] ................................... 자동 로그인 대상");
                autoLogin();
            }
        }
        /* 사용자의 앱 첫 실행 */
        else {
            mUser = UserFactory.create(realm);
            mHandler.sendEmptyMessage(MSG___COMPLETED___LOGIN);
        }
    }

    void autoLogin() {
        mAPI_User.login(mUser.getAuthKey()).enqueue(new Callback<RestApiVO>() {
            @Override
            public void onResponse(@NonNull Call<RestApiVO> call, @NonNull Response<RestApiVO> response) {
                if(response.code() == 401) {
                    SharedInfo.User.reset();
                    UserFactory.logout(realm);
                    mHandler.sendEmptyMessage(MSG___COMPLETED___LOGIN);
                    return;
                }
                RestApiVO body = response.body();
                if (body != null && body.getRes_code() == ResCode.SUCC) {
                    SharedInfo.User.mCreateDate = body.getDatetime();
                    SharedInfo.User.mAuthKey = mUser.getAuthKey();

                    SharedInfo.Coupon.mFreeCoupon = body.getCoupon();
                    if(SharedInfo.Coupon.getFreeCoupon() != null) Worker_FreeCoupon.build();

                    SharedInfo.User.mFreeExpSurvey = body.getFree_exp_survey();
                    mHandler.sendEmptyMessage(MSG___COMPLETED___LOGIN);
                } else {
                    showNetworkFail();
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestApiVO> call, @NonNull Throwable t) {
                Log.e("AAA", "API.login() >> onFailure() >> " + t.toString());
                showNetworkFail();
            }
        });
    }


    /* ############################################################################################# */
    // 다음 단계 진행

    /* Splash의 최소 노출 시간 설정 */
    final int MIN_TIME = 2000;
    long beginTime;
    void goNext() {
        long remainedTime = MIN_TIME - (System.currentTimeMillis() - beginTime);
        if(remainedTime <= 0) remainedTime = 0;

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                /* */
                if(SharedInfo.User.mFirstFlag == true) {
                    Intent it = new Intent(Splash___Activity.this, Ad___Activity.class);
                    startActivity(it);
                    finish();
                }
                else {
                    /* 설문지 작성 대상자인 경우 */
                    if(SharedInfo.User.isLogined()) {
                        if (SharedInfo.User.mFreeExpSurvey == 1) {
                            Intent it = new Intent(Splash___Activity.this, SurveyEvent___Activity.class);
                            startActivity(it);
                            finish();
                            return;
                        }
                    }

                    Intent it = new Intent(Splash___Activity.this, Home___Activity.class);
                    startActivity(it);
                    finish();
                }

            }
        }, remainedTime);
    }


    /* ############################################################################################# */
    // API

    CommonAPI mAPI_Common;
    UserAPI mAPI_User;

    void initAPI() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConstDef.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mAPI_Common = retrofit.create(CommonAPI.class);
        mAPI_User = retrofit.create(UserAPI.class);
    }


    /* ############################################################################################# */
    // 네트워크 오류에 의한 서비스 종료

    void showNetworkFail() {
        MaiDialog.build(this)
                .setImage(R.mipmap.notice)
                .setMessage("현재 네트워크 상태가 원활하지 않습니다. \n잠시 후, 다시 이용해 주세요.")
                .setOk("확인", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                })
                .show();
    }


    /* ================================================================================================================================ */
    // BILLING
    /* ================================================================================================================================ */

    void initBilling() {
        Log.e("AAA", "[ 구독 ] .............................................. initBilling()");
        mBilling = new BillingProcessor(this, ConstDef.BILLING_LICENSE_KEY, this);
        mBilling.initialize();

        /* 시뮬레이터에서 결제 기능이 동작하지 않아서, SKIP 하기 위한 코드 */
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                billing_check_flag = true;
                mHandler.sendEmptyMessage(MSG___COMPLETED___CHECK_BILLING);
            }
        }, 1500);
    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
        Log.d("AAA", "[ 구독 ] ....................................... onProductPurchased() >> " + details.toString());
    }

    @Override
    public void onPurchaseHistoryRestored() {
        Log.d("AAA", "[ 구독 ] ....................................... onPurchaseHistoryRestored()");
    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {
        Log.e("AAA", "[ 구독 ] ....................................... onBillingError() >> " + errorCode);

        mHandler.sendEmptyMessage(MSG___COMPLETED___CHECK_BILLING);
    }

    @Override
    public void onBillingInitialized() {
        Log.d("AAA", "[ 구독 ] ....................................... onBillingInitialized()");

        SharedInfo.Subscription.mSubsFlag = (mBilling.isSubscribed(ConstDef.SUBS_CODE) == true ? 1 : 0);
        mHandler.sendEmptyMessage(MSG___COMPLETED___CHECK_BILLING);
    }
}
