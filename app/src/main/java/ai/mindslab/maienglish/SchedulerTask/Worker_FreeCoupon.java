package ai.mindslab.maienglish.SchedulerTask;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import androidx.work.PeriodicWorkRequest;
import androidx.work.Result;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Model.CouponVO;

public class Worker_FreeCoupon extends Worker {
    public Worker_FreeCoupon(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        Log.e("AAA", "Worker ................................... doWork().......freeCoupon");
        CouponVO coupon = SharedInfo.Coupon.getFreeCoupon();
        if(coupon == null) {
            Log.e("AAA", "Worker ................................... doWork().......freeCoupon... coupon==null");
            return Result.failure();
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date end_date;

        try {
            end_date = dateFormat.parse(coupon.getExpire_datetime());

            if(coupon.getExpire_datetime().compareTo(dateFormat.format(new Date())) < 0) {
                SharedInfo.Coupon.setFreeCoupon(null);
                release();

                Log.e("AAA", "[ Coupon ] 당신의 무료 쿠폰 사용이 만료되었습니다.");
                return Result.success();
            }

        } catch (ParseException e) {
            e.printStackTrace();
            release();

            Log.e("AAA", "[ Coupon ] 쿠폰 사용 기간 확인 중 에러 발생 >> " + e.getMessage());
            SharedInfo.Coupon.setFreeCoupon(null);
            return Result.failure();
        }

        long remained_time = (end_date.getTime() - new Date().getTime())/1000/60;

        Log.e("AAA", String.format("[ Coupon ] 무료 쿠폰 사용중 - %d일 %d시간 %d분 남았어요.", (remained_time/60)/24, (remained_time/60)%24, remained_time%60));
        return Result.success();
    }

    public static void build() {
        if(SharedInfo.Coupon.mFreeCoupon == null) return;

        SharedInfo.Workers.mWork_FreeCoupon = new PeriodicWorkRequest.Builder(Worker_FreeCoupon.class, 15, TimeUnit.MINUTES).build();
        WorkManager.getInstance().enqueue(SharedInfo.Workers.mWork_FreeCoupon);

        Log.e("AAA", "Worker ...................................................... mWork_FreeCoupon.. build() >> " + SharedInfo.Coupon.getFreeCoupon().toString());
    }

    public static void release() {
        if(SharedInfo.Workers.mWork_FreeCoupon != null) {
            UUID workerId = SharedInfo.Workers.mWork_FreeCoupon.getId();
            WorkManager.getInstance().cancelWorkById(workerId);
            SharedInfo.Workers.mWork_FreeCoupon = null;
            Log.e("AAA", "Worker ...................................................... mWork_FreeCoupon.. release()");
        }
    }

}
