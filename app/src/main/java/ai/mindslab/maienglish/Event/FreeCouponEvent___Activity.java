package ai.mindslab.maienglish.Event;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import ai.mindslab.maienglish.Book.Books___Activity;
import ai.mindslab.maienglish.Common.ConstDef;
import ai.mindslab.maienglish.Common.MaiDialog;
import ai.mindslab.maienglish.Common.MaiProgress;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.Coupon.CouponState;
import ai.mindslab.maienglish.Coupon.Coupon___Activity;
import ai.mindslab.maienglish.ExternLib.RippleLayout;
import ai.mindslab.maienglish.Model.CouponVO;
import ai.mindslab.maienglish.Model.RestApiVO;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.Realm.UserFactory;
import ai.mindslab.maienglish.RestAPI.CouponAPI;
import ai.mindslab.maienglish.RestAPI.Param;
import ai.mindslab.maienglish.RestAPI.ResCode;
import ai.mindslab.maienglish.RestAPI.UserAPI;
import ai.mindslab.maienglish.SchedulerTask.Worker_FreeCoupon;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FreeCouponEvent___Activity extends AppCompatActivity {

    private Realm realm;
    CouponVO mCoupon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.free_coupon_event___activity);
        Utils.setColor_StatusBarIcon(this, true, this.getResources().getColor(R.color.sys_bg___books));

        Intent it = getIntent();
        mCoupon = (CouponVO) it.getSerializableExtra(Param.COUPON);

        ButterKnife.bind(this);

        Realm.init(this);
        realm = Realm.getDefaultInstance();

        initProgress();
        initAPI();
        initTopLayer();
        initCouponInfo();
    }


    /* ============================================================================================================================== */
    // TOP LAYER
    /* ============================================================================================================================== */

    @BindView(R.id.Text_TopTitle) TextView mText_Title;
    @BindView(R.id.Button_Back) RippleLayout mButton_Back;

    void initTopLayer() {
        mText_Title.setText("쿠폰 등록");

        mButton_Back.setOnRippleCompleteListener(new RippleLayout.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleLayout rippleView) {
                finish();
            }
        });
    }

    /* ============================================================================================================================== */
    // Progress
    /* ============================================================================================================================== */
    private MaiProgress mProgress;
    void initProgress() {
        mProgress = MaiProgress.build(this);
    }


    /* ============================================================================================================================== */
    // COUPON INFO
    /* ============================================================================================================================== */

    @BindView(R.id.Text_Period) TextView mText_Period;
    void initCouponInfo() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date beginDateTime = new Date();
        String endDateTime = null;

        cal.setTime(beginDateTime);
        cal.add(Calendar.DAY_OF_YEAR, mCoupon.getFree_days());
        endDateTime = dateFormat.format(cal.getTime());

        mText_Period.setText(dateFormat.format(beginDateTime).substring(0, 16) + " ~ " + endDateTime.substring(0, 16));
    }


    /* ============================================================================================================================== */
    // START BUTTON
    /* ============================================================================================================================== */

    @OnClick(R.id.Button_Start)
    public void onClick_Start(View v) {
        if(SharedInfo.Subscription.isSubscribed()) {
            MaiDialog.build(this)
                    .setImage(R.mipmap.coupon)
                    .setMessage("이미 구독을 하고 계십니다.\n다음에 사용하세요.")
                    .setOk("확인", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent it = new Intent(FreeCouponEvent___Activity.this, Books___Activity.class);
                            it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(it);
                        }
                    }).show();
        } else if(SharedInfo.Coupon.mFreeCoupon != null) {
            MaiDialog.build(this)
                    .setImage(R.mipmap.coupon)
                    .setMessage("이미 무료 쿠폰을 이용하고 계십니다.\n다음에 사용하세요.")
                    .setOk("확인", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent it = new Intent(FreeCouponEvent___Activity.this, Books___Activity.class);
                            it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(it);
                        }
                    }).show();
        } else {
            requestUseCoupon();
        }
    }

    void requestUseCoupon() {
        mProgress.show();
        mAPI_Coupon.useCoupon(SharedInfo.User.mAuthKey, mCoupon.getPublishing_id()).enqueue(new Callback<RestApiVO>() {
            @Override
            public void onResponse(@NonNull Call<RestApiVO> call, @NonNull Response<RestApiVO> response) {
                mProgress.hide();

                RestApiVO body = response.body();
                if (body != null && body.getRes_code() == ResCode.SUCC) {
                    MaiDialog.build(FreeCouponEvent___Activity.this)
                            .setImage(R.mipmap.coupon)
                            .setMessage("쿠폰이 사용되었습니다.\n\n3일후에 알림으로 제공되는 설문지를 작성해 주시면 추가 일주일 무료 쿠폰을 드립니다.")
                            .setOk("확인", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent it = new Intent(FreeCouponEvent___Activity.this, Books___Activity.class);
                                    it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(it);
                                }
                            }).show();

//                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                    mCoupon.setState(CouponState.PUBLISHING___USED);
//                    mCoupon.setUse_datetime(dateFormat.format(new Date()));
                    mCoupon.set(body.getCoupon());
                    SharedInfo.Coupon.setFreeCoupon(mCoupon);
                    Worker_FreeCoupon.build();

                } else if(body != null && body.getRes_code() == ResCode.NOT_FOUND) {
                    Toast.makeText(FreeCouponEvent___Activity.this, "이 쿠폰은 사용이 불가능한 쿠폰입니다.", Toast.LENGTH_SHORT).show();
                } else if(body != null && body.getRes_code() == ResCode.ALREADY) {
                    Toast.makeText(FreeCouponEvent___Activity.this, "이미 사용된 쿠폰입니다.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(FreeCouponEvent___Activity.this, "(1) 서버와 연결이 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestApiVO> call, @NonNull Throwable t) {
                mProgress.hide();
                Toast.makeText(FreeCouponEvent___Activity.this, "(2) 서버와 연결이 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /* ============================================================================================================================== */
    // API
    /* ============================================================================================================================== */

    CouponAPI mAPI_Coupon;

    void initAPI() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConstDef.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mAPI_Coupon = retrofit.create(CouponAPI.class);
    }
}


