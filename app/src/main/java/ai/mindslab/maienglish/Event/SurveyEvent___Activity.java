package ai.mindslab.maienglish.Event;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ai.mindslab.maienglish.Common.ConstDef;
import ai.mindslab.maienglish.Common.MaiDialog;
import ai.mindslab.maienglish.Common.MaiProgress;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.Coupon.CouponState;
import ai.mindslab.maienglish.ExternLib.RippleLayout;
import ai.mindslab.maienglish.Home___Activity;
import ai.mindslab.maienglish.Intro.Splash___Activity;
import ai.mindslab.maienglish.Model.RestApiVO;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.Realm.UserFactory;
import ai.mindslab.maienglish.RestAPI.CouponAPI;
import ai.mindslab.maienglish.RestAPI.Param;
import ai.mindslab.maienglish.RestAPI.ResCode;
import ai.mindslab.maienglish.RestAPI.UserAPI;
import ai.mindslab.maienglish.SideMenu.Web___Activity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SurveyEvent___Activity extends AppCompatActivity {

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.survey_event___activity);
        ButterKnife.bind(this);
        Utils.setColor_StatusBarIcon(this, true, this.getResources().getColor(R.color.sys_bg___books));

        Realm.init(this);
        realm = Realm.getDefaultInstance();

        initAPI();
        initTopLayer();
        initProgress();
    }


    @Override
    public void onBackPressed() {
        Intent it = new Intent(this, Home___Activity.class);
        startActivity(it);
        finish();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /* 일주일 추가 쿠폰 등록 */
        mAPI_Coupon.publishCoupon(SharedInfo.User.mAuthKey, ConstDef.COUPON_ID___FREE_7).enqueue(new Callback<RestApiVO>() {
            @Override
            public void onResponse(@NonNull Call<RestApiVO> call, @NonNull Response<RestApiVO> response) {
                mProgress.hide();

                RestApiVO body = response.body();
                if (body != null && body.getRes_code() == ResCode.SUCC) {
                    MaiDialog.build(SurveyEvent___Activity.this)
                            .setImage(R.mipmap.coupon_sky)
                            .setMessage("7일 무료 쿠폰이 발급되었습니다.")
                            .setOk("확인", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    finish();
                                    Intent it = new Intent(SurveyEvent___Activity.this, Home___Activity.class);
                                    startActivity(it);
                                }
                            }).show();

                } else {
                    Toast.makeText(SurveyEvent___Activity.this, "무료 쿠폰 발급이 실패했습니다.\n다음에 다시 시도해 주세요.", Toast.LENGTH_SHORT).show();

                    Intent it = new Intent(SurveyEvent___Activity.this, Home___Activity.class);
                    startActivity(it);
                    finish();
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestApiVO> call, @NonNull Throwable t) {
                mProgress.hide();
                Toast.makeText(SurveyEvent___Activity.this, "(1) 서버와 연결이 원활하지 않습니다.\n다음에 다시 시도해 주세요.", Toast.LENGTH_SHORT).show();

                Intent it = new Intent(SurveyEvent___Activity.this, Home___Activity.class);
                startActivity(it);
                finish();
            }
        });
    }

    /* ============================================================================================================================== */
    // TOP LAYER
    /* ============================================================================================================================== */

    @BindView(R.id.Text_TopTitle) TextView mText_Title;
    @BindView(R.id.Button_Back) RippleLayout mButton_Back;

    void initTopLayer() {
        mText_Title.setText("설문조사");

        mButton_Back.setOnRippleCompleteListener(new RippleLayout.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleLayout rippleView) {
                onBackPressed();
            }
        });
    }



    /* ============================================================================================================================== */
    // Progress
    /* ============================================================================================================================== */
    private MaiProgress mProgress;
    void initProgress() {
        mProgress = MaiProgress.build(this);
    }



    /* ============================================================================================================================== */
    // Survey
    /* ============================================================================================================================== */

    @OnClick(R.id.Button_Survey)
    public void onClick_Survey(View v) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(SharedInfo.Coupon.mSurveyUrl));
        intent.setPackage("com.android.chrome");   // 브라우저가 여러개 인 경우 콕 찍어서 크롬을 지정할 경우
        startActivityForResult(intent, 0);
    }



    /* ============================================================================================================================== */
    // API
    /* ============================================================================================================================== */

    CouponAPI mAPI_Coupon;

    void initAPI() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConstDef.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mAPI_Coupon = retrofit.create(CouponAPI.class);
    }
}
