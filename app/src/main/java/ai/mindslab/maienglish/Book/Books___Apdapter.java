package ai.mindslab.maienglish.Book;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ai.mindslab.maienglish.RestAPI.Param;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.ExternLib.MaterialRippleLayout;
import ai.mindslab.maienglish.Model.BookVO;
import ai.mindslab.maienglish.Model.UnitVO;
import ai.mindslab.maienglish.Model.mAITalkVO;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.Unit.Unit___Activity;
import ai.mindslab.maienglish.mAITalk.mAI_Talk___Activity;

public class Books___Apdapter extends PagerAdapter {

    Context mContext;
    List<BookVO> mList;
    List<View> mList_BookView;

    Books___Apdapter(Context ctx, List<BookVO> list, List<View> listView) {
        mContext = ctx;
        mList = list;
        mList_BookView = listView;
    }

    @Override
    public int getCount() {
        return (mList == null ? 0 : mList.size());
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object obj) {
        return view == obj;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = mList_BookView.get(position);
        container.addView(view);

        Log.e("AAA", String.format("positionn=%d, title=%s", position, mList.get(position).getTitle(mContext)));
        /* 타이틀 출력 */
        String[] sepTitle = mList.get(position).getTitle(mContext).split(";");
        TextView textView = (TextView) view.findViewById(R.id.Text_UpTitle);
        if(sepTitle.length > 0) textView.setText(sepTitle[0]);
        textView = (TextView) view.findViewById(R.id.Text_DownTitle);
        if(sepTitle.length > 1) textView.setText(sepTitle[1]);

        setContents(view, position);
        return view;
    }

    void setContents(View view, int position) {

        /* Free Text */
        android.support.v7.widget.GridLayout.LayoutParams lp1 = (android.support.v7.widget.GridLayout.LayoutParams) view.findViewById(R.id.Unit1).getLayoutParams();
        android.support.v7.widget.GridLayout.LayoutParams lp2 = (android.support.v7.widget.GridLayout.LayoutParams) view.findViewById(R.id.Talk1).getLayoutParams();

        Log.e("AAA", "IsAllowedAllContent.................... " + Utils.isAllowedAllContent());
        Log.e("AAA", "List Size = " + mList.size());
        if(Utils.isAllowedAllContent() == false) {
            View freeView = view.findViewById(R.id.Text_Free1);
            freeView.setVisibility(View.VISIBLE);
            freeView = view.findViewById(R.id.Text_Free2);
            freeView.setVisibility(View.VISIBLE);

            lp1.topMargin = Utils.dp2px(mContext, 4);
            lp2.topMargin = Utils.dp2px(mContext, 4);
        } else {
            View freeView = view.findViewById(R.id.Text_Free1);
            freeView.setVisibility(View.GONE);
            freeView = view.findViewById(R.id.Text_Free2);
            freeView.setVisibility(View.GONE);

            lp1.topMargin = Utils.dp2px(mContext, 32);
            lp2.topMargin = Utils.dp2px(mContext, 32);
        }

        setUnitData(view.findViewById(R.id.Unit1), mList.get(position).getList_unit().get(0), position, 0);
        setUnitData(view.findViewById(R.id.Unit2), mList.get(position).getList_unit().get(1), position, 1);
        setUnitData(view.findViewById(R.id.Unit3), mList.get(position).getList_unit().get(2), position, 2);
        setUnitData(view.findViewById(R.id.Unit4), mList.get(position).getList_unit().get(3), position, 3);

        setTalkData(view.findViewById(R.id.Talk1), mList.get(position).getList_mai_talk().get(0), mList.get(position).getList_unit().get(0).getUnit_no(), position, 0);
        setTalkData(view.findViewById(R.id.Talk2), mList.get(position).getList_mai_talk().get(1), mList.get(position).getList_unit().get(1).getUnit_no(), position, 1);
        setTalkData(view.findViewById(R.id.Talk3), mList.get(position).getList_mai_talk().get(2), mList.get(position).getList_unit().get(2).getUnit_no(), position, 2);
        setTalkData(view.findViewById(R.id.Talk4), mList.get(position).getList_mai_talk().get(3), mList.get(position).getList_unit().get(3).getUnit_no(), position, 3);
    }

    public void updateContents() {
        for(int xx = 0; xx < mList_BookView.size(); xx++) {
            setContents(mList_BookView.get(xx), xx);
        }
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    View.OnClickListener mOnClick_Unit = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int bookPosition = (Integer) v.getTag(R.string.tag___book_position);
            int unitIndex = (Integer) v.getTag(R.string.tag___unit_index);

            SharedInfo.Study.mBook = mList.get(bookPosition);

            Intent it = new Intent(mContext, Unit___Activity.class);
            it.putExtra("book_no", bookPosition+1);
            it.putExtra("unitIndex", unitIndex);
            it.putExtra("color", mList.get(bookPosition).getColor());
            mContext.startActivity(it);
        }
    };

    View.OnClickListener mOnClick_Talk = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int talkId = (Integer) v.getTag(R.string.tag___id);

            Intent it = new Intent(mContext, mAI_Talk___Activity.class);
            it.putExtra(Param.MAI_TALK_ID, talkId);
            mContext.startActivity(it);
        }
    };

    /* ########################################################################################### */

    void setUnitData(View view, UnitVO unit, int bookPosition, int unitIdx) {
        TextView textView = (TextView) view.findViewById(R.id.Text_UnitNo);
        textView.setText("Unit " + (unit.getUnit_no()));
        textView = (TextView) view.findViewById(R.id.Text_Title);
        textView.setText(unit.getTitle(mContext));

        MaterialRippleLayout button = (MaterialRippleLayout) view.findViewById(R.id.Button_Unit);
        button.setOnClickListener(mOnClick_Unit);
        ((View) button.getChildView()).setTag(R.string.tag___book_position, bookPosition);
        ((View) button.getChildView()).setTag(R.string.tag___unit_index, unitIdx);

        /* 학습 가능: 구매중 | 무료대상 북이고 첫번째 유닛 */
        if(Utils.isAllowedAllContent() || (bookPosition < SharedInfo.Free.mFreeBookCnt && unitIdx == 0)) {
            button.setEnabled(true);
            view.setElevation(Utils.dp2px(mContext, 4));

            View mask = view.findViewById(R.id.Mask);
            mask.setVisibility(View.GONE);
        } else {
            button.setOnClickListener(null);
            button.setEnabled(false);

            View mask = view.findViewById(R.id.Mask);
            mask.setVisibility(View.VISIBLE);
            view.setElevation(0);
        }

        ImageView freeImage = (ImageView) view.findViewById(R.id.Image_Free);
        /* 무료표시: 비구매 && 학습 가능 */
        if(Utils.isAllowedAllContent() == false && (bookPosition < SharedInfo.Free.mFreeBookCnt && unitIdx == 0)) {
            freeImage.setVisibility(View.VISIBLE);
        } else {
            freeImage.setVisibility(View.GONE);
        }
    }

    void setTalkData(View view, mAITalkVO talk, int unitNo, int bookPosition, int unitIdx) {
        TextView textView = (TextView) view.findViewById(R.id.Text_TalkNo);
        textView.setText("mAI talk " + (unitNo));

        MaterialRippleLayout button = (MaterialRippleLayout) view.findViewById(R.id.Button_Talk);
        button.setOnClickListener(mOnClick_Talk);
        ((View) button.getChildView()).setTag(R.string.tag___id, talk.getMai_talk_id());

        /* 학습 가능: 구매중 | 무료대상 북이고 첫번째 유닛 */
        if(Utils.isAllowedAllContent() || (bookPosition < SharedInfo.Free.mFreeBookCnt && unitIdx == 0)) {
            button.setEnabled(true);
            view.setElevation(Utils.dp2px(mContext, 4));

            View mask = view.findViewById(R.id.Mask);
            mask.setVisibility(View.GONE);
        } else {
            button.setOnClickListener(null);
            button.setEnabled(false);
            view.setElevation(0);

            View mask = view.findViewById(R.id.Mask);
            mask.setVisibility(View.VISIBLE);
        }

        ImageView freeImage = (ImageView) view.findViewById(R.id.Image_Free);
        /* 무료표시: 비구매 && 학습 가능 */
        if(Utils.isAllowedAllContent() == false && (bookPosition < SharedInfo.Free.mFreeBookCnt && unitIdx == 0)) {
            freeImage.setVisibility(View.VISIBLE);
        } else {
            freeImage.setVisibility(View.GONE);
        }
    }
}
