package ai.mindslab.maienglish.Book;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ai.mindslab.maienglish.Common.ConstDef;
import ai.mindslab.maienglish.Common.MaiProgress;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.ExternLib.RippleLayout;
import ai.mindslab.maienglish.Home___Activity;
import ai.mindslab.maienglish.Model.BookVO;
import ai.mindslab.maienglish.Model.RestApiVO;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.RestAPI.BookAPI;
import ai.mindslab.maienglish.RestAPI.ResCode;
import ai.mindslab.maienglish.SideMenu.SideMenu___Activity;
import ai.mindslab.maienglish.StudyHistory.StudyHistory___Activity;
import ai.mindslab.maienglish.StudyHistory.StudyHistory___NoLogin___Activity;
import ai.mindslab.maienglish.Subscribe.Subs___Activity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Books___Activity extends AppCompatActivity {
    final int RC___SUBSCRIPTION = 1801;

    @BindView(R.id.House) View mHouse;

    MaiProgress mProgress = null;

    static boolean mFirstFlag = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.books___activity);
        ButterKnife.bind(this);
        Utils.setColor_StatusBarIcon(this, true, this.getResources().getColor(R.color.sys_bg___ad));

        if(mFirstFlag) mProgress = MaiProgress.build(this).show();

        initTopButton();
        initAPI();
    }

    @Override
    protected void onResume() {
        super.onResume();

        initSubsButton();
        loadingData();
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent it = new Intent(this, Home___Activity.class);
        startActivity(it);
    }


    /* ############################################################################################################ */
    /* PAGER */
    /* ============================================================================================================ */

    @BindView(R.id.Pager)               com.example.lib.Deck    mPager;
    @BindView(R.id.Pager_Indicator)     CircleIndicator         mPager_Indicator;

    List<BookVO>  mList_Book = new ArrayList<BookVO>();
    List<View>  mList_BookView = new ArrayList<View>();
    Books___Apdapter mAdapter;

    void initPager() {
        mAdapter = new Books___Apdapter(this, mList_Book, mList_BookView);
        mPager.addOnPageChangeListener(mPagerListener);
        mPager.setOffscreenPageLimit(1);
        mPager.useDefaultPadding(this);
        mPager.setAdapter(mAdapter);
        mPager_Indicator.setViewPager(mPager);
        if(mList_Book.size() > 0) mHouse.setBackgroundColor(Color.parseColor(mList_Book.get(0).getColor()));
    }

    ViewPager.OnPageChangeListener mPagerListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {
        }

        @Override
        public void onPageSelected(int position) {
            mHouse.setBackgroundColor(Color.parseColor(mList_Book.get(position).getColor()));
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };

    /* ############################################################################################################ */
    // 데이타 로딩

    void loadingData() {
        mAPI_Book.getBookList(SharedInfo.User.mAuthKey, Utils.getStateAllowedAllContent()).enqueue(new Callback<RestApiVO>() {
            @Override
            public void onResponse(@NonNull Call<RestApiVO> call, @NonNull Response<RestApiVO> response) {
                if(response.code() == 401) {
                    Toast.makeText(Books___Activity.this, "인증이 되지 않았습니다. ", Toast.LENGTH_SHORT).show();
                    if(mProgress != null) mProgress.hide();
                    return;
                }
                RestApiVO body = response.body();
                if (body != null && body.getRes_code() == ResCode.SUCC) {
                    mList_Book.clear();
                    mList_Book.addAll(body.getBook_list());

                    if(mList_BookView.size() == 0) new buildView().execute();
                    else mAdapter.updateContents();

                } else {
                    Toast.makeText(Books___Activity.this, "네트워크가 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
                    if(mProgress != null) mProgress.hide();
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestApiVO> call, @NonNull Throwable t) {
                Log.e("AAA", "API.login() >> onFailure() >> " + t.toString());
                Toast.makeText(Books___Activity.this, "네트워크가 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
                if(mProgress != null) mProgress.hide();
            }
        });
    }

    class buildView extends AsyncTask<Void /* doInBackground IN */, Integer /* onProgressUpdate IN */, Integer /* doInBackground OUT, onPostExecute IN */> {
        protected Integer doInBackground(Void ... value) {
            if(mList_BookView.size() == 0) {
                for (int xx = 0; xx < mList_Book.size(); xx++) {
                    mList_BookView.add(LayoutInflater.from(Books___Activity.this).inflate(R.layout.book___card, null));
                }
            }
            return 0;
        }

        protected void onProgressUpdate(Integer... progress) {
            if(mProgress != null) mProgress.hide();
        }

        protected void onPostExecute(Integer result) {
            initPager();
            if(mProgress != null) mProgress.dismiss();
        }
    }



    /* ============================================================================================================================================== */
    /* API */
    /* ============================================================================================================================================== */

    BookAPI mAPI_Book;

    void initAPI() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConstDef.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mAPI_Book = retrofit.create(BookAPI.class);
    }



    /* ############################################################################################################ */
    /* 상단 공통 버턴 */

    @BindView(R.id.Button_Hambuger) RippleLayout mButton_Hambuger;

    void initTopButton() {
        mButton_Hambuger.setOnRippleCompleteListener(new RippleLayout.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleLayout rippleView) {
                Intent it = new Intent(Books___Activity.this, SideMenu___Activity.class);
                startActivity(it);
            }
        });
    }

    @OnClick(R.id.Button_TopLogo)
    void onClick_TopLog(View view) {
        Intent it = new Intent(this, Books___Activity.class);
        startActivity(it);
        finish();
    }

    @OnClick(R.id.Button_StudyHistory)
    void onClick_StudyHistory(View view) {
        if(SharedInfo.User.isLogined()) {
            Intent it = new Intent(this, StudyHistory___Activity.class);
            startActivity(it);
        } else {
            Intent it = new Intent(this, StudyHistory___NoLogin___Activity.class);
            startActivity(it);
        }
    }


    /* ############################################################################################################ */
    /* SUBS 버턴 */

    @BindView(R.id.Button_Subs) RippleLayout mButton_Subs;

    void initSubsButton() {
        Log.d("AAA", "[ 구독 ] .......................... state=" + SharedInfo.Subscription.mSubsFlag);
        Log.d("AAA", "[ 쿠폰 ] .......................... state=" + SharedInfo.Coupon.getFreeCoupon());
        Log.d("AAA", "[ 로그인 ] .......................... state=" + SharedInfo.User.isLogined());

        if(Utils.isAllowedAllContent()) mButton_Subs.setVisibility(View.GONE);
        else mButton_Subs.setVisibility(View.VISIBLE);

        mButton_Subs.setOnRippleCompleteListener(new RippleLayout.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleLayout rippleView) {
                Intent it = new Intent(Books___Activity.this, Subs___Activity.class);
                startActivityForResult(it, RC___SUBSCRIPTION);
            }
        });
    }

}
