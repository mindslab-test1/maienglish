package ai.mindslab.maienglish.STT;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.AudioRouting;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.graphics.drawable.AnimatedVectorDrawableCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.Map;

import ai.mindslab.maienglish.Common.ConstDef;
import ai.mindslab.maienglish.Common.MaiDialog;
import ai.mindslab.maienglish.RestAPI.Param;
import ai.mindslab.maienglish.Common.SharedInfo;
import ai.mindslab.maienglish.Common.Utils;
import ai.mindslab.maienglish.R;
import ai.mindslab.maienglish.RestAPI.HttpUtils;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

import static android.media.AudioRecord.RECORDSTATE_STOPPED;

public class SttHandler {
    public static final int MSG___STT_RESPONSE = 2931;

    final int STATE___DISABLE = -1;
    final int STATE___OFF = 0;
    final int STATE___ON = 1;

    int mState = STATE___DISABLE;

    Context mContext;
    Handler mHandler;
    ImageView mImage_Mic;
    ImageView mImage_MicEffect;
    String mAnswerText;

    public SttHandler(Context ctx, Handler handler, ImageView micImage, ImageView micEffect) {
        mContext = ctx;
        mHandler = handler;
        mImage_Mic = micImage;
        mImage_MicEffect = micEffect;

        mImage_Mic.setOnClickListener(mOnClick);
        setIdle();
    }

    /* ################################################################################################################################################### */
    // CONTROLLER

    public void setIdle() {
        mState = STATE___DISABLE;
        mImage_Mic.setEnabled(false);
        drawMic_Disable();

    }

    public void setReady(String answerText) {
        mAnswerText = answerText;
        mState = STATE___OFF;
        mImage_Mic.setEnabled(true);
        drawMic_Off();
        stopAudio();
    }

    public void setStart(String answerText) {
        mAnswerText = answerText;
        mState = STATE___ON;
        mImage_Mic.setEnabled(true);
        drawMic_On();
        startAudio();
        startSTT();
    }

    public boolean isIdle() {
        if(mState == STATE___DISABLE) return true;
        else return false;
    }

    View.OnClickListener mOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(mState == STATE___OFF) {
                setStart(mAnswerText);
            } else {
                setReady(mAnswerText);
            }
        }
    };

    /* ################################################################################################################################################### */
    // UI

    void drawMicState(int state) {
        switch(state) {
            case STATE___DISABLE:   drawMic_Disable(); break;
            case STATE___OFF:       drawMic_Off(); break;
            case STATE___ON:        drawMic_On(); break;
        }
    }

    void drawMic_Disable() {
        Picasso.with(mContext)
                .load(R.mipmap.mic___disable)
                .into(mImage_Mic);
    }

    void drawMic_Off() {
        Drawable drawable = mImage_MicEffect.getDrawable();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (drawable instanceof AnimatedVectorDrawable) {
                AnimatedVectorDrawable animatedVectorDrawable = (AnimatedVectorDrawable) drawable;
                animatedVectorDrawable.stop();
            }
        } else {
            if (drawable instanceof AnimatedVectorDrawableCompat) {
                AnimatedVectorDrawableCompat animatedVectorDrawableCompat = (AnimatedVectorDrawableCompat) drawable;
                animatedVectorDrawableCompat.stop();
            }
        }

//        AnimatedVectorDrawableCompat avd = (AnimatedVectorDrawableCompat) mImage_MicEffect.getDrawable();
        Picasso.with(mContext)
                .load(R.mipmap.mic___inactive)
                .into(mImage_Mic);
//        avd.stop();
        mImage_MicEffect.setVisibility(View.INVISIBLE);
    }

    void drawMic_On() {
        Drawable drawable = mImage_MicEffect.getDrawable();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (drawable instanceof AnimatedVectorDrawable) {
                AnimatedVectorDrawable animatedVectorDrawable = (AnimatedVectorDrawable) drawable;
                animatedVectorDrawable.start();
            }
        } else {
            if (drawable instanceof AnimatedVectorDrawableCompat) {
                AnimatedVectorDrawableCompat animatedVectorDrawableCompat = (AnimatedVectorDrawableCompat) drawable;
                animatedVectorDrawableCompat.start();
            }
        }


//        AnimatedVectorDrawableCompat avd = (AnimatedVectorDrawableCompat) mImage_MicEffect.getDrawable();
        Picasso.with(mContext)
                .load(R.mipmap.mic___active)
                .placeholder(R.mipmap.mic___active)
                .into(mImage_Mic);

//        avd.start();
        mImage_MicEffect.setVisibility(View.VISIBLE);
    }


    /* ################################################################################################################################################### */
    // AUDIO

    private int mAudioSource = MediaRecorder.AudioSource.MIC;
    private int mSampleRate = 16000;
    private int mChannelCount = AudioFormat.CHANNEL_IN_MONO;
    private int mAudioFormat = AudioFormat.ENCODING_PCM_16BIT;
    private int mBufferSize = AudioTrack.getMinBufferSize(mSampleRate, AudioFormat.CHANNEL_IN_STEREO, mAudioFormat);

    public AudioRecord mAudioRecord = null;

    boolean mIsRecording = false;
    Thread mRecordThread = null;

    void startAudio() {
        Log.e("AAA", "[ MIC ] ................................... start >>>>");

        mAudioRecord = new AudioRecord(mAudioSource, mSampleRate, mChannelCount, mAudioFormat, mBufferSize);
        mAudioRecord.startRecording();
        Log.e("AAA", "[ MIC ] state = " + mAudioRecord.getState());
        Log.e("AAA", "[ MIC ] record state = " + mAudioRecord.getRecordingState());
        if(mAudioRecord.getRecordingState() == RECORDSTATE_STOPPED) {
            setReady("");
//            MaiDialog.build(mContext)
//                    .setImage(R.mipmap.info)
//                    .setMessage("다른 곳에서 마이크를 사용중입니다.\n확인 후에 다시 이용하세요.")
//                    .setOk("확인", null)
//                    .show();
            if(mAudioRecord != null) mAudioRecord.stop();
            if(mAudioRecord != null) mAudioRecord.release();
            mAudioRecord = null;

            Toast.makeText(mContext, "다른 곳에서 마이크를 사용중입니다.\n확인 후에 다시 이용하세요.", Toast.LENGTH_SHORT).show();
            return;
        }

        mIsRecording = true;
        mRecordThread = new Thread(new Runnable() {
            @Override
            public void run() {
                byte[] readData = new byte[mBufferSize];

                Log.e("AAA", "[ MIC ] thread.... begin ");
                while(mIsRecording) {
                    int ret = mAudioRecord.read(readData, 0, mBufferSize);  //  AudioRecord의 read 함수를 통해 pcm data 를 읽어옴
                    if(ret <= 0) {
                        Log.e("AAA", "[ MIC ] packet read fail >> ret = " + ret);
                        break;
                    }
                    try {
                        mWebSocket.send(ByteString.of(readData));
                    } catch (Exception e) {}
                }

                Log.e("AAA", "[ MIC ] thread.... end ");
            }
        });
    }

    void captureAudio() {
        if(mRecordThread != null) mRecordThread.start();
    }

    void stopAudio() {
        mIsRecording = false;
        try { if(mRecordThread != null) mRecordThread.join(); } catch (InterruptedException e) { e.printStackTrace(); }
        mRecordThread = null;

        if(mAudioRecord != null) mAudioRecord.stop();
        if(mAudioRecord != null) mAudioRecord.release();
        mAudioRecord = null;

        Log.e("AAA", "[ MIC ] ................................... stop <<<<");
    }


    /* ################################################################################################################################################### */
    // WebSocket

    public OkHttpClient mClient;
    public WebSocket mWebSocket = null;

    void startSTT() {
        String param = String.format("?v=%s&biz=%s&channel=%s&language=%s&service=%s&userId=%s&answerText=%s&type=%s",
                "1",
                "BP0002",
                "contact channel",
                "eng",
                "",
                "mAIEnglish." + (SharedInfo.User.isLogined() ? SharedInfo.User.mEmail: "Guest"),
                mAnswerText,
                "E");

        mClient = HttpUtils.getUnsafeOkHttpClient().build();
        Request request = new Request.Builder().url(ConstDef.BASE_WSS_URL + param).build();
        SttWebSocketListener socketListener = new SttWebSocketListener();
        mWebSocket = mClient.newWebSocket(request, socketListener);
        mClient.dispatcher().executorService().shutdown();
    }


    public class SttWebSocketListener  extends WebSocketListener {
        private static final int NORMAL_CLOSURE_STATUS = 1000;
        @Override
        public void onOpen(WebSocket webSocket, Response response) {
            Log.e("AAA", "WSS Connected........................ " + webSocket.request().url().toString());
            captureAudio();
        }
        @Override
        public void onMessage(WebSocket webSocket, String text) {
            Log.e("AAA", "Receiving : " + text);
            Map<String, Object> map = Utils.jsonString2Map(text);
            if(map != null) {
                if(map.get(Param.STT_RES_CODE).equals(ConstDef.STT_RES_CODE___SUCCESS)) {
                    ((Activity) mContext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() { setReady(mAnswerText); }
                    });

                    mHandler.sendMessage(mHandler.obtainMessage(MSG___STT_RESPONSE, map));
                    onClosing(webSocket, NORMAL_CLOSURE_STATUS, "received result");
                }
                else {
                    ((Activity) mContext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() { setReady(mAnswerText); }
                    });

                    Log.e("AAA", "음성인식 타임아웃 발생");
                    onClosing(webSocket, NORMAL_CLOSURE_STATUS, "received result");
                }
            } else {
                Log.e("AAA", "[ WSS-STT ] onMessage ................................ map = null, text = " + text);

                onClosing(webSocket, NORMAL_CLOSURE_STATUS, "received result");
            }
        }
        @Override
        public void onMessage(WebSocket webSocket, ByteString bytes) {
            Log.e("AAA", "Receiving bytes : " + bytes.hex());
        }
        @Override
        public void onClosing(WebSocket webSocket, int code, String reason) {
            Log.e("AAA", "Closing : " + code + " / " + reason);

            webSocket.close(code, reason);
        }
        @Override
        public void onFailure(WebSocket webSocket, Throwable t, Response response) {
            Log.e("AAA", "Error : " + t.getMessage());

            ((Activity) mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(mContext, "[ WSS - STT ]\n서버와 연결이 되지 않습니다.\n잠시 후 다시 시도하세요.", Toast.LENGTH_LONG).show();
                    setReady(mAnswerText);
                }
            });
            onClosing(webSocket, NORMAL_CLOSURE_STATUS, null);

        }
    }


    /* ################################################################################################################################################### */
    // HANDLER

    Handler mMyHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what) {

            }
        }
    };
}
